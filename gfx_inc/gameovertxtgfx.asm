; ASM source file created by SevenuP v1.12
; SevenuP (C) Copyright 2002-2004 by Jaime Tejedor G�mez, aka Metalbrain

;GRAPHIC DATA:
;Pixel Size:      ( 56,  16)
;Char Size:       (  7,   2)
;Sort Priorities: X char, Char line, Y char
;Attributes:      At the end
;Mask:            No

gameover:
	DEFB	  6,  0,  0,  0,  8,  0,  0
	DEFB	 15,  0,  0,  0, 30,  0,  0
	DEFB	 29,224,  0,  0, 39,  0,  0
	DEFB	 44, 64,  0,  0, 81,128,  0
	DEFB	 76,128,  0,  0,209,192,  0
	DEFB	 77,  0,  0,  0,154,192, 68
	DEFB	207,194, 16, 16,148,208,239
	DEFB	205,101, 53, 56,144,214,181

	DEFB	205, 48,154,172,148,210,148
	DEFB	201,146,146,164,218,138,164
	DEFB	209, 20,146,168,209,138,196
	DEFB	 97,180,146,176, 81,138,132
	DEFB	113,100,146,160,115, 14,148
	DEFB	 31,197,210,164, 62,  4,100
	DEFB	  7,  2,128, 24, 28,  0,  0
	DEFB	  0,  0,  0,  0,  0,  0,  0
; (Attributes here, insert label if needed)
	DEFB	  2,  2,  2,  2,  2,  2,  2
	DEFB	 65, 65,  1,  1, 65, 65,  1	;126BYTES


