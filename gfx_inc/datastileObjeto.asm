	org	$6c00		; 

; Tiles OBJETO 

; deco_marcador 1
	defb 22,16,2
	DEFB	 24, 24
	DEFB	 28, 56
	DEFB	 15,240
	DEFB	207,243
	DEFB	231,231
	DEFB	115,206
	DEFB	 61,188
	DEFB	 63,252

	DEFB	 63,252
	DEFB	 61,188
	DEFB	115,206
	DEFB	231,231
	DEFB	207,243
	DEFB	 15,240
	DEFB	 28, 56
	DEFB	 24, 24
; (Attributes here, insert label if needed)
	DEFB	  2,  2
	DEFB	  2,  2
;	DEFB	 66, 66
;	DEFB	 66,  2


; deco_marcador 2
	defb 23,16,2
	DEFB	 24, 24
	DEFB	124, 62
	DEFB	126,126
	DEFB	254,127
	DEFB	254,127
	DEFB	127,254
	DEFB	 62,124
	DEFB	  5,160

	DEFB	  5,160
	DEFB	 62,124
	DEFB	127,254
	DEFB	254,127
	DEFB	254,127
	DEFB	126,126
	DEFB	124, 62
	DEFB	 24, 24
; (Attributes here, insert label if needed)
	DEFB	  2,  2
	DEFB	  2,  2
;	DEFB	 66, 66
;	DEFB	 66,  2


; deco_marcador 3
	defb 24,16,2
	DEFB	  2, 16
	DEFB	 25, 16
	DEFB	 25, 32
	DEFB	  9, 38
	DEFB	201, 30
	DEFB	 51,192
	DEFB	  4, 33
	DEFB	  4, 62

	DEFB	124, 32
	DEFB	132, 32
	DEFB	  3,204
	DEFB	120,147
	DEFB	100,144
	DEFB	  4,152
	DEFB	  8,152
	DEFB	  8, 64
; (Attributes here, insert label if needed)
	DEFB	  2,  2
	DEFB	  2,  2
;	DEFB	 66, 66
;	DEFB	 66,  2


; deco_marcador 4
	defb 25,16,2
	DEFB	  8, 16
	DEFB	  8, 16
	DEFB	  8, 16
	DEFB	  4, 32
	DEFB	230,103
	DEFB	 19,200
	DEFB	 14,112
	DEFB	  4, 32

	DEFB	  4, 32
	DEFB	 14,112
	DEFB	 19,200
	DEFB	226, 71
	DEFB	  6, 96
	DEFB	  4, 48
	DEFB	  8, 16
	DEFB	  8, 16
; (Attributes here, insert label if needed)
	DEFB	  2,  2
	DEFB	  2,  2
;	DEFB	 66, 66
;	DEFB	 66,  2



; espadacruceta_derecha
	defb 26,8,1
	DEFB	128,224,112, 30, 58, 28,  0,  0
; (Attributes here, insert label if needed)
	DEFB	  2


; espadacruceta_izquierda
	defb 27,8,1
	DEFB	  0,  0, 40, 56, 28, 20, 10,  2
; (Attributes here, insert label if needed)
	DEFB	  2


; espadamastil
	defb 28,8,2
	DEFB	  0,  0
	DEFB	  0, 31
	DEFB	  0,252
	DEFB	  1,241
	DEFB	  7,134
	DEFB	 28, 48
	DEFB	112,128
	DEFB	192,  0
; (Attributes here, insert label if needed)
	DEFB	  5,  5


; espadapocion
	defb 29,8,2
	DEFB	 15,168
	DEFB	  3, 64
	DEFB	  1,128
	DEFB	  7,160
	DEFB	 11,244
	DEFB	 23,250
	DEFB	 23,214
	DEFB	 15,168
; (Attributes here, insert label if needed)
	DEFB	  6,  4


; espadapomo
	defb 30,8,1
	DEFB	  0,  0, 12,  8,  7, 26,126,243
; (Attributes here, insert label if needed)
	DEFB	  3


; espadapunta
	defb 31,8,1
	DEFB	  7, 12, 24, 48, 96,192,128,128
; (Attributes here, insert label if needed)
	DEFB	  5

; trampilla horizontal
	db	 69,8,4
	DEFB	 56,  0,  0,  0
	DEFB	 69, 24,  0, 24
	DEFB	131,221,117,223
	DEFB	177, 27,174,154
	DEFB	  4,  0,  0,  0
	DEFB	 56,  0,  0,  0
	DEFB	  0,  0,  0,  0
	DEFB	  0,  0,  0,  0
; (Attributes here, insert label if needed)
	DEFB	 65, 67, 67, 67

; trampilla vertical
	db	 70,16,1
	DEFB	 22, 22,127,  2, 22, 22, 22, 22
	DEFB	 22, 22, 22, 22,127,  2, 22,  0
; (Attributes here, insert label if needed)
	DEFB	 71, 71

; vaciar Objetos y acciones
;tile1x1:
	db	71,8,1
	db	0,0,0,0,0,0,0,0
	db	6
;tile1x2:
	db	72,8,2
	db	0,0,0,0,0,0,0,0
	db	0,0,0,0,0,0,0,0
	db	6,6
;tile2x2:
	db	73,16,2
	db	0,0,0,0,0,0,0,0
	db	0,0,0,0,0,0,0,0
	db	0,0,0,0,0,0,0,0
	db	0,0,0,0,0,0,0,0
	db	6,6
	db	6,6

; trampilla horizontal en vertical. Caida
	db	74,32,1
	DEFB	 56, 68,  2, 50, 82, 36,  8, 28
	DEFB	  4,  4,  0,  6, 14,  0,  8,  4
	DEFB	  8,  4, 12,  4,  8,  4,  8,  4
	DEFB	 12,  4,  0, 14, 14,  0, 12,  4
; (Attributes here, insert label if needed)
	DEFB	  1,  3,  2,  2

; Activador de puertas
	db	75,16,2
	DEFB	  0,  0
	DEFB	  0,  0
	DEFB	  0,  0
	DEFB	  1,128
	DEFB	  6, 96
	DEFB	  8, 16
	DEFB	  1,128
	DEFB	  3,192

	DEFB	  3,192
	DEFB	  9,144
	DEFB	  6, 64
	DEFB	  1,  0
	DEFB	  0,  0
	DEFB	  0,  0
	DEFB	  0,  0
	DEFB	  0,  0

	DEFB	  7,  3
	DEFB	  3,  3

; Puerta Vertical subida o desactivada
	db	76,16,1
	DEFB	 22,127,  2, 22,  0,  0,  0,  0
	DEFB	  0,  0,  0,  0,  0,  0,  0,  0
	DEFB	  7,  6

; Calaveras
	db	77,8,1
	DEFB	  0, 28, 62, 87, 19,126, 84,  0
	DEFB	  7

; Flecha Indicativa Espada Activa
	db	78,8,1
	DEFB	  0,  8,116, 66,118,124,  8,  0
	DEFB	 71

; Flecha Indicativa Pocima Activa
	db	79,8,1
	DEFB	  0, 30, 26, 26, 49, 26, 12,  0
	DEFB	 71


; Desactivador de enemigos
	db	80,16,2
	DEFB	  0,  0
	DEFB	  0,  0
	DEFB	  0,  0
	DEFB	  0,  0
	DEFB	  1,  0
	DEFB	  3,128
	DEFB	  7,  0
	DEFB	 15,160

	DEFB	  7, 64
	DEFB	  3,128
	DEFB	  1,  0
	DEFB	  0,  0
	DEFB	  0,  0
	DEFB	  0,  0
	DEFB	  0,  0
	DEFB	  0,  0
; (Attributes here, insert label if needed)
	DEFB	  3,  1
	DEFB	  2,  2

;tile1x3:
;	db	81,24,1
;	db	0,0,0,0,0,0,0,0
;	db	0,0,0,0,0,0,0,0
;	db	0,0,0,0,0,0,0,0
;	db	6,6,6