;----------------------------------------------------------------------------------------
;  RUTINA DE MAPEADO
;----------------------------------------------------------------------------------------
; Entrada senpro= ARRIBA,DER,ABAJO,IZQ. (0,1,2,3). panta = anterior
; Salida en el contenido hl el nuevo valor de panta

mapping:
; Es necesario que H=0 y L=variable panta. As� que en la etiq.panta hay que dejar el db anterior sin uso
; Eso o poner aqui un ld h,0 desp�es de esta primera linea, lo cual es absurdo
	ld	hl,[panta]	; 5/16
	add	hl,hl		; 3/11
	add	hl,hl		; 3/11 multiplicamos el valor 
	ld	de,TABSEN	; 3/10 TABLA CON 256X4 = 1024  POSIBLES DIRECCIONES
	add	hl,de		; 3/11
	ld	a,[senpro]	; 4/13
	add	a,l		; 1/4
	ld	l,a		; 1/4
	jr	nc,bmapping	; 3/12
	inc	h		; 1/4
bmapping:
	ld	a,[hl]		; 2/7 NUEVO VALOR DE LA VARIABLE PANTA
        ld	[panta],a	; 4/13
        ret			; 3/10
;  FIN RUTINA DE MAPEADO	; total estados = 126
;----------------------------------------------------------------------------------------
;TABLA DE DIRECCIONES DE SALIDA DE PANTALLA. MAPEADO 
;----------------------------------------------------------------------------------------
;
; Creo todas las direcciones de cada pantalla para el valor de senpro. 0,1,2,3
;
TABSEN:					; pantalla
	defb 255,255,2,255		; 0
	defb 255,255,5,255		; 1
	defb 0,3,13,255			; 2
	defb 255,4,14,2			; 3
	defb 255,5,15,3			; 4
	defb 1,6,16,4			; 5, etc...
	defb 255,7,17,5
	defb 255,8,18,6
	defb 255,255,19,7
	defb 255,10,24,255
	defb 255,255,25,9
	defb 255,12,255,26
	defb 255,13,255,11
	defb 2,14,27,12
	defb 3,15,255,13
	defb 4,16,255,14
	defb 5,17,28,15
	defb 6,18,255,16
	defb 7,19,255,17
	defb 8,20,255,18
	defb 255,21,255,19
	defb 255,22,255,20
	defb 255,23,255,21
	defb 255,24,255,22
	defb 9,25,255,23
	defb 10,26,255,24
	defb 255,11,255,25
	defb 13,255,29,255
	defb 16,255,32,255
	defb 27,30,33,255
	defb 255,31,255,29
	defb 255,32,255,30
	defb 28,255,255,31
	defb 29,34,37,255
	defb 30,35,38,33
	defb 31,36,39,34
	defb 32,255,40,35
	defb 33,38,255,255
	defb 34,39,255,37
	defb 35,40,255,38
	defb 36,41,255,39
	defb 255,42,45,40
	defb 255,43,46,41
	defb 255,44,47,42
	defb 255,50,48,43
	defb 41,46,255,255
	defb 42,47,255,45
	defb 43,48,255,46
	defb 44,49,255,47
	defb 50,255,255,48
	defb 51,255,49,255
	defb 52,255,50,255
	defb 53,255,51,255
	defb 255,54,52,255
	defb 255,55,255,53
	defb 255,56,255,54
	defb 255,57,255,55
	defb 255,255,255,56