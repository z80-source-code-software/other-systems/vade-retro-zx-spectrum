; Esta rutina ha de ser llamada antes que a rucobjpant.asm en el engine
; debido a que ambas usan las mismas variables y los datos de esa otra rutina
; son necesarios para la comprobaci�n posterior.

;----------------------------------------------------------------------------------------
; Colocamos las acciones ha tomar en pantalla: puertas, trampas, 
;----------------------------------------------------------------------------------------
poneracciones:
	ld	de,6
	ld	hl,tabla_Acciones -6
	ld	a,[panta]
	ld	b,a
bponeracc:
	add	hl,de
	ld	a,[hl]			
	cp	255			; fin tabla
	ret	z
	cp	b			; pantalla
	jr	nz,bponeracc
accionencontrada:
	inc	hl			
	ld	[churrAcc],hl		; Guardo la posic. hl en la variable para usarla despu�s.
	inc	hl
	ld	a,[hl]			; numero de tile / objeto a pintar
	ld	[num_tile],a
	inc	hl
	ld	a,[hl]			; columna en char
	ld	[col_dat],a
	ld	c,a
	inc	hl
	ld	a,[hl]			; fila en char
	ld	[fil_dat],a
	ld	b,a
	inc	hl
	ld	a,[hl]
	ld	[tipAccion],a		; Guardo la acci�n a resolver para esa pantalla.
	call	search_dat		; En bupaim.asm. Salida tile a imprimir en 'de'
;	call	c_dirbufftile		; colocaci�n en buffer de los objetos
;	ret
	jp	c_dirbufftile
;----------------------------------------------------------------------------------------
;
; Contiene: Pantalla, Activado ?,num.tile, pos. X char, pos. Y pixels, tipo accion
; tipo accion: 1 puerta horizontal, 2 puerta vertical, 3 accion con enemigos

tabla_Acciones:
	db	28,1,69,10,128,1
	db	35,1,70,12, 80,2
	db	40,1,73, 5,104,3	; tile vacio para cuando acabes la acci�n de desact.enemigos
	db	47,1,70,26,104,2
	db	52,1,73,18, 72,3	; tile vacio para cuando acabes la acci�n de desact.enemigos
	db	55,1,70,20,120,2
	db	56,1,73, 6, 80,3	; tile vacio para cuando acabes la acci�n de desact.enemigos
	db	255

