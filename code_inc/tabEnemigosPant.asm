;----------------------------------------------------------------------------------------
; Tabla de animaciones por pantalla. 
;----------------------------------------------------------------------------------------
;;	org 54353
; 
; activo (si o no), posx en char, posy en pixels, tamba�o1frame (para sumar a dblowAnimMem)
; codanim codigo para actuar sobre los enemigos vert 1,2,4,8 y horiz 16,32,64,128 cada uno tiene una funci�n 
; seg�n direcci�n. direccion ix+15 0 horizontal, 1 vertical
; sentido en la direcci�n: 0 arriba o derecha 1 abajo o izquierda. Al inicio estar� al reves (vert) excepto 
; los enemigos que solo tengan un sentido.
; desplaza, pixels que se mueve en vertical. en horizontal no se usa
;		dblowIniAnim	dbhighIniAnim	tama�oframe	n.fram	altosc	anchoch
; ara�a		$e0,		$bd,		64		3	16	2	
; ave		$a0,		$be,		32		2	8	2
; bola		$e0,		%be,		16		1	8	1
; gota		$f0,		$be,		16		1	8	1

; fantas_BL_der	$a0,		$b9,		96		3	24	2
; fantas_BL_izq $c0,		$ba,

; chinchillader $e0,		$bb,		32		3	8	2
; chinchillaizq $40,		$bc,
; flecha_der	$a0,		$bc,		32		2	8	2
; flecha_izq	$e0,		$bc,
; murcielagoder	$20,		$bd,		32		3	8	2
; murcielagoizq	$80,		$bd,

; Notas importantes:
; La tabla de ENEMIGOS es mayor que la TABENEMPANT porque debemos guardar los datos de la posic.
; en pantalla de los enemigos y en el buffer. 

; Defb's con los siguientes datos
; panta, activo, posx,  posy, ticks, frameskip, frameActual, framestotal, altosc, anchochars,
; ix0,	ix1,	ix2,	ix3,	ix4,	ix5,		ix6,	ix7,	  ix8,  	ix9
; tama�o1frame, dblowEnem, dbhighEnem, dblowEnemIni,dbhighEnemIni, direccion, limite1
;	ix10,	ix11,		ix12,  ix13,		ix14,	   ix15,	ix16
; limite2, codanim, sentido, desplaz	dblowPant, dbhighPant, dblowBuff, dbhighBuff
; ix17,	    ix18,	ix19	ix20	ix21		ix22	ix23		ix24

; ix+	     0,1, 2,  3,4,5,6,7, 8, 9, 10, 11, 12, 13, 14,15, 16, 17,18,19,20, 21, 22, 23, 24
; enemigos en vertical
;	db  12,1,12, 88,1,5,1,1, 8, 2,-32,$a0,$be,$a0,$be, 1, 88,128,  4, 0, 4,   0, 0,  0, 0 ;ave REPETIR ENTRE 4 Y 8. BAJA 4
; 	db  12,1,22, 88,1,6,2,2,16, 2,-64,$e0,$bd,$e0,$bd, 1, 88,128,  8, 0, 8,   0, 0,  0, 0 ;ara�a REPETIR 4. BAJA 6
;	db  12,1,10, 88,1,4,0,0, 8, 1,-16,$e0,$be,$e0,$be, 1, 88,128,  2, 1, 1,   0, 0,  0, 0 ;bola
;	db  30,0,26, 56,1,3,0,0, 8, 1,-16,$f0,$be,$f0,$be, 1, 56, 88,  1, 1, 4,   0, 0,  0, 0 ;gota !ojo!
; enemigos en horizontal
;	db  30,1, 7, 88,1,4,2,2, 8, 2, 32,$e0,$bb,$e0,$bb, 0, 12,  3, 32, 0, 0,   0, 0,  0, 0 ;chinchillader
;	db  30,1, 7, 72,1,6,2,2, 8, 2, 32,$20,$bd,$20,$bd, 0, 12,  3, 64, 0, 0,   0, 0,  0, 0 ;murcielagoder (velocidad 3 a 6)
;	db  30,0, 3, 88,1,3,1,1, 8, 2, 32,$a0,$bc,$a0,$bc, 0, 12,  3,128, 0, 0,   0, 0,  0, 0 ;flechader
;	db  30,0,12, 88,1,3,1,1, 8, 2, 32,$e0,$bc,$e0,$bc, 0, 12,  3,128, 1, 0,   0, 0,  0, 0 ;flechaizq

;-------------------------------------------------------------------------------------------------------
; notas de posicion de enemigos. 
; 1. La bola siempre debe estar en el primer db de la pantalla. Por este motivo es causa el 2.
; 2. A raiz de esto la flecha JAMAS debe estar el primer db de la pantalla o interactuar�n juntas.
; O no podr�s ponerlas en la misma pantalla. Ya que la gota que aparece actua sobre ix+1 activandolas
; Notas importantes:
; poner ix+4 = 1, ix+6 = al �ltimo frame osea igual que ix+7. Esto es para siempre ya que de lo contrario
; no funcionar� correctamente la rutina de gestion. Hay que tenerlo en cuenta a la hora de salir de la 
; pantalla, la rutina de restauraci�n debe poner esos valores al grabar denuevo los enemigos de la pantalla
; en tabenempant. Habra que ver esto bien porque no se usan de igual forma los db's para enemigos vert u hor
; 
TABENEMPANT:				 
; ix+	    0,1,2,3,   4,5,6,7, 8, 9,10, 11, 12, 13, 14,15, 16, 17,18,19,20
; ix+4 siempre =0
; ix+6 siempre = ix+7
; ix+10 tama�o en negativo para los enemigos verticales de doble sentido (ave y ara�a)
; ix+19 en movimiento vertical siempre a 0

 	db 00,1,14, 72,1,6,2,2,16,2,-64,$e0,$bd,$e0,$bd,1, 72,120,  8,0, 8	;ara�a
;	db 00,1,20,104,1,6,2,2, 8,2, 32,$20,$bd,$20,$bd,0, 24, 20, 64,0, 0	;murcielagoder 

;	db 01,1,10, 72,1,6,2,2,16,2,-64,$e0,$bd,$e0,$bd,1, 72,128,  8,0, 8	;ara�a
	db 01,1,26,136,1,4,2,2, 8,2, 32,$e0,$bb,$e0,$bb,0, 26, 20, 32,0, 0	;chinchillader

	db 02,1,16, 72,1,6,2,2, 8,2, 32,$20,$bd,$20,$bd,0, 16,  6, 64,0, 0	;murcielagoder 

;	db 03,1, 8, 88,1,8,1,1, 8,2,-32,$a0,$be,$a0,$be,1, 88,128,  4,0, 4	;ave
	db 03,1,15,112,1,2,2,2, 8,2, 32,$20,$bd,$20,$bd,0, 18, 13, 64,0, 0	;murcielagoder 
 	db 03,1,22,104,1,6,1,1, 8,2,-32,$a0,$be,$a0,$be,1,104,128,  4,0, 4	;ave

;	db 04,1, 4,112,1,5,2,2, 8,2, 32,$80,$bd,$80,$bd,0, 14,  4, 64,1, 0	;murcielagoder 
	db 04,1,26,112,1,5,2,2, 8,2, 32,$20,$bd,$20,$bd,0, 26,  6, 64,0, 0	;murcielagoder 

;	db 05,1, 7, 56,1,5,1,1, 8,2,-32,$a0,$be,$a0,$be,1, 56,120,  4,0, 4	;ave
	db 05,1,26, 80,1,6,1,1, 8,2,-32,$a0,$be,$a0,$be,1, 80,144,  4,0, 4	;ave

	db 06,0,15,120,1,3,1,1, 8,2, 32,$e0,$bc,$e0,$bc,0, 15,  0,128,1, 0	;flechaizq

	db 07,1,11, 56,1,3,1,1, 8,2,-32,$a0,$be,$a0,$be,1, 56,104,  4,0, 4	;ave abajo
;	db 07,1,19,104,1,8,1,1, 8,2,-32,$c0,$be,$c0,$be,1, 64,104,  4,0, 4	;ave arriba

;	db 09,1,13,120,1,3,2,2, 8,2, 32,$20,$bd,$20,$bd,0, 24, 13, 64,0, 0	;murcielagoder 
	db 09,1,28,120,1,5,1,1, 8,2,-32,$c0,$be,$c0,$be,1, 80,120,  4,0, 4	;ave arriba

	db 10,1, 4,120,1,3,2,2, 8,2, 32,$20,$bd,$20,$bd,0, 13,  4, 64,0, 0	;murcielagoder 
;	db 10,1,26, 88,1,5,2,2, 8,2, 32,$20,$bd,$20,$bd,0, 26, 16, 64,0, 0	;murcielagoder 

	db 11,1, 2, 96,1,4,2,2, 8,2, 32,$e0,$bb,$e0,$bb,0, 13,  2, 32,0, 0	;chinchillader
;	db 11,1,16, 88,1,6,1,1, 8,2,-32,$a0,$be,$a0,$be,1, 88,120,  4,0, 4	;ave abajo

	db 12,1,12, 88,1,5,1,1, 8,2,-32,$a0,$be,$a0,$be,1, 88,128,  4,0, 4	

	db 13,1,22, 96,1,10,2,2,16,2,-64,$e0,$bd,$e0,$bd,1, 96,128, 8,0, 8	;ara�a

	db 14,1,14,136,1,6,2,2, 8,2, 32,$e0,$bb,$e0,$bb,0, 26, 14, 32,0, 0	;chinchillader

	db 15,1, 6,136,1,4,2,2, 8,2, 32,$e0,$bb,$e0,$bb,0, 13,  6, 32,0, 0	;chinchillader
	db 15,1,23,136,1,4,2,2, 8,2, 32,$40,$bc,$40,$bc,0, 23, 16, 32,1, 0	;chinchillaizq

	db 17,1,19,128,1,3,2,2, 8,2, 32,$80,$bd,$80,$bd,0, 19,  8, 64,1, 0	;murcielagoizq 

	db 18,1, 6,136,1,3,2,2, 8,2, 32,$e0,$bb,$e0,$bb,0, 19,  6, 32,0, 0	;chinchillader
	db 18,1,25, 96,1,4,2,2,16,2,-64,$e0,$bd,$e0,$bd,1, 96,128,  8,0, 8	;ara�a

	db 20,1,14,104,1,5,1,1, 8,2,-32,$c0,$be,$c0,$be,1,104,136,  4,0, 4	;ave arriba

	db 21,1, 9, 88,1,3,2,2, 8,2, 32,$e0,$bb,$e0,$bb,0, 21,  9, 32,0, 0	;chinchillader
	db 21,1,23, 72,1,3,1,1, 8,2,-32,$c0,$be,$c0,$be,1, 72,136,  4,0, 4	;ave arriba

	db 22,1,12,120,1,3,2,2, 8,2, 32,$20,$bd,$20,$bd,0, 19, 12, 64,0, 0	;murcielagoder 
	db 22,1,27,120,1,3,2,2, 8,2, 32,$80,$bd,$80,$bd,0, 27, 20, 64,1, 0	;murcielagoizq 

	db 23,1,26,144,1,3,2,2, 8,2, 32,$e0,$bb,$e0,$bb,0, 26, 10, 32,0, 0	;chinchillader
;	db 23,1,24, 88,1,3,1,1, 8,2,-32,$c0,$be,$c0,$be,1, 88,120,  4,0, 4	;ave arriba

	db 24,0,27,144,1,3,1,1, 8,2, 32,$e0,$bc,$e0,$bc,0, 27,  0,128,1, 0	;flechaizq

	db 25,1,26, 96,1,4,2,2, 8,2, 32,$20,$bd,$20,$bd,0, 26, 20, 64,0, 0	;murcielagoder 
;	db 25,1,12, 96,1,4,2,2, 8,2, 32,$80,$bd,$80,$bd,0, 18, 12, 64,1, 0	;murcielagoizq
	db 25,1,25,144,1,4,2,2, 8,2, 32,$40,$bc,$40,$bc,0, 25, 14, 32,1, 0	;chinchillaizq

	db 27,1, 8,104,1,4,2,2, 8,2, 32,$20,$bd,$20,$bd,0, 14,  8, 64,0, 0	;murcielagoder 
	db 27,1,21,104,1,4,2,2, 8,2, 32,$80,$bd,$80,$bd,0, 21, 15, 64,1, 0	;murcielagoizq

	db 28,1,10, 64,1,4,2,2,16,2,-64,$e0,$bd,$e0,$bd,1, 64,112,  8,0, 8	;ara�a

	db 29,1,18,128,1,3,2,2, 8,2, 32,$20,$bd,$20,$bd,0, 25, 18, 64,0, 0	;murcielagoder 
	db 29,1, 6, 72,1,4,2,2,16,2,-64,$e0,$bd,$e0,$bd,1, 72,112,  8,0, 8	;ara�a

	db 30,0, 5, 48,1,3,0,0, 8,1,-16,$f0,$be,$f0,$be,1, 48, 88,  1,1, 4	;gota
	db 30,1, 6, 88,1,4,2,2, 8,2, 32,$e0,$bb,$e0,$bb,0, 13,  6, 32,0, 0	;chinchillader
	db 30,1,23, 88,1,4,2,2, 8,2, 32,$40,$bc,$40,$bc,0, 23, 16, 32,1, 0	;chinchillaizq

	db 31,1,16, 40,1,2,0,0, 8,1, 16,$e0,$be,$e0,$be,1, 40, 88,  2,1, 4	;bola
	db 31,1, 9, 80,1,3,1,1, 8,2,-32,$c0,$be,$c0,$be,1, 48, 80,  4,0, 4	;ave arriba

	db 32,0,29, 88,1,3,1,1, 8,2, 32,$e0,$bc,$e0,$bc,0, 29,  0,128,1, 0	;flechaizq

	db 33,1, 3,112,1,4,2,2, 8,2, 32,$20,$bd,$20,$bd,0, 10,  3, 64,0, 0	;murcielagoder 
	db 33,1,22,112,1,3,2,2, 8,2, 32,$80,$bd,$80,$bd,0, 22, 14, 64,1, 0	;murcielagoizq

	db 34,1,27, 96,1,4,2,2, 8,2, 32,$40,$bc,$40,$bc,0, 27, 18, 32,1, 0	;chinchillaizq
	db 34,1, 8, 88,1,6,2,2,16,2,-64,$e0,$bd,$e0,$bd,1, 88,136,  8,0, 8	;ara�a

	db 35,1,14, 88,1,2,2,2, 8,2, 32,$80,$bd,$80,$bd,0, 14,  4, 64,1, 0	;murcielagoizq
	db 35,1,26, 96,1,5,1,1, 8,2,-32,$c0,$be,$c0,$be,1, 96,144,  4,0, 4	;ave arriba

	db 36,1,10,104,1,8,2,2,16,2,-64,$e0,$bd,$e0,$bd,1,104,128,  8,0, 8	;ara�a

	db 37,0, 4,144,1,3,1,1, 8,2, 32,$a0,$bc,$a0,$bc,0, 27,  4,128,0, 0	;flechader
	db 37,1,14, 48,1,8,2,2,16,2,-64,$e0,$bd,$e0,$bd,1, 48,104,  8,0, 8	;ara�a

	db 38,0, 6, 56,1,3,0,0, 8,1,-16,$f0,$be,$f0,$be,1, 56,112,  1,1, 4	;gota
	db 38,0,16,144,1,3,1,1, 8,2, 32,$e0,$bc,$e0,$bc,0, 16,  6,128,1, 0	;flechaizq

	db 39,0, 8,144,1,3,1,1, 8,2, 32,$a0,$bc,$a0,$bc,0, 27,  8,128,0, 0	;flechader

	db 40,1, 4, 72,1,2,2,2, 8,2, 32,$80,$bd,$80,$bd,0, 22,  4, 64,1, 0	;murcielagoizq

	db 41,1, 4,128,1,4,2,2, 8,2, 32,$20,$bd,$20,$bd,0, 12,  4, 64,0, 0	;murcielagoder 
	db 41,1,26,112,1,4,2,2, 8,2, 32,$80,$bd,$80,$bd,0, 26, 18, 64,1, 0	;murcielagoizq

	db 42,1,15, 72,1,6,2,2,16,2,-64,$e0,$bd,$e0,$bd,1, 72,112,  8,0, 8	;ara�a
	db 42,1,24,136,1,4,2,2, 8,2, 32,$e0,$bb,$e0,$bb,0, 27, 24, 32,0, 0	;chinchillader
	db 42,1, 5,136,1,4,2,2, 8,2, 32,$e0,$bb,$e0,$bb,0,  5,  2, 32,0, 0	;chinchillader

	db 43,1,17, 88,1,4,0,0, 8,1,-16,$e0,$be,$e0,$be,1, 88,128,  2,1, 4	;bola
	db 43,1, 8, 72,1,4,2,2,16,2,-64,$e0,$bd,$e0,$bd,1, 72,128,  8,0, 8	;ara�a
	db 43,1,25, 64,1,6,2,2,16,2,-64,$e0,$bd,$e0,$bd,1, 64,104,  8,0, 8	;ara�a


	db 44,0,26, 80,1,3,0,0, 8,1,-16,$f0,$be,$f0,$be,1, 80,136,  1,1, 4	;gota
	db 44,1, 7, 80,1,6,2,2,16,2,-64,$e0,$bd,$e0,$bd,1, 80,128,  8,0, 8	;ara�a

	db 45,1,25,120,1,3,1,1, 8,2,-32,$c0,$be,$c0,$be,1, 48,120,  4,0, 4	;ave arriba

	db 46,0,28, 80,1,3,0,0, 8,1,-16,$f0,$be,$f0,$be,1, 80,120,  1,1, 4	;gota

	db 47,1,16, 72,1,4,2,2, 8,2, 32,$e0,$bb,$e0,$bb,0, 26, 16, 32,0, 0	;chinchillader
	db 47,1, 8, 88,1,3,2,2,16,2,-64,$e0,$bd,$e0,$bd,1, 88,128,  8,0, 8	;ara�a
	db 47,1,18, 88,1,6,2,2,16,2,-64,$e0,$bd,$e0,$bd,1, 88,128,  8,0, 8	;ara�a

	db 48,1, 4,128,1,4,2,2, 8,2, 32,$20,$bd,$20,$bd,0, 14,  4, 64,0, 0	;murcielagoder 
	db 48,1,27,128,1,4,2,2, 8,2, 32,$80,$bd,$80,$bd,0, 27, 17, 64,1, 0	;murcielagoizq

	db 49,1,22, 88,1,4,0,0, 8,1,-16,$e0,$be,$e0,$be,1, 88,112,  2,1, 4	;bola

	db 50,1,21, 72,1,4,2,2, 8,2, 32,$20,$bd,$20,$bd,0, 27, 21, 64,0, 0	;murcielagoder 

	db 51,0,21,112,1,3,0,0, 8,1,-16,$f0,$be,$f0,$be,1,112,144,  1,1, 4	;gota
	db 51,1, 9, 56,1,4,2,2, 8,2, 32,$e0,$bb,$e0,$bb,0, 16,  9, 32,0, 0	;chinchillader

;	db 52,1,11, 32,1,5,2,2,16,2,-64,$e0,$bd,$e0,$bd,1, 32, 80,  8,0, 8	;ara�a
	db 52,1,26,104,1,4,2,2, 8,2, 32,$80,$bd,$80,$bd,0, 26, 13, 64,1, 0	;murcielagoizq

	db 53,0,24, 64,1,3,0,0, 8,1,-16,$f0,$be,$f0,$be,1, 64,120,  1,1, 4	;gota
	db 53,1,11, 56,1,4,1,1, 8,2,-32,$a0,$be,$a0,$be,1, 56,104,  4,0, 4	;ave abajo
	db 53,1,18, 64,1,8,2,2,16,2,-64,$e0,$bd,$e0,$bd,1, 64,104,  8,0, 8	;ara�a

	db 54,0,24, 48,1,3,0,0, 8,1,-16,$f0,$be,$f0,$be,1, 48,144,  1,1, 4	;gota
	db 54,0,12,144,1,3,1,1, 8,2, 32,$a0,$bc,$a0,$bc,0, 24, 12,128,0, 0	;flechader

	db 55,1,21, 56,1,4,0,0, 8,1,-16,$e0,$be,$e0,$be,1, 56,120,  2,1, 4	;bola

	db 56,1,25, 48,1,4,0,0, 8,1,-16,$e0,$be,$e0,$be,1, 48,128,  2,1, 4	;bola
;	db 56,1, 8, 56,1,4,0,0, 8,1,-16,$e0,$be,$e0,$be,1, 56, 96,  2,1, 1	;bola
	db 56,1,18,112,1,4,2,2, 8,2, 32,$80,$bd,$80,$bd,0, 18,  6, 64,1, 0	;murcielagoizq

;	db 255	no hace falta