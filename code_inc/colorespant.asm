; 
; Rutina de fade out de colores
;
fadeout_color:
	ld	hl,pausacolor		; valor inicial = 8
;fadeout_color2:
	dec	[hl]
	ret	z
	ld	a,[hl]
; 22528 + 4*32 = 128
	ld	hl,22528+128
	ld	de,22528+129
	ld	bc,511
	ld	[hl],a
	ldir
	ld	b,10
apausacolor:
; repetici�n de todo
	push	bc
	ld	a,1
	ld	[delaypausa],a
afadeoutcolortime:
; bucle 1 de pausa
	ld	b,255
fadeoutcolortime:
	djnz	fadeoutcolortime
; bucle 2 de pausa
	ld	hl,delaypausa
	rl	[hl]
	jr	nc,afadeoutcolortime
	pop	bc
	djnz	apausacolor
	xor	a	; anulo el m�s que probable acarreo de las operaciones anteriores
	jr	fadeout_color
pausacolor	defs	1
delaypausa	defs	1