;--------------------------------------------------------------------------------------------------------
;Rutina de borrado de barra de energ�a
;--------------------------------------------------------------------------------------------------------

; Esta rutina discrimana el da�o seg�n el enemigo OBVIANDO LAS ANIMACIONES
quita_energia:

;
;Si es una llamarada o un rayo quitar el m�ximo de da�o
;
		ld	a,[ix+18]
		and	a
		jr	z,da_o_4
;
; aqui hay que discriminar seg�n el tama�o del enemigo que te ha tocado para borrar 2,4 u 8 pixels
;

		ld	a,[ix+9]
		cp	2		; 1 o 2 de ancho
		jr	c,da_o_1
		ld	a,[ix+8]
		cp	24
		jr	nz,da_o_2
da_o_4:
		call	desp_un_pixel	; es da�o de fuerza 4. solo el Nazgul y Delcram
		ld	hl,offbarra
		dec	[hl]
		call	z,bofb

		call	desp_un_pixel	; es da�o de fuerza 4. solo el Nazgul y Delcram
		ld	hl,offbarra
		dec	[hl]
		call	z,bofb

da_o_2:
		call	desp_un_pixel	; da�o 2
		ld	hl,offbarra
		dec	[hl]
		call	z,bofb
da_o_1:
		call	desp_un_pixel	; da�o 1
		ld	hl,offbarra
		dec	[hl]
		call	z,bofb
		ret
bofb:
		ld	a,8
		ld	[offbarra],a
		ld	hl,[barra]	; posic. del char de la barra actual
		dec	l
                ld	a,l
                cp	#69                  ;Si la barra ha llegado al final $4069
                jr	z,bbofb
                ld	[barra],a
                ld	a,h
                ld	[barra+1],a
                ret
bbofb:
;
; Activamos la muerte de Eshur
;
		xor	a
                ld	[vidas],a           ;Vida = 0. Tas morio
                ret
desp_un_pixel:
		push	hl
		ld	hl,[barra]	; posic. del char de la barra actual
		sla	[hl]
		inc	h
		sla	[hl]
		inc	h
		sla	[hl]
		inc	h
		sla	[hl]
		inc	h
		sla	[hl]
		inc	h
		sla	[hl]
		inc	h
		sla	[hl]
		inc	h
		sla	[hl]
		inc	h
		pop	hl
		ret
;--------------------------------------------------------------------------------------------------------
; Rutina de aumento de barra de energ�a al coger las calaveras
;--------------------------------------------------------------------------------------------------------
Aumentador_Energia:
		ld	a,8
		ld	[offbarra],a
		ld	b,2	; que son los char's que se aumentan al recoger una calavera
		ld	de,[barra]
asiaumen_tando:
		ld	hl,tab_aum_barra
		push	bc
		push	de
		ld	b,8
siaumen_tando:
		ld	a,[hl]
		ld	[de],a
		inc	d
		inc	hl
		djnz	siaumen_tando
		pop	de
		pop	bc
		ld	a,e
		cp	$74		; m�xima energia de la barra
		ret	z
		inc	e		; siguiente char a aumentar
		ld	a,e
		ld	[barra],a	; guardo la nueva posicion.
		djnz	asiaumen_tando
		ld	hl,barra
		dec	[hl]		; para ajustar el valor de la barra a lo aumentado
		ret
; Tabla en Tablasvarias.asm
; 
; datos de la barra a imprimir
;
;tab_aum_barra:
;	DEFB	  0,136,  0,119,170,128,  8,  0
