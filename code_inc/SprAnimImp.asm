;----------------------------------------------------------------------------------------
; Calculo de la direcci�n del prota en el archivo de pantalla
;----------------------------------------------------------------------------------------
; ix+2 colu en chars, ix+3 fila en pixels SALIDAS: HL y dir_spr_e
im_pix_a:							; 45869
        ld      a,[ix+3]	;fila en scannes o pixels
        ld      l,a
        ld      a,[ix+2]	;colu en chars (para usar pixels pasar previam a char)
        call    tabla4metal
        ld      [dir_spr_a],HL    
 ;       ret	No es necesario ya que no vamos a usar avolcamiento_anim en SprAnimVolc.asm
;----------------------------------------------------------------------------------------
;RUTINA DE CALCULO DE LA DIRECCION EN EL BUFFER INTERMEDIO DADAS LA FILA Y LA COLUMNA
;ENTRADA:FILA pixels Y COLU en chars   SALIDA:Posicion del sprite en el buffer intermedio 
;----------------------------------------------------------------------------------------
c_dirbuff_a:							; 45883
	xor	a		; acarreo a 0
	ld	e,a		; 1/4
        ld	a,[ix+3]	; 4/13
	rra			; 1/4
	rr	e		; 2/8
	rra			; 1/4
	rr	e		; 2/8
	rra			; 1/4
	rr	e		; 2/8
	ld	d,a		; 1/4
	ld	a,[ix+2]	; 4/13 
	add	a,e
	ld	e,a
	ld	hl,$ed90-$400	; 3/10 buffer $ed90 tama�o marcador de arriba 1024= $400
	add	hl,de           ; 3/11
	ld	[d_bu_spr_a],hl	; SALIDAS: HL y d_bu_spr_e
        ret			; 3/10. Total = 108 t-states.
;----------------------------------------------------------------------------------------
; IMPRESION DE SPRITE.
;----------------------------------------------------------------------------------------
imp_ani:							; 45911
        ld      de,[dir_spr_a]	; direccion del sprite en la videoram
	ld	hl,[spr_a]	; direccion de sprite a imprimir
evaluamov_a:
	ld	a,[ix+8]
	ld      c,a             ; paso el valor a c
loopanim:
        push    de
	ld	a,[ix+9]
	ld	b,a		; ancho
buloopanim:
        ld      a,[de]          ; Coge lo de la pantalla
        and     [hl]            ; Borro lo que no sean 1. Enmascaramiento
        inc     hl
        or      [hl]            ; Le sumo el sprite
        ld      [de],a		; guarda en pantalla
        inc     hl
        inc     e		; aumenta posici�n pantalla 1 a la derecha
        djnz    buloopanim
        pop     de		; posicion de pantalla original
        inc     d		; aqu� viene la rutina de bajar scan de toda la vida
        ld      a,d
        and     7
        jr      nz,sal2ani
        ld      a,e
        add     a,32
        ld      e,a
        jr      c,sal2ani
        ld      a,d
        sub     8
        ld      d,a
sal2ani:
        dec     c
        jr      nz,loopanim
        ret
