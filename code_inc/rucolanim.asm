;----------------------------------------------------------------------------------------
; Colocamos las animaciones de cada pantalla. Tabanimaciones.asm
;----------------------------------------------------------------------------------------
ponanimaciones:
	ld	de,20
	ld	hl,TABANIM -20		; TABLA DE ANIMACIONES DE LAS PANTALLAS. 
	ld	a,[panta]
	ld	b,a
bponanimac:
;bponerobj1:
	add	hl,de
	ld	a,[hl]			
	cp	255			; fin tabla
	ret	z
	cp	b			; pantalla
	jr	nz,bponanimac
;
; animaciones encontradas
;
	ld	de,ANIMAC		; TABLA DE ANIMACION EN PANTALLA
sig_animac:
	push	bc
	ld	bc,20
	ldir
	pop	bc			; solo hay 2 animaciones por pantalla
	ld	a,[hl]			
	cp	b
	jr	z,sig_animac
;
; activo la animacion
;
	ld	hl,miraranimac
ret_animac:
	ld	[vermiraranimaciones+1],hl		; Activo mirar animaciones
	ret
;
; Rutina para ''anular las animaciones''
;
; Nota: no guardar las nuevas posiciones de las animaciones sino que empiecen de 0
; El motivo fundamental es la gesti�n de la gota al caer desde la gestion de enemigos
; y el que pueda aparecer la animaci�n antes de que caiga, al pasar de pantalla.
; En la rutina de gesti�n de enemigos no guardaremos los datos de la gota cayendo.
; Por lo tanto no se hace necesario guardar los datos de las animaciones.
;
quitanimaciones:
	ld	a,255
	ld	hl,ANIMAC		; TABLA DE ANIMACION EN PANTALLA
	ld	[hl],a
	ld	hl,ANIMAC+20
	ld	[hl],a
;
; desactivo la animacion
;
	ld	hl,nomiraranimac
	jr	ret_animac
