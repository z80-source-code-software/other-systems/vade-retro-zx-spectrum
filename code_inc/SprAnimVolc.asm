;----------------------------------------------------------------------------------------
; Calculo de la posic.en pant. y en el buffer del enemigo o la plataforma 
;----------------------------------------------------------------------------------------
;avolcamiento_anim:
;        call    im_pix_a      ; hl=dir_spr POSIC.ARCH.PANT       40890    SprProtaImp.asm
;	call    c_dirbuff_a   ; hl=d_bu_spr Posic. en buffer
;	ret
;----------------------------------------------------------------------------------------
; Rutina de volcado del fondo de pantalla que ocupa el sprite. Los enemigos tienen 
; que tener un m�ximo de 2 char de ancho para esta rutina o descomentar las lineas para 3
;----------------------------------------------------------------------------------------
vuelcafondospranim:						;40897
	ld	hl,[d_bu_spr_a]	; viene de c_dirbuff
	ld	de,[dir_spr_a]	; viene de im_pix	
;
; Necesitamos cargar en C el tama�o sprite  ancho x (altosc o altochar (x8))  
; para ello rotamos el ancho y sabremos cuantas veces sumar el alto en escannes
;
	ld	a,[ix+8]	; altosc
	ld	c,a		; lo guardo
	ld	a,[ix+9]	; ancho en char
	ld	b,a		; lo guardo para luego
	rra			; rotamos al carry
	jr	c,companchoan	; si 1deancho c=altosc
	ld	a,c		; si 2deancho c=altosc+altosc
	add	a,c		; 
	ld	c,a		; guardo el dato en c para despu�s
;
; Ahora necesito saber que ancho tiene para discriminar entre subrutinas.
; Los enemigos tienen 1 o 2 de ancho.
;
companchoan:
	ld	a,b		; saco el valor del ancho guardado arriba
	cp	1
	jr	z,anim1ancho
;
; sprite de 2 ancho
;
	ld	b,0
lazoavolcan2:
	ldi
	ldi
	ret	po
	ld	a,c
;	and	a
;	ret	z
	ld	c,30		;posicion imediata debajo del scann anterior
	add	hl,bc
	ld	c,a
	dec	de	;con esto queda resuelto el que la colu provoque un acarreo
	dec	e
	inc	d
        ld      a,d
        and     7
        jr      nz,lazoavolcan2  ;baja una l�nea dentro del caracter
        ld      a,#20
        add     a,e
        ld      e,a
        jr      c,lazoavolcan2    ;cambio de tercio
        ld      a,d
        sub     8
        ld      d,a
        jr      lazoavolcan2       ;cambio de caracter
;
; sprite de 1 ancho
;
anim1ancho:
	ld	b,0
lazoavolcan1:
	ldi
	ret	po
	ld	a,c
;	and	a
;	ret	z
	ld	c,31		;posicion imediata debajo del scann anterior
	add	hl,bc
	ld	c,a
	dec	de	;con esto queda resuelto el que la colu provoque un acarreo
	inc	d
        ld      a,d
        and     7
        jr      nz,lazoavolcan1  ;baja una l�nea dentro del caracter
        ld      a,#20
        add     a,e
        ld      e,a
        jr      c,lazoavolcan1    ;cambio de tercio
        ld      a,d
        sub     8
        ld      d,a
        jr      lazoavolcan1       ;cambio de caracter
