;----------------------------------------------------------------------------------------
; Rutinas de volcado de pantalla desde el buffer.
;----------------------------------------------------------------------------------------
; Volcado del buffer pantalla y atributos a la pantalla de juego
volcar_pant:
        ld      de,$4080        ; Inicio Pantalla de juego  39135
	ld      hl,$ed90        ; Inicio Buffer de pantalla
        ld      bc,4096
otralinea:
        push    de
        ldi
        ldi
        ldi
        ldi
        ldi
        ldi
        ldi
        ldi
        ldi
        ldi
        ldi
        ldi
        ldi
        ldi
        ldi
        ldi
        ldi
        ldi
        ldi
        ldi
        ldi
        ldi
        ldi
        ldi
        ldi
        ldi
        ldi
        ldi
        ldi
        ldi
        ldi
        ldi
        pop    de
        inc    d
        ld     a,d
        and    7
        jr     nz,otralinea
        or     b
        jr     z,vol_caratt
        ld     a,$20
        add    a,e
        ld     e,a
        jr     c,otralinea         ; cambio de tercio
        ld     a,d
        sub    8
        ld     d,a
        jr     otralinea           ; cambio de caracter
;----------------------------------------------------------------------------------------
; hl ya est� en su posici�n en el buffer de attributos.
vol_caratt:
        ld     de,$5880
	ld     bc,512
        ldir
        ret
