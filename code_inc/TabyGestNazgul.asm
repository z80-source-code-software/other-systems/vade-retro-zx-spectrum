;----------------------------------------------------------------------------------------
; Tabla del enemigo especial. BL-Nazg�l
;----------------------------------------------------------------------------------------
; Defb's con los siguientes datos

; Interv,activo, posx,  posy, ticks, frameskip, frameActual, framestotal, altosc, anchochars,
; ix0,	    ix1,    ix2,   ix3,	 ix4,	ix5,		ix6,	ix7,	  ix8,  	ix9

; tama�o1frame, dblowEnem, dbhighEnem, dblowEnemIni,dbhighEnemIni, direccion, ticks_intervalo,
;	ix10,	ix11,		ix12,  ix13,		ix14,	   ix15,	ix16

; frameskip_intervalo, codanim, sentido, desplaz,dblowPant, dbhighPant, dblowBuff, dbhighBuff, choque
; ix17,			 ix18,	ix19	 ix20	 ix21		ix22	ix23		ix24	ix25

; No necesitamos panta ni activo el enemigo en principio va a 'presentarse' solo de la 0 a la 26
; ix0 + ix1  ser� la variable intervalo y activo. 

; no necesitamos limite1 y limite2 asi que la reconvertimos
; ix16 ser� la variable de ticks de intervalo 
; ix17 ser� la variable de frameskip de intervalo 

; Se ha intentado usar las posiciones en IX del resto de los enemigos para usar las mismas rutinas
; de calculo (avolcamientoenemigo) impresi�n y calculo/volcado en el buffer

; framestotal es tonteria tenerlo, sabes que siempre es fija. Pero si no necesito el db lo dejar�.
; desplaz. ix+20 tampoco lo uso al igual que codanim ix+18 y que direccion ix+15

;NAZGUL:
	;	0,1,2,  3,4,5,6,7, 8,9,10, 11, 12, 13, 14,15, 16, 17,18,19,20,21,22,23,24,25
;	db	0,0,1,120,1,8,2,2,24,2,96,$a0,$b9,$a0,$b9, 0,200,200, 0, 0, 4, 0, 0, 0, 0,0


GESTION_NAZGUL:
	push	ix
	ld	ix,NAZGUL
;
; Nazgul Activo ? Por ejemplo si lo has muerto ya en esa pantalla
;
	ld	a,[ix+1]
	and	a
	jp	z,finNAZGUL
;
; intervalo activo S/N
;
	ld	a,[ix+0]
	and	a
	jp	z,inc_intervalo_nazgul
;
; contador de ticks			Nazgul_activado
;
	ld	a,[ix+4]	
	dec	a
	ld	[ix+4],a
	jp	nz,imprime_nazgul
;
; pongo el skipframe del db's en ticks
;
	ld	a,[ix+5]		
	ld	[ix+4],a
;
; Comprobamos si es el �ltimo frame
;
	ld	a,[ix+6]	; frameActual
	cp	2
	jr	z,ponframe_nazgul_0
;
; Incremento el valor del frame
;
	inc	a
	ld	[ix+6],a
	call	vuelcafondosprenem	; vuelco el fondo del enemigo
	ld	a,[ix+10]	; el tama�o de nazgul varia si esta activo o muerto
	ld	c,a
	ld	b,0
	ld	l,[ix+11]	; dblowAnim
	ld	h,[ix+12]	; dbhighAnim
	add	hl,bc
	ld	[ix+11],l
	ld	[ix+12],h	; guardo el valor para el pr�ximo frame
;
; Miramos el valor de Y para saber por donde anda el prota
;
	ld	b,[ix+3]	; posY nargul
	ld	a,[fila]	; prota
	cp	b
	jr	c,nargul_parriba
	jp	z,imprime_nazgul
nargul_pabajo:
	ld	a,4
	add	a,b
	ld	[ix+3],a
	jr	bnargul_parriba
nargul_parriba:
	ld	a,b
	sub	4
	ld	[ix+3],a
bnargul_parriba:
	jr	aimprime_nazgul
;
; Pongo el frame a 0
;
ponframe_nazgul_0:	
	call	vuelcafondosprenem	; vuelco el fondo de la animaci�n
;
; La muerte del enemigo est� activada ?
;
	ld	a,[ix+25]
	and	a
	jr	z,bpframe_nazgul0
	ld	[ix+1],0		; desactivo el enemigo para no seguir imprimiendolo
	ld	hl,NO_LLAMA_NAZGUL
	ld	[Pre_Nazgul],hl
	jp	finNAZGUL
bpframe_nazgul0:
	ld	[ix+6],0
;
; Inicializo los frames a pesar de no saber si va a darse la vuelta el sprite.
;
	ld	a,[ix+13]
	ld	[ix+11],a	; low
	ld	a,[ix+14]
	ld	[ix+12],a	; high
;
; Rutina para comprobar donde est� el prota y actuar sobre la horizontal
;
	ld	b,[ix+2]		; por defecto la �ltima que tuvo
	ld	a,[colu]
	cp	b
	jr	c,P_a_izq_de_N
; incremento columna nargul
	ld	a,[ix+2]
	inc	a
	ld	[ix+2],a
	ld	a,[ix+19]		; que sentido llevas ?
	cp	1
	jr	nz,aimprime_nazgul	;finNAZGUL		; no cambio sentido ni db's del frame
;
; cambio sentido de Nazgul y db de inicio del frame para ir a derecha
;
	ld	[ix+19],0
	ld	[ix+11],$a0		; dblow nazgul a der
	ld	[ix+13],$a0
	ld	[ix+12],$b9		; dbhigh nazgul a der
	ld	[ix+14],$b9
	jr	aimprime_nazgul	;afinNAZGUL		; si todo es correcto aprovecho esta etiq.
P_a_izq_de_N:
; decrementar columna nargul
	ld	a,[ix+2]
	dec	a
	ld	[ix+2],a
	ld	a,[ix+19]		; que sentido llevas ?
	and	a
	jr	nz,aimprime_nazgul	;finNAZGUL		; no cambio sentido ni db's del frame
;
; cambio sentido de Nazgul y db de inicio del frame para ir a izquierda
;
	ld	[ix+19],1
	ld	[ix+11],$c0		; dblow nazgul a izq
	ld	[ix+13],$c0	
	ld	[ix+12],$ba		; dbhigh nazgul a izq
	ld	[ix+14],$ba
aimprime_nazgul:
	call	avolcamiento_enem
;
; Imprimo el enemigo. Pongon en hl el valor del sprite a imprimir
;
imprime_nazgul:
;
; Modifico el valor del registro B en SprEnemImp.asm para que sea el ancho del sprite actual.
;
	exx
	ld	hl,chganchospen+1
	ld	[hl],2		; Nazgul es de 2 de ancho
	exx
	call	imp_ene		; imprimo el enemigo
;
; Si el enemigo ya est� imprimiendose SU MUERTE no vuelvas a comprobar el choque
;
	ld	a,[ix+25]
	and	a
	jr	Nz,finNAZGUL	; esta activa la animaci�n de la muerte del enemigo
;
; inmunidad Activada ?
;
	ld	a,[inmunidad]
	and	a
	jr	nz,finNAZGUL
;
; Detectar choques con el prota
;
	call	det_choques_enemigos
;
; comprobacion de si has matado a Nazgul
;
	ld	a,[vchoqEnemesp]	; para detchoquesF3B la variable es vchoqEnem
	and	a
	jr	z,finNAZGUL
;
; si hay choque llama a la rutina de poner la animaci�n del enemigo muerto
;
	call	ponmuerteEnemigoNazgul
	xor	a
	ld	[vchoqEnemesp],a
	jr	finNAZGUL
;
; incrementar el ticks_intervalo
;
inc_intervalo_nazgul:
	ld	a,[ix+16]
	add	a,1
	jr	c,pon_intervalo_nazgul_1
	ld	[ix+16],a
	jr	finNAZGUL
;
; Activo el intervalo para empezar con la animaci�n.
;
pon_intervalo_nazgul_1:
	ld	[ix+0],1
	ld	a,[ix+17]		; cojo frameskip_intervalo y lo copio en ticks_intervalo
	ld	[ix+16],a		; restauro el valor para la pr�xima vez.
;
; Ahora calculo donde debo sacar a NAZGUL. Seg�n posici�n del prota al entrar en pantalla
;
	ld	a,[dirmovsp]
	and	a
	jr	z,nargulsaleporab	; prota saltaba. sale por abajo
	cp	1
	jr	z,nargulsaleporiz	; prota sali� por la derecha. Aparece a la izq.pant.
	cp	2
	jr	z,nargulsaleporar
; nargul sale por la der.pant.
	ld	[ix+2],29
	ld	[ix+11],$c0		; dblow nazgul a izq
	ld	[ix+13],$c0	
	ld	[ix+12],$ba		; dbhigh nazgul a izq
	ld	[ix+14],$ba
	jr	finNAZGUL
nargulsaleporiz:
	ld	[ix+2],1
	jr	afinNAZGUL		; las direcciones arriba y abajo
nargulsaleporab:
	ld	[ix+3],120
	jr	afinNAZGUL		; se pinta igual que saliendo a derechas
nargulsaleporar:
	ld	[ix+3],32
afinNAZGUL:
	ld	[ix+11],$a0		; dblow nazgul a der
	ld	[ix+13],$a0
	ld	[ix+12],$b9		; dbhigh nazgul a der
	ld	[ix+14],$b9
finNAZGUL:
	pop	ix
	ret
;
; Gestion de cambio de pantalla de NAZGUL
;

chg_pant_nazgul:
; dB'S de los datos de Nazgul que cambiamos cuando desaparece por toque con el prota.
	ld	ix,NAZGUL
	ld	[ix+6],2	; frameactual
	ld	[ix+8],24	; altosc
	ld	[ix+10],96	; tama�o
	ld	[ix+25],0	; detecci�n de choque con prota
; Que sentido tiene el prota ? 1 va a derechas 3 va a izquierdas
	ld	a,[dirmovsp]
	cp	1
	jr	z,Naz_mirara_der
; Naz_mirara_izq
	ld	[ix+11],$c0	; dblow
	ld	[ix+12],$ba	; dbhigh
	ld	[ix+19],1	; ve a izquierdas
	jr	B_Naz_mirara_der
Naz_mirara_der:	
	ld	[ix+11],$a0	; dblow
	ld	[ix+12],$b9	; dbhigh
	ld	[ix+19],0	; ve a derechas
B_Naz_mirara_der:
	ld	hl,NAZGUL		; desactivo intervalo
	ld	[hl],0
	inc	hl
	ld	[hl],1			; activo el bicho
	ld	de,04
	add	hl,de
;
; pantallas de 0 a 5 = 6 frameskip y 150 frameskip_intervalo
; pantallas de 6,7,8 = 8 frameskip y  50 frameskip_intervalo
;
	ld	a,[panta]
	cp	5
	jr	c,intN_1
	ld	[hl],8		; frameskip
	ld	de,11
	add	hl,de
	ld	[hl],75	; ticks_intervalo
	inc	hl
	ld	[hl],75	; frameskip_intervalo
	jr	fin_chg_pant_nazgul
intN_1:
	ld	[hl],6		; frameskip
	ld	de,11
	add	hl,de
	ld	[hl],175	; ticks_intervalo
	inc	hl
	ld	[hl],175	; frameskip_intervalo

fin_chg_pant_nazgul:	
	ld	de,04
	add	hl,de		; borrar nazgul de la pantalla de video y buffer
	xor	a
	ld	[hl],a
	inc	hl
	ld	[hl],a
	inc	hl
	ld	[hl],a
	inc	hl
	ld	[hl],a
	ret