;----------------------------------------------------------------------------------------
; Rutina para limpiar el buffer de pantalla y pintar el buffer de atributos de amarillo 
limpiobuffer
	ld	hl,$ed90
	ld	de,$ed91
	ld	[hl],0
	ld	bc,4096		
	ldir
;pintabufferattr:  ld hl,$fd90; inicio buff attr     ld	de,$fd90+1            
	ld	a,6			; color de fondo amarillo
        ld	[hl],a
        ld	bc,511
        ldir                  
        ret
