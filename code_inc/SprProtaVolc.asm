;----------------------------------------------------------------------------------------
; Rutina de volcado del fondo de pantalla que ocupa el sprite.
;----------------------------------------------------------------------------------------
; Rutinas calculo en el buffer de la posic.del prota en pantalla
;avolcamiento:
;;      call    im_pix      ; hl=dir_spr POSIC.ARCH.PANTALLA      SprProtaImp.asm
;;	ret
vuelcafondosprprota:		; Sprite de 3 alto x2 ancho
	ld	hl,[d_bu_spr]	; viene de c_dirbuff
	ld	de,[dir_spr]	; viene de im_pix	
; parte optimizada por metalbrain para nelo y quqo
	ld	bc,48	; total ancho 2* alto3 *8
lazoavolc1:
	ldi
	ldi
; m�s optimizacion metal
	ret	po	; Hay Paridad cuando bc=0
	ld	a,c
;	and	a
;	ret	z
	ld	c,30		;posicion imediata debajo del scann anterior
; por supuesto para cualquier sprite de 3 de ancho
	add	hl,bc
	ld	c,a
	dec	de	;con esto queda resuelto el que la colu provoque un acarreo
	dec	e
	inc	d
        ld      a,d
        and     7
        jr      nz,lazoavolc1  ;baja una l�nea dentro del caracter
        ld      a,#20
        add     a,e
        ld      e,a
        jr      c,lazoavolc1    ;cambio de tercio
        ld      a,d
        sub     8
        ld      d,a
        jr      lazoavolc1       ;cambio de caracter

vuelcafondosprprotaEsp:		; Sprite de 3 alto por 3 ancho
	ld	hl,[d_bu_spr]	; viene de c_dirbuff
	ld	de,[dir_spr]	; viene de im_pix	
; parte optimizada por metalbrain para nelo y quqo
	ld	bc,72	; total ancho 3* alto3 *8
lazoavolc1Esp:
	ldi
	ldi
	ldi		; para 3 de ancho, comentar si son 2 de ancho
; m�s optimizacion metal
	ret	po	; Hay Paridad cuando bc=0
	ld	a,c
;	and	a
;	ret	z
	ld	c,29		;posicion imediata debajo del scann anterior
; por supuesto para cualquier sprite de 3 de ancho
	add	hl,bc
	ld	c,a
	dec	de	;con esto queda resuelto el que la colu provoque un acarreo
	dec	e	; para 3 de ancho si son 2 de ancho comentar esta linea
	dec	e
	inc	d
        ld      a,d
        and     7
        jr      nz,lazoavolc1Esp  ;baja una l�nea dentro del caracter
        ld      a,#20
        add     a,e
        ld      e,a
        jr      c,lazoavolc1Esp    ;cambio de tercio
        ld      a,d
        sub     8
        ld      d,a
        jr      lazoavolc1Esp       ;cambio de caracter
