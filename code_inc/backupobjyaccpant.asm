;----------------------------------------------------------------------------------------
; Recolocamos objetos en tabla_objetos y en la tabla de acciones
;----------------------------------------------------------------------------------------
backup_objetos:
	ld	hl,tabla_Objetos+1		; rucobjpant.asm
	ld	de,9
	ld	b,22
bbackup_objetos:	
	ld	[hl],1
	add	hl,de
	djnz	bbackup_objetos

	ld	hl,backup_tabla_Acciones
	ld	de,tabla_Acciones
	ld	bc,42
	ldir
	ret
