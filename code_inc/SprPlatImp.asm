;----------------------------------------------------------------------------------------
; Calculo de la direcci�n del plataforma en el archivo de pantalla
;----------------------------------------------------------------------------------------
; ix+2 colu en chars, ix+3 fila en pixels SALIDAS: HL y dir_spr_e
im_pix_p:							; 45869
        ld      a,[ix+3]	;fila en scannes o pixels
        ld      l,a
        ld      a,[ix+2]	;colu en chars (para usar pixels pasar previam a char)
        call    tabla4metal
        ld      [dir_spr_p],HL    
;        ret
;----------------------------------------------------------------------------------------
;RUTINA DE CALCULO DE LA DIRECCION EN EL BUFFER INTERMEDIO DADAS LA FILA Y LA COLUMNA
;ENTRADA:FILA pixels Y COLU en chars   SALIDA:Posicion del sprite en el buffer intermedio 
;----------------------------------------------------------------------------------------
;c_dirbuff_p:							; 45883
	xor	a		; acarreo a 0
	ld	e,a		; 1/4
        ld	a,[ix+3]	; 4/13
	rra			; 1/4
	rr	e		; 2/8
	rra			; 1/4
	rr	e		; 2/8
	rra			; 1/4
	rr	e		; 2/8
	ld	d,a		; 1/4
	ld	a,[ix+2]	; 4/13 
	add	a,e
	ld	e,a
	ld	hl,$ed90-$400	; 3/10 buffer $ed90 tama�o marcador de arriba 1024= $400
	add	hl,de           ; 3/11
	ld	[d_bu_spr_p],hl	; SALIDAS: HL y d_bu_spr_e
        ret			; 3/10. Total = 108 t-states.
;----------------------------------------------------------------------------------------
; IMPRESION DE SPRITE. Deben tener los sprites y plataformas de 2chars. ancho
;----------------------------------------------------------------------------------------
imp_pla:							; 45911
        ld      de,[dir_spr_p]	; direccion del sprite en la videoram
	ld	hl,[spr_p]	; direccion de sprite a imprimir
evaluamov_p:
	ld	c,8		; alto en scannes
looppla:
        push    de
	ld	b,2		; ancho
bulooppla:
        ld      a,[de]          ; Coge lo de la pantalla
        and     [hl]            ; Borro lo que no sean 1. Enmascaramiento
        inc     hl
        or      [hl]            ; Le sumo el sprite
        ld      [de],a		; guarda en pantalla
        inc     hl
        inc     e		; aumenta posici�n pantalla 1 a la derecha
        djnz    bulooppla
        pop     de		; posicion de pantalla original
        inc     d		; aqu� viene la rutina de bajar scan de toda la vida
        ld      a,d
        and     7
        jr      nz,sal2pla
        ld      a,e
        add     a,32
        ld      e,a
        jr      c,sal2pla
        ld      a,d
        sub     8
        ld      d,a
sal2pla:
        dec     c
        jr      nz,looppla
        ret
;----------------------------------------------------------------------------------------
;Rutina de Impresion de atributos de LA PLATAFORMA.
;----------------------------------------------------------------------------------------
impatrib_p:							; 45955
; Impresion de atributos en la Videoram Atrib.
	call	c_dirAtr_plat
	ld      b,1                ; alto del enemigo
sifattrp_p:
	push	hl
	ld      c,2	           ; ancho del enemigo
siattrpro_p:
        ld      [hl],70		   ; COLOR amarillo con brillo
        inc     hl
        dec     c
        jr      nz,siattrpro_p
	pop	hl
        ld      de,32
        add     hl,de
        djnz    sifattrp_p
        ret

; C�lculo direc.Videoram Atrib.. fila pixels columna en char
c_dirAtr_plat:							; 45989
	ld	a,[ix+3]	; 5/19 fila
	and	248		; 2/7 anulando valores intermedios para el calculo
	ld	c,a		; 1/4
	xor	a		; 1/4
	sla	c		; 2/8 desplazo la fila 2 veces a
	rla			; 1/4 izquierda rotando b
	sla	c		; 2/8 para que el resultado de dividir /8
	rla			; 1/4 y luego multiplicar x32 sea lo mismo.
	ld	b,a		; 1/4 fila en bc
	ld	a,[ix+2]	; 5/19 colu
	ld	l,a		; 1/4
	ld	h,$58		; 2/7 
	add	hl,bc		; 3/11 hl = direcci�n Atributos 103t-states
	ret
;----------------------------------------------------------------------------------------
; Calculo del attr.del fondo plataforma en el Buffer Attr.    fila pixels columna en char
;----------------------------------------------------------------------------------------
c_dirbuff_plat:							; 46011
; C�lculo direc.en el buffer Atrib.. 
	ld	hl,$fd90	; buffer de atributos.
	ld	a,[ix+3]	; 5/19 fila
	and	248		; 2/7 anulando valores intermedios para el calculo
	sub	32		; tama�o del marcador superior
	jr	z,sumacoluatt_plat
	ld	c,a		; 1/4
	xor	a		; 1/4
	sla	c		; 2/8 desplazo la fila 2 veces a
	rla			; 1/4 izquierda rotando b
	sla	c		; 2/8 para que el resultado de dividir /8
	rla			; 1/4 y luego multiplicar x32 sea lo mismo.
	ld	b,a		; 1/4 fila en bc
	add	hl,bc
sumacoluatt_plat:
	ld	a,[ix+2]	; 5/19 colu
	ld	c,a		; 1/4
	ld	b,0		; 2/7 
	add	hl,bc		; 3/11 hl = direcc.en Buffer Atrib. 103t-states
	ex	de,hl		; lo guardo en de
; imprimo el atributo en la pantalla
	call	c_dirAtr_plat	; hl = direc.Atributos en pantalla.
	ld	a,[de]		; de = direc.Atrib. en el buffer.Atrib.
	ld	[hl],a
	inc	l
	inc	e
	ld	a,[de]
	ld	[hl],a
	ret
