; Resto de datas del menu

letracontrol:
; (Insert graphic label here if needed)

	DEFB	  0,  0,  0,  0,  0,  0,  0
	DEFB	  0,  0,  0,  0,  0,  0,  0
	DEFB	  0, 56,  0,  0,  0,  1,  0
	DEFB	  0,114,  0,  8,  0,  0,128
	DEFB	  0,215,  0, 48,  0,  0, 64
	DEFB	 32, 50, 36,144,  0,  0, 96
	DEFB	 96, 20, 83, 84, 99, 52,176
	DEFB	 32, 56, 74, 90,212,154,144

	DEFB	 32, 20, 97, 82,147,144,144
	DEFB	 32, 38, 64,146,148,144,144
	DEFB	 48,115, 49, 44, 99, 81, 96
	DEFB	  0,  0,  2,  0,  0,  0,  0
	DEFB	  0,  0,  1,128,  0,  0,  0
	DEFB	  0,  0,  0,  0,  0,  0,  0
	DEFB	  0,  0,  0,  0,  0,  0,  0
	DEFB	  0,  0,  0,  0,  0,  0,  0

	DEFB	  0, 29,  0,  0,  0,  0, 57
	DEFB	  0, 35,  0,  2,128,  0, 71
	DEFB	  0, 66,  0,  1,  0,  0, 10
	DEFB	  0,104,  0,  1,  0,  0, 22
	DEFB	 96,188, 32,  1,  0, 32, 60
	DEFB	144,146, 10, 25, 12, 26, 20
	DEFB	 48,121, 93, 53, 18, 77, 14
	DEFB	 64, 37, 73, 49, 14, 72, 26

	DEFB	240,110, 73, 37, 18, 72, 10
	DEFB	128, 88, 77,152,205, 72, 10
	DEFB	  0,  0,  0,  0,  0,  0, 68
	DEFB	  0,  0,  0,  0,  0,  0, 57
	DEFB	  0,  0,  0,  0,  0,  0,  0
	DEFB	  0,  0,  0,  0,  0,  0,  0
	DEFB	  0,  0,  0,  0,  0,  0,  0
	DEFB	  0,  0,  0,  0,  0,  0,  0
; (Attributes here, insert label if needed)
	DEFB	 70, 69, 69, 69, 69, 69, 69
	DEFB	  6, 65, 65, 65, 65, 65, 65
	DEFB	 70, 68, 68, 68, 68, 68, 68
	DEFB	  6, 67, 67, 67, 67, 67, 67


cachoespada:
	DEFB	 25,176
	DEFB	 27,176
	DEFB	 25,176
	DEFB	 27,176
	DEFB	 25,176
	DEFB	 27,176
	DEFB	 27,176
	DEFB	 27,176

	DEFB	 27,176
	DEFB	 27,176
	DEFB	 27,176
	DEFB	 27,176
	DEFB	 27,176
	DEFB	 27,176
	DEFB	 27,176
	DEFB	 27,176

	DEFB	 27,176
	DEFB	 27,176
	DEFB	 27,176
	DEFB	 27,176
	DEFB	 27,176
	DEFB	 27,176
	DEFB	 27,176
	DEFB	 27,176

	DEFB	 27,176
	DEFB	 27,176
	DEFB	 27,176
	DEFB	 27,176
	DEFB	 27,176
	DEFB	 27,176
	DEFB	 27,176
	DEFB	 27,176
; (Attributes here, insert label if needed)
	DEFB	 71, 71
	DEFB	 71, 71
	DEFB	 71, 71
	DEFB	 71, 71


resto_letrero:
	DEFB	  0,  0,  0,  0,  0,  0,  0, 19
	DEFB	176,  0,  0,  0
	DEFB	  0,  0,  0,  0,  0,  0,  0, 11
	DEFB	176,  0,  0,  0
	DEFB	  0,  0,  0,  0,  0,  0,  0, 19
	DEFB	176,  0,  0,  0
	DEFB	  0,  0,  0,  0,  0,  0,  0, 11
	DEFB	176,  0,  0,  0
	DEFB	  0,  0,  0,  0,  0,  0,  0,  3
	DEFB	176,  0,  0,  0
	DEFB	  0,  0,  0,  0,  0,  0,  6,  3
	DEFB	176,  0,  0,  0
	DEFB	  0,  0,  0,  0,  0,  0, 13,  3
	DEFB	176,192,  0,  0
	DEFB	  0,  0,  0,  0,  0,  0, 63,  3
	DEFB	176, 61, 96,  0

	DEFB	  0,  0,  0,  0,  0,  0,127, 47
	DEFB	 76,141, 48,  0
	DEFB	  0,  0,  0,  0,  0, 70,223,190
	DEFB	 63,238,240,  0
	DEFB	  0,  0,  0,  0,  0,169,255,223
	DEFB	254,244,254,  0
	DEFB	  0,  0,  0,  0,  0,127,235,222
	DEFB	 58,233,221,128
	DEFB	  0,  0,  0,  0,  7,251,207,191
	DEFB	223,239,247,224
	DEFB	  0,  0,  0,  7,255,243,159,127
	DEFB	239,191,254, 56
	DEFB	  0,  0,  0, 12,255,119,190,255
	DEFB	231, 63,191,200
	DEFB	  0,  0,  0, 57,159,151, 57,190
	DEFB	248,127,255,236

	DEFB	  0,  0,  3,227,253, 46, 91, 23
	DEFB	245,239,251,188
	DEFB	  0,  0, 14, 14,127,244,157,255
	DEFB	247,120,255,120
	DEFB	  0,  0, 24,255,249,215,239,239
	DEFB	235,255,239,166
	DEFB	  0,  0,119,255,239,239,239,127
	DEFB	221,172,255,143
	DEFB	  0,  7,159,255,115,255,183,222
	DEFB	254, 47,255,250
	DEFB	  1,253,255,207,113,221,231,124
	DEFB	255,248,191, 16
	DEFB	  6,223,255,223,246, 94,187, 78
	DEFB	255,251,235,176
	DEFB	 13,255,118,191,159, 71,247,255
	DEFB	255,191,172,128

	DEFB	 63,251,223,255,255,255,207,125
	DEFB	253,254,188,  0
	DEFB	 47,188,238,253,127,207,255,255
	DEFB	255,205,250,  0
	DEFB	 51,112,251,249,248,219,206,255
	DEFB	255,255,239,  0
	DEFB	216, 96, 79,181,255,241,191,226
	DEFB	127,123,225,128
	DEFB	184,  0,  3,239,228,238, 23,254
	DEFB	 20, 74,223,  0
	DEFB	240,  0,  1,125, 37,216, 29,135
	DEFB	  9,  2,112,  0
	DEFB	 48,  0,  1,  8,  0,128,  3,  4
	DEFB	255,  1,192,  0
	DEFB	  0,  0,  0,  0,  0,  0,  1,223
	DEFB	128,  0,  0,  0
; (Attributes here, insert label if needed)
	DEFB	 66, 66, 66, 66, 66, 66,  2,  7
	DEFB	 71,  2,  2, 66
	DEFB	 66, 66, 66,  2, 66, 66, 66,  2
	DEFB	 66, 66, 66, 66
	DEFB	  2,  2, 66, 66, 66, 66,  2, 66
	DEFB	 66, 66, 66, 66
	DEFB	  2,  2,  2, 66, 66, 66,  2, 66
	DEFB	 66, 66, 66, 66


; (Insert graphic label here if needed)
empunadura:

	DEFB	  1,128
	DEFB	 14,112
	DEFB	 49,140
	DEFB	 47,244
	DEFB	 71,226
	DEFB	 83,202
	DEFB	 93,186
	DEFB	191,249

	DEFB	191,249
	DEFB	 93,186
	DEFB	 83,202
	DEFB	 71,226
	DEFB	 47,244
	DEFB	 49,140
	DEFB	 14,112
	DEFB	  1,128

	DEFB	 31,248
	DEFB	 16,  8
	DEFB	 31,248
	DEFB	 16,  8
	DEFB	 31,248
	DEFB	 16,  8
	DEFB	 31,248
	DEFB	 16,  8

	DEFB	 31,248
	DEFB	 48, 12
	DEFB	 63,252
	DEFB	 63,252
	DEFB	 32,  4
	DEFB	 63,252
	DEFB	 37,  4
	DEFB	 63,252

	DEFB	 41, 36
	DEFB	 63,252
	DEFB	 63,252
	DEFB	 52,  4
	DEFB	 63,252
	DEFB	 63,252
	DEFB	 42,132
	DEFB	 31,248
; (Attributes here, insert label if needed)
	DEFB	 66, 66
	DEFB	 66,  2
	DEFB	  3,  3
	DEFB	 67,  3
	DEFB	 67,  3
