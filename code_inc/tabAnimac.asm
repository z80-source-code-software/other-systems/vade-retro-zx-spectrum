;----------------------------------------------------------------------------------------
; Tabla de animaciones por pantalla. 

; Los enemigos animados son : rayo, gotaInicio, gotaFinal y llamarada. Son animaciones fijas
; Rayo:		0 
;	2 frames, 
;	intervalo entre acciones muy corto para que no sea posible pasarlo sin desactivarlo.
;	tama�o 1x3
; gotaInicio:	1
;	3 frames,
;	intervalo entre acciones no tiene salvo el continuo cada vez que desaparezca la anterior
;	tama�o 1x1
;	Produce 1 enemigo que es la gota que cae
; gotaFinal:	2
;	identica a la anterior
;	Produce 1 animaci�n qu es la gota al aparecer en la estalactita.
; llamarada:	0
;	4 frames,
;	intervalo entre acciones amplio para que el prota use los elementos de donde surge
;	tama�o 2x1
;	podriamos intentar que tambi�n fuera posible desactivarlas como el rayo.
;	dos sentidos


; Defb's con los siguientes datos
; panta, activo, posx,  posy, ticks, frameskip, frameActual, framestotal, altosc, anchochars,
; ix0,	ix1,	ix2,	ix3,	ix4,	ix5,		ix6,	ix7,	  ix8,  	ix9
; tama�o1frame, dblowAnim, dbhighAnim, intervalo, ticks_intervalo, frameskip_intervalo,
;	ix10,	ix11,		ix12,	ix13,	   ix14,		ix15,		
; dblowAnimIni,dbhighAnimIni, codanim, posY2
; ix16,		ix17		ix18,	ix19

; Explicaci�n de datos:
; 
; activo (si o no), posx en char, posy en pixels, tamba�o1frame (para sumar a dblowAnimMem)
; intervalo (0=no empezar, 1=empezar animacion) frameskip_intervalo (tiempo de espera entre animaciones)
; frameskip_intervalo es ir� de X a 255 (cuanto m�s corto m�s r�pida comenzar� la animaci�n)
; Una vez alcanzado 255 cambiamos el valor de intervalo de 0 a 1. El valor de frameskip_intervalo se 
; pasara a ticks_intervalo que ser� el contador
; codanim (codigo para actuar) en animaciones ser� 1=gota que aparece 2=gota que desaparece
; posY2 es la distancia de la posY a la posY' de la gota al caer

;		dblowIniAnim	dbhighIniAnim	tama�oframe	n.fram	altosc	anchoch
; Rayo		00h		BFh		48d		2	24	1
; llamaradader	60h		BFh		32d		4	8	2
; llamaradaizq.	E0h		BFh		32d		4	8	2
; gotasale	60h		C0h		16d		3	8	1
; gotafinsuelo	90h		C0h		16d		3	8	1


; ix+	    0,1,2,3,   4,5,6,7, 8, 9,10, 11, 12,13,14, 15,  16,17,18,19
ANIMAC:
;;	ejemplos
;;	db  0,1, 4, 88,8,8,0,3, 8, 2,32,#60,#bf,0,200,200,#60,#bf,0,0	; llamarada derecha pant 0
;;	db 38,1,16, 64,8,8,0,3, 8, 2,32,#e0,#bf,0,200,200,#e0,#bf,0,0	; llamarada izquier 
;;	db 12,1, 7,120,8,8,0,1,24, 1,48,#00,#bf,0,240,240,#00,#bf,0,0	; rayo
;;	db 13,1,12,120,8,8,0,2, 8, 1,16,#60,#c0,0,240,240,#60,#c0,1,16	; gota

	db 255,0, 0,  0,0,0,0,0, 0,0, 0,  0,  0,0,  0,  0,  0,  0,0, 0
	db 255,0, 0,  0,0,0,0,0, 0,0, 0,  0,  0,0,  0,  0,  0,  0,0, 0
	db 255
