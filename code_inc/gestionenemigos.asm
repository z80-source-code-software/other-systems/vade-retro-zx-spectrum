;----------------------------------------------------------------------------------------
; Rutinas de Gestion de enemigos.
;----------------------------------------------------------------------------------------

;
; Rutina Principal
;
GESTENEM:
	push	hl		; Guardo sin saber si es necesario
	push	de
	ld	ix,ENEMIGOS
BGESTENEM:
;
; enemigo activo S/N
;
	ld	a,[ix+1]
	and	a
	jp	z,detec_flechas	; Ver flechas o enemigos desactivados por muerte
;
; contador de ticks
;
mirarticks:	
	dec	[ix+4]
	jp	nz,imprimemismoenemigo
;
; pongo el skipframe del db's en ticks
;
	ld	a,[ix+5]		
	ld	[ix+4],a
;
; Comprobamos si es el �ltimo frame
;
	ld	b,[ix+7]	; framesTotal
	ld	a,[ix+6]	; frameActual
	cp	b
	jr	z,ponframeEnemigo_0
;
; Incremento el valor del frame
;
	inc	[ix+6]
;
; Meto los valores del churro en las variables antes de llamar a volcar el fondo 
;
	call	vuelcafondosprenem	; vuelco el fondo del enemigo
;
; Ahora con el tama�o del frame calculamos el frame a imprimir. Sumandolo a dblowAnim
;

; cacho de metalbrain
	ld      c,[ix+10]       ; tama�o del frame
	ld	a,127		; cualquier numero mayor es tomado como negativo
	cp	c		; si es mayor habr� acarreo
	sbc     a,a		; si acarreo 127- 127 -1 = 255, si no acarreo 127-127=0
	ld	b,a		; b=255 con acarreo (negativo) b=0 positivo
; fin cacho de metelbrain	
	ld	l,[ix+11]	; dblowAnim
	ld	h,[ix+12]	; dbhighAnim
	add	hl,bc
	ld	[ix+11],l
	ld	[ix+12],h	; guardo el valor para el pr�ximo frame
	jr	imprimemismoenemigo

ponframeEnemigo_0:	
	call	vuelcafondosprenem	; vuelco el fondo de la animaci�n
;
; La muerte del enemigo est� activada ?
;
	ld	a,[ix+25]
	and	a
	jr	z,bpframe_0

	ld	[ix+1],0		; desactivo el enemigo para no seguir imprimiendolo
	JR	verotroenemigo
bpframe_0:
;
; frame a 0
;
	ld	[ix+6],0
;
; Inicializo los frames a pesar de no saber si va a darse la vuelta el sprite.
;
	ld	a,[ix+13]
	ld	[ix+11],a	; low
	ld	a,[ix+14]
	ld	[ix+12],a	; high
;
; discrimino la Direcci�n entre vertical u horizontal. 
;
	ld	a,[ix+15]
	and	a		; 0 horizontal, 1 vertical
	jp	z,enem_horizontal
	jp	enem_vertical
;
; En caso de que el enemigo estuviera desactivado y que fuera la flecha.  
;
detec_flechas:
	ld	a,[ix+18]		; la flecha =128
	rla
	jr	nc,verotroenemigo
;
; detectar el movimiento del prota en su posible trayectoria.
;
	ld	b,[ix+3]	; posY
	ld	a,[fila]
	add	a,16
	cp	b
	jr	nz,verotroenemigo
	ld	[ix+1],1
;
; Sonido
;
	ld	a,1
	ld	[repe_fx],a
        ld	a,49		; 
        ld	[long_fx],a
	ld	hl,convalorson	; etiquetas en la rutina de interrupciones
	ld	[vervalorson+1],hl
	jr	verotroenemigo
;
; Imprimo el enemigo. Pongon en hl el valor del sprite a imprimir
;
imprimemismoenemigo:
	call	cambiaranchoenem	; cambio el ancho y llamo a imp_ene (lo junto por ahorrar)
;
; Si el enemigo ya est� imprimiendose SU MUERTE no vuelvas a comprobar el choque
;
	ld	a,[ix+25]
	and	a
	jr	nz,verotroenemigo	; esta activa la animaci�n de la muerte del enemigo
;
; inmunidad Activada ?
;
	ld	a,[inmunidad]
	and	a
	jr	nz,verotroenemigo
;
; Detectar choques de los enemigos con el prota
;
	call	det_choques_enemigos
;
; comprobacion de si ha chocado con enemigo. Esto lo hago para usar la misma rutina con Nazgul
;
	ld	a,[vchoqEnem]
	and	a
	jr	nz,verotroenemigo
an_verotroenemigo:
;
; Comprueba si eres t� el que mata al enemigo
;
	ld	a,[vchoqEnemesp]
	and	a
	jr	z,verotroenemigo
;
; si hay choque llama a la rutina de poner la animaci�n del enemigo muerto
;
	call	ponmuerteEnemigo
verotroenemigo:
	ld	de,26
	add	ix,de
;
; Comprobamos si ya has mirado ambas animaciones
;
	ld	a,[ix+0]
	rla	; asi es mucho m�s r�pido pero no puedo haber ning�n dato superior a 128
	jp	nc,BGESTENEM  ; IX+0 que es panta no puede ser igual o superior a 128

FINGESTENEMIGOS:
;
; Aqui gestionar al enemigo BL_ Nazgul
;
	ld	hl,[Pre_Nazgul]
	jp	[hl]
LLAMA_NAZGUL:
	call	GESTION_NAZGUL
NO_LLAMA_NAZGUL:
;
; Aqui gestionar al enemigo Delcram
;
	ld	hl,[Pre_Delcram]
	jp	[hl]
LLAMA_DELCRAM:
	call	GESTION_DELCRAM
NO_LLAMA_DELCRAM:
	pop	de
	pop	hl
	ret

cambiaranchoenem:
;
; Modifico el valor del registro B en SprEnemImp.asm para que sea el ancho del sprite actual.
;
	exx
	ld	hl,chganchospen+1
	ld	a,[ix+9]
	ld	[hl],a
	exx
	call	imp_ene		; imprimo el enemigo
	ret

;
; Fin Rutina Ppal
;
;----------------------------------------------------------------------------------------

Pre_Nazgul	defs	2
vchoqEnem	defs	1		; variable de detecci�n de choque con prota
vchoqEnemesp	defs	1		; "		"			" con mandoble

;----------------------------------------------------------------------------------------
; Gesti�n de enemigos verticales

; 
; Los Enemigos verticales son : Gota=1, Bola=2, Ave=4, Ara�a=8
;
enem_vertical:

;
; Comprobamos si has llegado al limite. Para ello necesito ayudarme del sentido del enemigo
;

; 0 arriba 1 abajo 
	ld	a,[ix+19]
	and	a		; arriba?
	jp	z,enem_vertical_arriba
; 
; mov_enem_vertical_abajo. Comprobaci�n de limite2
;
	ld	b,[ix+17]
	ld	a,[ix+3]	; posY
	cp	b
	jr	z,fin_limite2_mov_vert
;
; en caso contrario no hemos llegado al limite y estamos bajando. Tenemos que continuar
; pintando el monigote, sea el que sea pero en su nueva posicion.
;
	ld	b,[ix+20]	; desplazamiento vertical en pixels
	ld	a,[ix+3]
	add	a,b
	ld	[ix+3],a	; nueva posY
ll_a_todos_2:
	call	avolcamiento_enem	;calc_nueva_pos_enem
ll_a_todos:
	call	cambiaranchoenem	; cambio el ancho y llamo a imp_ene (lo junto por ahorrar)
	jp	verotroenemigo
;
; Caso de fin de limite ahora debemos saber que enemigos es para actuar sobre el.
;
fin_limite2_mov_vert:
;
; Comprobamos codanim
;
	xor	a		; anulo posible acarreo 
	ld	a,[ix+18]
	rra			; Gota que ha aparecido gracias a la animaci�n
	jr	c,gotaenem
	rra			; Bolaghost
	jr	c,bolaenem	; 
	ex	af,af'		; Guardo el ave y la ara�a
; 
; Cambiar sentido de movimiento del ave y de la ara�a
;
	ld	[ix+19],0	; 0 arriba 1 abajo 
;
; Pongo en negativo el valor del tama�o del frame. Para cuando sumes en la rutina ppal
;
	ld	a,[ix+10]	; tama�o del frame
	neg			; ahora debe restar al sumar el frame para poder
	ld	[ix+10],a	; imprimir correctamente el enemigo al subir
; 
; cambio el valor de los db's inicio high de la ara�a y del ave que son la misma. Ahorro de bytes
;
	ld	[ix+14],$be
	ld	[ix+12],$be	; high
;
; Saco el valor de codanim anteriormente guardado para el ave o la ara�a
;
	ex	af,af'
	rra
	jr	c,avenem
; 
; cambio el valor de los db's inicio de la ara�a
;
	ld	[ix+13],$60
	ld	[ix+11],$60	; low
	jr	ll_a_todos	;	verotroenemigo
avenem:
; 
; cambio el valor de los db's inicio de la ave
;
	ld	[ix+13],$c0
	ld	[ix+11],$c0	; low
	jr	ll_a_todos	;verotroenemigo
;
; enemigos verticales de un solo sentido
;
gotaenem:
	ld	[ix+1],0	; desactivo el estar ACTIVO
;
; restauro el valor de la gota inicial
;
	ld	a,[ix+16]	; guardado en limite 1 que no usamos para la gota ni la bola
	ld	[ix+3],a
	call	avolcamiento_enem	;calc_nueva_pos_enem
; 
; ahora tengo que activar en tabla de animaciones la gota salpicando.
;
	push	ix
	ld	ix,ANIMAC
	ld	[ix+1],1
	pop	ix
	jp	verotroenemigo
bolaenem:
;
; la bola cae y espera a que pases por encima para volver a ponerse en movimiento.
;
	ld	a,[colu]
	ld	b,a
	ld	a,[ix+2]
	cp	b
	jp	nz,imprimemismoenemigo
	ld	a,[ix+3]		; fila bola
	sub	4
	ld	[ix+3],a
	call	avolcamiento_enem	; calc_nueva_pos_enem
	ld	[ix+19],0		; enemigo hac�a arriba
	jp	imprimemismoenemigo
;------------------------------------------------------------------------------
enem_vertical_arriba:
; 
; mov_enem_vertical_arriba. Comprobaci�n de limite1
;
	ld	b,[ix+16]
	ld	a,[ix+3]	; posY
	cp	b
	jr	z,fin_limite1_mov_vert
;
; en caso contrario no hemos llegado al limite y estamos bajando. Tenemos que continuar
; pintando el monigote, sea el que sea pero en su nueva posicion.
;
	ld	b,[ix+20]	; desplazamiento vertical en pixels
	ld	a,[ix+3]
	sub	b
	ld	[ix+3],a	; nueva posY
	jp	ll_a_todos_2
;
; Caso de fin de limite ahora debemos saber que enemigos es para actuar sobre el.
;
fin_limite1_mov_vert:
; 
; Cambiar sentido de movimiento del ave y de la ara�a
;
	ld	[ix+19],1	; 0 arriba 1 abajo 
;
; Pongo en positivo el valor del tama�o del frame. Para cuando sumes en la rutina ppal
;
	ld	a,[ix+10]	; tama�o del frame
	neg			; ahora debe restar al sumar el frame para poder
	ld	[ix+10],a	; imprimir correctamente el enemigo al subir
;
; Comprobamos codanim
;
	ld	a,[ix+18]
	cp	2		; si bola sigue
	jp	z,verotroenemigo
	bit	2,a		;  Si el bit est� a 1 es que es el ave.
	jr	nz,avenemarriba
; 
; cambio el valor de los db's inicio de la ara�a
;
	ld	[ix+13],$e0
	ld	[ix+11],$e0	; low
	ld	[ix+14],$bd
	ld	[ix+12],$bd	; high
	jp	ll_a_todos	;verotroenemigo
avenemarriba:
; 
; cambio el valor de los db's inicio de la ave
;
	ld	[ix+13],$a0
	ld	[ix+11],$a0		; low, high no modificado
	jp	ll_a_todos	;verotroenemigo

;----------------------------------------------------------------------------------------
; Gesti�n de enemigos horizontales

; 
; Los Enemigos horizontales son : flechas=1, murcielago=2, chinchilla=4, fantasmita=8
;
enem_horizontal:
;
; Comprobamos si has llegado al limite. Para ello necesito ayudarme del sentido del enemigo
;

; 0 derecha 1 izquierda
	ld	a,[ix+19]
	and	a		; derecha?
	jp	z,enem_horizontal_derecha
; 
; mov_enem_horizontal_izquierda. Comprobaci�n de limite2
;
	ld	b,[ix+17]
	ld	a,[ix+2]	; posX
	cp	b
	jr	z,fin_limite2_mov_horiz
;
; en caso contrario no hemos llegado al limite.Pintamos monigote en su nueva posicion.
;
	dec	a
	ld	[ix+2],a	; nueva posX
	jp	ll_a_todos_2

fin_limite2_mov_horiz:
;
; cambio sentido al enemigo
;
	ld	[ix+19],0
;
; Comprobamos codanim
;
	ld	a,[ix+18]
	rla			; flecha 
	jr	c,c_flechader
	rla			; 
	jr	c,c_murcielagoder
	rla
	jp	nc,verotroenemigo
;
; Cambio de direcci�n. Cambiamos: dblow frame e inicio y dbhigh frame e inicio (murcielago y chinchilla)
;
c_chinchillader:
	ld	[ix+11],$e0
	ld	[ix+13],$e0
	ld	[ix+12],$bb
	ld	[ix+14],$bb
	jp	ll_a_todos	;verotroenemigo
c_flechader:
	ld	[ix+19],1	; dejo el sentido que lleva
	ld	a,[ix+16]	; restauro posX con limite1
	ld	[ix+2],a	; posX
	jr	finsalidaflecha
c_murcielagoder:
	ld	[ix+11],$20
	ld	[ix+13],$20
; dbhign no cambia as� que no la tocamos
	jp	ll_a_todos	;verotroenemigo
; 
; mov_enem_horizontal_derecha. Comprobaci�n de limite1
;
enem_horizontal_derecha
	ld	b,[ix+16]
	ld	a,[ix+2]	; posX
	cp	b
	jr	z,fin_limite1_mov_horiz
;
; en caso contrario no hemos llegado al limite.Pintamos monigote en su nueva posicion.
;
	inc	a
	ld	[ix+2],a	; nueva posX
	jp	ll_a_todos_2
fin_limite1_mov_horiz:
	ld	[ix+19],1	; cambio sentido al enemigo
;
; Comprobamos codanim
;
	ld	a,[ix+18]
	rla			; flecha 
	jr	c,c_flechaizq
	rla			; 
	jr	c,c_murcielagoizq
	rla
	jp	nc,verotroenemigo	; si hay acarreo es la chinchilla
;
; Discriminaci�n de enemigos finalizada
;

;
; Cambio de direcci�n. Cambiamos: dblow frame e inicio y dbhigh frame e inicio (murcielago y chinchilla)
;
c_chinchillaizq:
	ld	[ix+11],$40
	ld	[ix+13],$40
	ld	[ix+12],$bc
	ld	[ix+14],$bc
	jp	ll_a_todos	;verotroenemigo
c_flechaizq:
	ld	[ix+19],0	; volver a dejar el sentido al enemigo
	ld	a,[ix+17]	; restauro posX con limite2
	ld	[ix+2],a	; posX
finsalidaflecha:
	ld	[ix+1],0	; desactivo el enemigo
	call	avolcamiento_enem	;calc_nueva_pos_enem
	jp	verotroenemigo
c_murcielagoizq:
	ld	[ix+11],$80
	ld	[ix+13],$80
; dbhign no cambia as� que no la tocamos
	jp	ll_a_todos	;verotroenemigo
