 marcadorabajo:
	incbin "gfx_inc\marcadorabajo.zx7"	; 
; Descomprimo el marcador en un trozo del buffer de pantalla. Voy a ponerlo en 64000=FA00
descomprimemarcadorabajo:
	ld	hl,marcadorabajo	
	ld	de,64000	; $fa00	Direcci�n en el buffer de pantalla donde descomprimirse.
	ld	bc,767		; bytes del titulo comprimido.
	jp	dzx7		; descompresor