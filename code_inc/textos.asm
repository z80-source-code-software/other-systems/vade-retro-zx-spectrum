;----------------------------------------------------------------------------------------
;   Rutina de impresion de textos. Usando la ROM
;----------------------------------------------------------------------------------------

; 
Imprime_Marco:
	ld	de,texto_Marco
        ld	c,0
	ld	b,2
	push	bc
	ld	b,32	; contador de carac. a imprimir
	ld	a,22	; codigo AT
        rst	16
	ld	a,10	; Fila marco superior
        rst	16
	xor	a       ; columna 0
        rst	16
crearlinea:
	ld	a,[de]  ; cargo el primer caracter
        rst	16	; chimpun
        inc	de	; incremento la posi pa cargar el siguiente
        djnz	crearlinea
	pop	bc
	ld	b,32	; contador de carac. a imprimir
	ld	a,22	; codigo AT
        rst	16
	ld	a,12	; Fila marco superior
        rst	16
	xor	a       ; columna 0
        rst	16
crearlinea2:
	ld	a,[de]  ; cargo el primer caracter
        rst	16	; chimpun
        inc	de	; incremento la posi pa cargar el siguiente
        djnz	crearlinea2
; Colorear
	ld	a,6
	ld	hl,#5940
	ld	de,#5941
	ld	[hl],a
	ld	bc,95	; caracteres a imprimir
	ldir
	ret


Imprime_TextoEspada:
	ld	de,texto_Espada
        ld	c,0
	ld	b,2
	ld	b,32	; contador de carac. a imprimir
	ld	a,22	; codigo AT
        rst	16
	ld	a,11	; Fila marco superior
        rst	16
	xor	a       ; columna 0
        rst	16
lineaEspada:
	ld	a,[de]  ; cargo el primer caracter
        rst	16	; chimpun
        inc	de	; incremento la posi pa cargar el siguiente
        djnz	lineaEspada
	jr	coloreartexto

Imprime_TextoPocima:
	ld	de,texto_Pocima
        ld	c,0
	ld	b,2
	ld	b,32	; contador de carac. a imprimir
	ld	a,22	; codigo AT
        rst	16
	ld	a,11	; Fila marco superior
        rst	16
	xor	a       ; columna 0
        rst	16
lineaPocima:
	ld	a,[de]  ; cargo el primer caracter
        rst	16	; chimpun
        inc	de	; incremento la posi pa cargar el siguiente
        djnz	lineaPocima
;
; colorear fondo texto
;
coloreartexto:
	ld	hl,#5960
	ld	[hl],3
	inc	hl
	ld	a,4	; color Verde ,	parpadeo en blanco=135
	ld	[hl],a
	ld	de,#5962
	ld	bc,30	; caracteres a imprimir
	ldir
	ld	[hl],3
;
; Soltar tecla y espera para pulsar teclas
;
	call	solespe
;
; Soltar tecla y espera para pulsar teclas
;
	call	solespe

;
; Restauro la pantalla principal
;
	call	volcar_pant	
	ret
;
; Soltar tecla y espera para pulsar teclas. Est�n en MenuyMarcador
;
solespe:
	call	Soltar_tecla
	call	Espera_tecla
	ret

texto_Marco:
;	defb "|                              |"
	defb 139,131,131,131,131,131,131,131,131,131,131,131,131,131,131,131
	defb 131,131,131,131,131,131,131,131,131,131,131,131,131,131,131,135

	defb 142,140,140,140,140,140,140,140,140,140,140,140,140,140,140,140
	defb 140,140,140,140,140,140,140,140,140,140,140,140,140,140,140,141

texto_Espada:
	defb 138,"  You got the sword. Use it!  ",133
texto_Pocima:
	defb 138," Potion enabled. Kill Delcram ",133
