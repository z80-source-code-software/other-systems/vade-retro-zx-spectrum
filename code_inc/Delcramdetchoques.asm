;-----------------------------------------------------------------------------------
; RUTINAS DE DETECCION DE CHOQUES y GOLPEO DE AGANZIO
;-----------------------------------------------------------------------------------

; Entrada fila y colu  Salida calculo los limites del prota dejandolos en DE Y HL 
det_choques_delcram:
        ld	a,[fila]	
        ld	d,a		; d= fila +3px
        add	a,21
	ld	e,a		; e= fila +24px (ALTO)
        ld	a,[colu]
        ld	h,a		; h= colu
;
; discriminar si el anchosp es 2 o 3 para ajustar el golpeo de la espada si se produce o el choque
;
	ld	a,[anchosp]
	cp	3
	jr	z,adet_ch_delcram
	ld	a,h
	inc	a	        ; l= colu +1    (ANCHO)
	jr	badet_ch_delcram
adet_ch_delcram:
	ld	a,h
	add	a,2
badet_ch_delcram:
	ld	l,a	        ; l= colu +2   (ANCHO) para espadazo
;
; Tamiz de comprobacion con enemigos. Tabla ENEMIGOS IX+2 ES X e IX+3 ES Y
;
	ld	b,[ix+2]	; b= col_enem
	ld	a,b
	add	a,3
	ld	c,a             ; c= col_enem +3
;
;comprobamos Columnas
;
        ld	a,c		
	cp	h
	ret	c
        ld	a,b
	cp	l
	jr	z,verfila_aganzio_prota
        ret	nc
verfila_aganzio_prota:
        ld	a,[ix+3]
	add	a,2
	ld	b,a		; b= fil_enem +2px
	add	a,22
	ld	c,a		; c= fil_enem +24px
;
;comprobamos Filas
;
	ld	a,c		
	cp	d
	ret	c
	ld	a,b
	cp	e
	ret	nc
;
; Comprobamos si la espada est� activa. 
;
	ld	a,[accion]
	bit	0,a			; Est� Activo el bit de espadazo ?
	jr	z,enemy_w_delcram
;
; golpeas a Delcram
;

; Aqui veremos si llevas la p�cima para hacerle da�o
;	ld	a,[PocimaAct]
	ld	a,[hechizo]
	and	a			; si hay 1 es que llevas la p�cima
	ret	z			; en caso contrario te permite ""salvarte""
; tener una oportunidad para volverte y recogerla. Bueno es un decir.

	ld	a,[delcramcm]		; incremento la variable de matar a delcram 
	inc	a			; cuando valga 3
	ld	[delcramcm],a
	cp	18			; Son unos 3 golpes
	ret	nz	
	ld	a,6
	out	[254],a
	ld	a,1
	ld	[vchoqEnem],a	; pongo a 1 el que haya choque para que matemos a Delcram
	ld	[repe_fx],a
	ld	a,25	
        ld	[long_fx],a
	ld	hl,convalorson	; etiquetas en la rutina de interrupciones
	ld	[vervalorson+1],hl
	ld	a,4
	out	[254],a
        ld	a,75
        ld	[long_fx],a
	ret
;
; Delcram te ataca
;

enemy_w_delcram:
	xor	a
	ld	[vchoqEnem],a	; Delcram ataca o te toca y no lo est�s matando 
;Esta variable nos vale para saber si tienes que comprobar la detecci�n o no en la gestion.
	inc	a
	ld	[inmunidad],a	; activo la inmunidad
;
; Sonido, quitar barra energia y activar la variable choques
;
	ld	[repe_fx],a
	ld	a,13
        ld	[long_fx],a
	ld	hl,convalorson	; etiquetas en la rutina de interrupciones
	ld	[vervalorson+1],hl
;
; en el caso de que en el choque con Delcram est� activado SU ataque, entonces m�ximo da�o
;
	ld	a,[ix+0]
	and	a
	jr	nz,minimoda_o
	call	da_o_4		; maximo da�o  (barraenergia.asm)
	ret
minimoda_o:	
	call	da_o_1		; esto est� en quita_energia pero as� no hago m�s da�o que 1 px
	ret
;-----------------------------------------------------------------------------------
;FIN DETECCION CHOQUES
;-----------------------------------------------------------------------------------

;-----------------------------------------------------------------------------------
; MUERTE DE AGANZIO la he puesto aqui. Viene de TabyGestAganzio.asm
;-----------------------------------------------------------------------------------
Muerte_DELCRAM:
;
; contador de ticks	para no incrementar los frames antes de que se puedan ver		
;
	ld	a,[ix+4]	
	dec	a
	ld	[ix+4],a
	jp	nz,imprime_delcram
;
; Uso ix+18 como contador para saber cuando debo poner o restaurar los valores a imprimir
;
	ld	a,[ix+18]
	and	a
	jr	z,comienzamuerteA
	cp	4
	jr	nc,fin_muerteA
	inc	a
	ld	[ix+18],a
	jp	b_cont_ticks_delcram	; De nuevo a la rutina principal en TabyGestAganzio.asm
comienzamuerteA:
	ld	[ix+18],1
	ld	[ix+0],1	; deja de atacar si lo estabas haciendo. (comentar no es necesario)
	ld	[ix+4],8	; frameskip de la muerte a 8
	ld	[ix+5],8	; frameskip de la muerte a 8
	ld	[ix+6],0
	ld	[ix+7],3	; frames para la muerte de Aganzio 4 (igual para atacar)
; valor de inicio del db' low Anim MUERTE
	ld	[ix+11],$c3
	ld	[ix+13],$c3
; valor de inicio del db' high Anim MUERTE
	ld	[ix+12],$e0
	ld	[ix+14],$e0
	call	avolcamiento_enem
	call	imp_ene		; imprimo el enemigo
	jp	cont_ticks_delcram	; De nuevo a la rutina principal en TabyGestAganzio.asm
fin_muerteA:
	call	vuelcafondosprenem	; vuelco el fondo de la animaci�n
;
; pongo los valores de inicio que hab�a en el churro antes de entrar en la pantalla para otra partida.
;
	call	Rest_delcram	; si cabe la subrutina aqu� mejor meterla aqu�.
	call	avolcamiento_enem
;
; Activar el JUEGO ACABADO. HAS GANADO --- you win
;
	xor	a
	ld	[final],a
	ld	[vchoqEnem],a	; Desactivo el choque con Delcram
	jp	finDELCRAM	; De nuevo a la rutina principal en TabyGestAganzio.asm

;-----------------------------------------------------------------------------------
; ATAQUE
;-----------------------------------------------------------------------------------
Ataque_DELCRAM:

;
; contador de ticks	para no incrementar los frames antes de que se puedan ver		
;
	ld	a,[ix+4]	
	dec	a
	ld	[ix+4],a
	jp	nz,imprime_delcram
;
; Uso ix+20 como contador para saber cuando debo poner o restaurar los valores a imprimir
;
	ld	a,[ix+20]
	and	a
	jr	z,comienzaelataque
	cp	4
	jr	z,fin_ataque
	inc	a
	ld	[ix+20],a
	jp	b_cont_ticks_delcram	; devuelvo el control arriba en 2
comienzaelataque:
	call	vuelcafondosprenem	; vuelco el fondo deL SPRITE
	ld	[ix+20],1	; es lo mismo que lo siguiente y como no va a volver aqu� m�s...
	ld	[ix+5],8
	ld	[ix+4],8
	ld	[ix+6],0
	ld	[ix+7],3	; frames para el ataque 4
; valor de inicio del db' low Anim ATACAR
	ld	[ix+11],$83
	ld	[ix+13],$83
; valor de inicio del db' high Anim ATACAR
	ld	[ix+12],$de
	ld	[ix+14],$de
	call	avolcamiento_enem
	call	imp_ene		; imprimo el enemigo
	jp	cont_ticks_delcram	; devuelvo el control arriba en 1
fin_ataque:
	ld	[ix+0],1	; deja de atacar
	ld	[ix+20],0	; fr de ataque a 0
	ld	[ix+4],8
	ld	[ix+5],8
	ld	[ix+6],0	; frames para andar 
	ld	[ix+7],2	; frames para andar 
; valor de inicio del db' low Anim ANDAR a izquierdas
	ld	[ix+11],$d3
	ld	[ix+13],$d3
; valor de inicio del db' high Anim ANDAR a izquierdas
	ld	[ix+12],$dc
	ld	[ix+14],$dc
	jp	b_cont_ticks_delcram	; devuelvo el control arriba en 2
