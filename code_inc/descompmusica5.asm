;
; Descompresión del player y musicas
;
des_musica:
	ld	hl,30529	; Player y músicas comprimidas
	ld	de,60816	; $ed90	Dirección del buffer de pantalla 
	ld	bc,1327		; para descomprimir el player y tablas
ades_musica:
	call	dzx7
	ret
des_song0:
	ld	hl,31856	; menu
	ld	de,62604		;60816+1788 Tablas+Player+64bytes ; donde se descomprimirán las canciones
	ld	bc,220
	jr	ades_musica
des_song1:
	ld	hl,31856+220	; gameover 32164
	ld	de,62604		;60816+1788 Tablas+Player+64bytes ; donde se descomprimirán las canciones
	ld	bc,286
	jr	ades_musica
des_song2:
	ld	hl,43799	; final
	ld	de,62604		;60816+1788 Tablas+Player+64bytes ; donde se descomprimirán las canciones
	ld	bc,809
	jr	ades_musica

; Definir buffer RAM musica. Necesario para el player WYZ
; Se pone aquí por falta de mejor sitio.
def_buffer_mu:
	ld      hl,BUFFER_DEC        	;* $40 BYTES FREE RAM MEMORY 
        ld      [CANAL_A],hl		;* $10 BYTES x CHANNEL SHOULD BE ENOUGHT
        ld      hl,BUFFER_DEC+$10        	
        ld      [CANAL_B],hl      	
	ld      hl,BUFFER_DEC+$20       	
        ld      [CANAL_C],hl
	ld	hl,BUFFER_DEC+$30       	
	ld	[CANAL_P],hl
	ret

; 30496 musica a desomprimir debajo de este ret