;----------------------------------------------------------------------------------------
; Tabla del enemigo especial. Delcram
;----------------------------------------------------------------------------------------
; Defb's con los siguientes datos

; Ataque,activo, posx,  posy, ticks, frameskip, frameActual, framestotal, altosc, anchochars,
; ix0,	    ix1,    ix2,   ix3,	 ix4,	ix5,		ix6,	ix7,	  ix8,  	ix9

; tama�o1frame, dblowEnem, dbhighEnem, dblowEnemIni,dbhighEnemIni, Yt,      limite1,
;	ix10,	ix11,		ix12,  ix13,		ix14,	   ix15,	ix16

; limite2, codanim, sentido, frAtack,dblowPant, dbhighPant, dblowBuff, dbhighBuff, choque
; ix17,	   ix18,	ix19	 ix20	 ix21		ix22	ix23		ix24	ix25

; Yt formula heuristica esa (0,1,2) ataca, corre o anda
; frAtack 4
; desplaz. ix+20 tampoco lo uso al igual que codanim ix+18 
;----------------------------------------------------------------------------------------
;DELCRAM:
;	;	0,1,2,  3,4,5,6,7, 8,9,10, 11, 12, 13, 14,15, 16, 17,18,19,20,21,22,23,24,25
;	db	1,1,8,120,1,6,2,2,24,3,144,$23,$db,$23,$db, 2,  8, 23, 0, 1, 0, 0, 0, 0, 0,0


; Derechas	#db23(56099)	+#90 (144), #dbb3 +#90, #dc43			3 frames
; Izquierdas	#dcd3(56531)	+#90 (144), #dd63 +#90, #ddf3			3 frames
; Ataque	#de83(56963)	+#90 (144), #df13 +#90, #dfa3 +#90, #e033	4 frames
; Muerte	#e0c3(57539)	+#90 (144), #e153 +#90, #e1e3 +#90, #e303	4 frames


Pre_Delcram	defs	2

GESTION_DELCRAM:
	push	ix
	ld	ix,DELCRAM
;
; Delcram Activo ? Por ejemplo si lo has muerto ya en esa pantalla
;
	ld	a,[ix+1]	;El valor debe ser 1 ya que cuando sea 0 es que lo has matado
	and	a
	jp	z,Muerte_DELCRAM


;
; ATACANDO ? SI ATACA AL PROTA HAY QUE PONER LOS DB'S CORRESPONDIENTES Y CONTAR LOS FRAMES PARA VOLVER
; 
	ld	a,[ix+0]
	and	a
	jp	z,Ataque_DELCRAM


cont_ticks_delcram:
;
; contador de ticks			
;
	ld	a,[ix+4]	
	dec	a
	ld	[ix+4],a
	jp	nz,imprime_delcram

b_cont_ticks_delcram:
;
; pongo el skipframe del db's en ticks
;
	ld	a,[ix+5]		
	ld	[ix+4],a
;
; Comprobamos si es el �ltimo frame
;
	ld	b,[ix+7]	; Total frames
	ld	a,[ix+6]	; frameActual
	cp	b		; andar,ataque o muerte, entonces aqu� habra�que coger el valor ix+7
	jr	z,ponframe_delcram_0
;
; Incremento el valor del frame
;
	inc	a
	ld	[ix+6],a
	call	vuelcafondosprenem	; vuelco el fondo del enemigo
; el tama�o de un frame de Delcram es igual para todas sus animaciones	144bytes
	ld	c,144
	ld	b,0
	ld	l,[ix+11]	; dblowAnim
	ld	h,[ix+12]	; dbhighAnim
	add	hl,bc
	ld	[ix+11],l
	ld	[ix+12],h	; guardo el valor para el pr�ximo frame
	jp	imprime_delcram
;----------------------------------------------------------------------------------------
;
; FRAME A 0, CAMBIO DE SENTIDO, DISCERNIR- (ataque, andar o correr)
;
ponframe_delcram_0:	
	call	vuelcafondosprenem	; vuelco el fondo de la animaci�n

	ld	[ix+6],0
;
; Inicializo los frames a pesar de no saber si va a darse la vuelta el sprite.
;
	ld	a,[ix+13]
	ld	[ix+11],a	; low
	ld	a,[ix+14]
	ld	[ix+12],a	; high
;
; Vamos a ver que sentido lleva Delcram
;
; 0 derecha 1 izquierda
	ld	a,[ix+19]
	and	a		; derecha?
	jr	z,delcram_horizontal_derecha
; 
; Delcram_horizontal_izquierda. Comprobaci�n de limite2
;
delcram_horizontal_izquierda:
	ld	b,[ix+17]
	ld	a,[ix+2]	; posX
	cp	b
	jr	z,fin_limite2_mov_horiz_delcram
;
; en caso contrario no hemos llegado al limite.Pintamos monigote en su nueva posicion.
;
	dec	a
	ld	[ix+2],a	; nueva posX
	call	aimprime_delcram
	jr	imprime_delcram	;	jr aimprime_delcram	en la version anterior

fin_limite2_mov_horiz_delcram:
;
; cambio sentido al enemigo
;
	ld	[ix+19],0	; mandalo a andar a la derecha
;
; db's nuevos para mirar a la derecha
;
	ld	[ix+11],$23
	ld	[ix+13],$23
	ld	[ix+12],$db
	ld	[ix+14],$db
	jr	imprime_delcram
; 
; mov_enem_horizontal_derecha. Comprobaci�n de limite1
;
delcram_horizontal_derecha
	ld	b,[ix+16]
	ld	a,[ix+2]	; posX
	cp	b
	jr	z,fin_limite1_mov_horiz_delcram
;
; en caso contrario no hemos llegado al limite.Pintamos monigote en su nueva posicion.
;
	inc	a
	ld	[ix+2],a	; nueva posX
	call	aimprime_delcram
	ld	a,[ix+15]
	and	a
	jr	nz,imprime_delcram	; En caso de que yendo a derechas te golpee el prota, vuelvete
fin_limite1_mov_horiz_delcram:
;
; cambio sentido al enemigo
;
	ld	[ix+19],1	; mandalo a andar a la izquierda
;
; db's nuevos para mirar a la izquierda
;
	ld	[ix+11],$d3
	ld	[ix+13],$d3
	ld	[ix+12],$dc
	ld	[ix+14],$dc

imprime_delcram:
	call	cambiaranchoenem	; cambio el ancho y llamo a imp_ene en GestionenemigosFinal2.asm
;
; Si est�s imprimiendo la muerte de Delcram, saltat� la detecci�n
;
	ld	a,[vchoqEnem]
	and	a		; si el valor es 1 es que est� muriendo !!
	jr	nz,finDELCRAM	; por lo tanto NO vuelvas a comprobar el choque
;
; Detectar choques con el prota para comprobar si le pega con la espada
;
	call	det_choques_delcram
;
; si hay choque llama a la rutina de poner la animaci�n del enemigo muerto
;
	ld	a,[vchoqEnem]
	and	a
	jr	z,finDELCRAM
	ld	[ix+1],0	; si hay choque poner a 0
finDELCRAM:
	pop	ix
	ret

;
; Impresi�n  de Delcram y discernir seg�n opciones, actuar IA
;

aimprime_delcram:
;
; La IA s�lo ser� tomada en casos de movimiento del personaje, no cuando d� la vuelta
;
	call	IAdelcram	; Traemos el valor de IX+15 desde all�.
	ld	a,[ix+15]
	and	a
	jr	z,IAtaque
baimprime_delcram:
	cp	1
	jr	z,IArapido
	ld	[ix+5],8	; anda
	jr	bIArapido
IAtaque:
	ld	[ix+0],0	; activo atacar
	jr	bIArapido
IArapido:
	ld	[ix+5],3	; corre
bIArapido:
	call	avolcamiento_enem
	ret


;-----------------------------------------------------------------------------------
;
; MUERTE y ATAQUE de DELCRAM la he trasladado a detchoquesDelcram por falta de espacio aqui
;
;-----------------------------------------------------------------------------------