;-------------------------------------------------------------------------------
; IA del Boss -- o algo parecido
;-------------------------------------------------------------------------------


;
; Calculo del valor de ix+15
;
IAdelcram:
;
; Calculo de la variable bossXt puede tomar valores 0,1 o 2
;
calculo_bossXt:
	ld      a,[accion]		; 4/13
        rra	                        ; 1/4 comprobar si est� activo ACCION en el espadazo
        jr      c,blandiendo_espa	; 3/10
	ld	a,[ix+2]
	sub	3
	ld	d,a
	add	a,9
	ld	e,a			; de (colu-3, colu+3)
	ld	a,[ix+3]
	sub	8
	ld	h,a
	add	a,24
	ld	l,a			; hl (fila-8scannes, l es indiferente = suelo)
;
; miramos donde est� el prota seg�n su columna
;
	ld	a,[colu]
	cp	e
	jr	z,esta_proximo
	inc	a		; colu +1
	ld	b,a
	ld	a,d		; coluboss
	cp	b
	jr	nz,noesta_proximo
;
; miramos donde est� el prota seg�n su fila
;
	ld	a,[fila]
	add	a,20
	ld	c,a
	ld	a,h
	cp	c
	jr	c,esta_proximo
noesta_proximo:
	ld	a,1
	jr	fin_calculo_bossXt
esta_proximo:
	ld	a,2
	jr	fin_calculo_bossXt
blandiendo_espa:
	xor	a
fin_calculo_bossXt:
	ld	[ix+15],a	
	ret