;----------------------------------------------------------------------------------------
; Rutina de desplazamiento de sprite       PARADO
; Vamos a desplazar el sprite en un buffer antes de volcarlo con el fondo.
;----------------------------------------------------------------------------------------
;							96 bytes que son 2x3x8 + mask
; A la derecha
desp_spr_der:
	xor	a
        ld      hl,buffrotspr1
	ld	b,24
bparsaltoder:
        scf		;1/4	1b
        rr      [hl]	;4/15	2b
        inc     hl	;1/6	1b
        rra		;1/4	1b
        srl     [hl]	;4/15	2b
        inc     hl	;1/6	1b
        rla		;1/4	1b
        rr      [hl]	;4/15	2b
        inc     hl	;1/6	1b
        rra		;1/4	1b
        rr      [hl]	;4/15	2b
        inc     hl	;1/6	1b	;16bx24=384bytes 100tstatesx24=2400tstates
	djnz	bparsaltoder	;3/13 x23 = 299 + 100tstatesx24=2400tstates =2799 + 8 = 2807 tstates
	ret		; dif 2807-2400=407tstates x desplaz. para 3=1221 tst  para 6= 2442 tstates
; A la izqquierda
desp_spr_izq:
	xor	a
	ld      hl,buffrotspr1+95
	ld	b,24
bparsaltoizq:
	sla     [hl]	; sprite3
	dec	hl
	rra		; acarreo sprite3 en bit 7,a
;El flag de acarreo lo activo para mask3
	scf		
	rl	[hl]	; mask3
	dec	hl
	rla		; acarreo mask3 en bit 0,a
;En el flag de acarreo tengo el acarreo del sprite3
	rl	[hl]	; sprite2
	dec	hl
	rra		; acarreo sprite2 en bit 7,a
	rl	[hl]	; mask2
	dec	hl
;En el flag de acarreo tengo el acarreo del sprite2
	djnz	bparsaltoizq
	ret
;----------------------------------------------------------------------------------------
; Rutina de desplazamiento de sprite       ESPADAZO	3X3X8
;							144bytes que son 3x3x8=sprite +masc

; A la derecha
desp_spr_esp_der:
	xor	a
        ld      hl,buffespspr1
	ld	b,24
bparsaltoderesp:
        scf
        rr      [hl]
        inc     hl
        rra
        srl     [hl]
        inc     hl
        rla
        rr      [hl]
        inc     hl
        rra
        rr      [hl]
        inc     hl
	rla
        rr      [hl]
        inc     hl
        rra
        rr      [hl]
        inc     hl
	djnz	bparsaltoderesp
	ret
; A la izqquierda
desp_spr_esp_izq:
	xor	a
	ld      hl,buffespspr1+143
	ld	b,24
bparsaltoizqesp:
	sla     [hl]	; sprite3
	dec	hl
	rra		; acarreo sprite3 en bit 7,a
;El flag de acarreo lo activo para mask3
	scf		
	rl	[hl]	; mask3
	dec	hl
	rla		; acarreo mask3 en bit 0,a
;En el flag de acarreo tengo el acarreo del sprite3
	rl	[hl]	; sprite2
	dec	hl
	rra		; acarreo sprite2 en bit 7,a
	rl	[hl]	; mask2
	dec	hl
	rla		; acarreo mask2 en bit 0,a
;En el flag de acarreo tengo el acarreo del sprite2
	rl	[hl]	; sprite1
	dec	hl
	rra		; acarreo sprite1 en bit 7,a
	rl	[hl]	; mask1
	dec	hl
	djnz	bparsaltoizqesp
	ret