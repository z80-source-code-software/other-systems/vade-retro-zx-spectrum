;----------------------------------------------------------------------------------------
; Colocamos objetos en pantalla
;----------------------------------------------------------------------------------------
ponerobjetos:
	ld	de,09
	ld	hl,tabla_Objetos -09
	ld	a,[panta]
	ld	b,a
bponerobj1:
	add	hl,de
	ld	a,[hl]			
	cp	255			; fin tabla
	ret	z
	cp	b			; pantalla
	jr	nz,bponerobj1
objetoencontrado:
	inc	hl			
	ld	a,[hl]			; objeto activo o sin recoger
	and	a
	ret	z			; Solo habr� un objeto a coger en pantalla
	ld	[churrObj],hl		; Guardo la posic. hl en la variable para usarla despu�s.
	inc	hl
	ld	a,[hl]			; numero de tile / objeto a pintar
	ld	[num_tile],a
	inc	hl
	ld	a,[hl]			; columna en char
	ld	[col_dat],a
	inc	hl
	ld	a,[hl]			; fila en char
	ld	[fil_dat],a
	inc	hl
	ld	a,[hl]
	ld	[tipObjeto],a
	ld	hl,mirarobjetos
	ld	[vermirarobjetos+1],hl
	call	search_dat		; En bupaim.asm. Salida tile a imprimir en 'de'
	jp	c_dirbufftile
;----------------------------------------------------------------------------------------
;
; Contiene: Pantalla, Activado ?,num.tile, pos. X char, pos. Y pixels, tipo Objeto
; Siguen los datos de los objetos que le indican la posic. en el marcador a impr
; X char, Y char, tile1x1 o 2x1  o 2x2 valores de tiles para borrar el obje.en pantalla
; tipos de Objeto: 0=calaveras/energia, 1=sello, 2=p�cima, 3=espada, 4=puertas, 5=enemigos
; Para las palancas de activacion de puertas los db's son ligeramente distintos.
; Al no imprimirse 1 no se usa y el otro ser�a para restaurar el valor de num.tile
; 28558
tabla_Objetos:
	db	 0,1,27, 5,128,3,20,21,71	; tipo 3= espada	1x1
	db	 1,1,30,25,128,3,21,21,71	;			1x1
	db	 3,1,28,15,136,3,19,22,72	;			2x1
	db	 7,1,77,20,104,0,11,22,71	; calavera que estaba en la pant.50
	db	 8,1,22,22, 96,1, 1,21,73	; tipo 1= sello		2x2
	db	13,1,31,23, 72,3,18,23,71	;			1x1
	db	16,1,23, 6, 64,1, 6,21,73	;			2x2
	db	21,1,26,17, 64,3,21,22,71	;			1x1		
	db	25,1,29,11,144,2,16,22,72	; tipo 2= pocima	2x1
	db	26,1,25, 8,120,1,29,21,73	;			2x2
	db	27,1,77, 5,120,0,10,22,71	; tipo 0= calavera      1x1
	db	28,1,24, 2, 96,1,24,21,73	;			2x2
	db	33,1,77,28, 56,0,13,22,71	; calavera que estaba en la 52
	db	35,1,75,14,136,4,14,136,73	; tipo 4= puerta
	db	39,1,77,10,120,0,12,22,71	;
	DB	40,1,80, 5,104,5, 5,104,73	; tipo 5= desactiva ene 2x2
	db	46,1,77,29, 64,0,14,22,71
	db	47,1,75, 7, 64,4, 7,64,73
	db	52,1,80,18, 72,5,18,72,73
	db	55,1,75,16, 56,4,16,56,73
	db	56,1,80, 6, 80,5, 6,80,73
	db	57,1,77, 4,144,0,15,22,71	; antes en la 56
	db	255
