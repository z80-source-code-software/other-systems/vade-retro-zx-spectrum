;----------------------------------------------------------------------------------------
; Calculo de la posic.en pant. y en el buffer del enemigo o la plataforma 
;----------------------------------------------------------------------------------------
; Se eliminan las lineas, directamente saltamos a im_pix_p
;avolcamiento_plat:
;       call    im_pix_p      ; hl=dir_spr POSIC.ARCH.PANT       40890    SprProtaImp.asm
;	call    c_dirbuff_p   ; hl=d_bu_spr Posic. en buffer
;	ret
;----------------------------------------------------------------------------------------
; Rutina de volcado del fondo de pantalla que ocupa el sprite. Los enemigos tienen 
; que tener un m�ximo de 2 char de ancho para esta rutina o descomentar las lineas para 3
;----------------------------------------------------------------------------------------
vuelcafondosprplat:						;40897
	ld	hl,[d_bu_spr_p]	; viene de c_dirbuff
	ld	de,[dir_spr_p]	; viene de im_pix	
; cargar en C el tama�o ancho x alto x 8 del enemigo
	ld	bc,16
plat_lazoavolc2:
	ldi
	ldi
; m�s optimizacion metal
	ret	po	; Hay Paridad cuando bc=0
	ld	a,c
;	and	a
;	ret	z
	ld	c,30	; posicion imediata debajo del scann anterior
	add	hl,bc
	ld	c,a
	dec	de	; con esto queda resuelto el que la colu provoque un acarreo
	dec	e
	inc	d
        ld      a,d
        and     7
        jr      nz,plat_lazoavolc2  ; baja una l�nea dentro del caracter
        ld      a,#20
        add     a,e
        ld      e,a
        jr      c,plat_lazoavolc2   ; cambio de tercio
        ld      a,d
        sub     8
        ld      d,a
        jr      plat_lazoavolc2     ; cambio de caracter
