;----------------------------------------------------------------------------------------
; movimientos 
;----------------------------------------------------------------------------------------
;	org	40194
;--------------------------------------------------------------------------
; Tabla de cambio de direccion del prota. Frames
tabla_chg_spr:
		db	0,4
		db	1,4
		db	2,4
		db	3,4
		db	4,2
		db	5,1
;--------------------------------------------------------------------------
; Tabla de calculo del desplazamiento del frame en buffrotspr
tabla_des_spr:
		db	0,0
		db	1,3
		db	2,6
		db	3,0
		db	4,3
		db	5,6
;----------------------------------------------------------------------------------------
; EVENTO DE SALTO- Desarrollo Entrada en 'a'= valor  'e'= puls. teclas. 
;----------------------------------------------------------------------------------------
tab_sal_ve:				
        db       -4,-4,-4,-4,-2,-2,-2,-2,2,2, 2, 2, 4, 4, 4, 4
;----------------------------------------------------------------------------------------

CONT_SALTA:			
	ld	a,[bytevesal]
	cp	16			; Comprobamos si bytevesal es 16 para anularlo
	jr	z,ANTES_DE_SALTA_ACTU_ATRs
	cp	8			;
	jp	c,CONT_SALTA_SUBIENDO	; En caso contrario est�s bajando.
;
; BAJANDO 
;
	ld	a,[skipf_p]
	cp	1
	jr	c,aumento_skipf_salto
	xor	a
	ld	[skipf_p],a
;
; Antes de comprobar si abajo est� libre para bajar, vamos a comprobar si a los lados lo est�
;
; Movimiento a la derecha o izq ?
	ld      a,[dirmovsp]
	cp	1			; a derecha
	jr	z,ab_comp_ob_der_sal
	cp	3			; a izquierda
	jr	nz,SOLOenverti		; resto direcciones
; bajando a izquierdas
	call	obst_iz
ab_acompobdersal:
	and	a
	jr	z,SOLOenverti		; si hay obstaculo entonces solo en vertical.
;
; compruebo el suelo
;
	call	obst_aba
	and	a
; En el caso de que haya encontrado un obstaculo al bajar, ANULA el salto.
	jr	Z,AANTES_DE	
	jr	TIRA_POR_TABLA		; sino el salto es normal
;bajando a derechas
ab_comp_ob_der_sal:
	call	obst_de
	jr	ab_acompobdersal
;
; En el caso de que haya encontrado un obstaculo a un lado, no puedes avanzar en esa direccion.
;
SOLOenverti:
	call	obst_aba
	and	a
; En el caso de que haya encontrado un obstaculo al bajar, ANULA el salto.
	jr	Z,AANTES_DE
	call	vuelcafondosprprota	; imprimo el fondo con los datos previos de fila y colu
; usamos bytevesal para recorrer la tabla de salto en vertical
	ld	a,[bytevesal]
	ld      hl,tab_sal_ve
	add     a,l
        ld      l,a                     ; hl apunta al valor dentro de la tabla
	ld	a,[fila]
	add	a,[hl]
	ld	[fila],a
	ld	hl,bytevesal
	inc	[hl]			; Actualizo bytevesal
	jp	im_pix		; Antes saltabas a avolcamiento en SprProtaVolc.asm

aumento_skipf_salto:
	inc	a
	ld	[skipf_p],a
	ret
;
; salida desde la parte de bajada del salto
;
AANTES_DE:
	ld	a,[flagpa]
	rra
	ret	c

; 
; colocar el sprite si es necesario en una posicion absoluta.
;
ajustesalto:
	ld	a,[fila]
	and	7
	jr	z,ANTES_DE_SALTA_ACTU_ATRs
	call	vuelcafondosprprota	; imprimo el fondo con los datos previos de fila y colu
	ld	a,[fila]
	and	248
	ld	[fila],a
	call	im_pix		; Antes saltabas a avolcamiento en SprProtaVolc.asm
;
; salida desde la parte de subida del salto
;
ANTES_DE_SALTA_ACTU_ATRs:
; Tienes que anular el salto.
	xor	a
	ld	[accion],a
	ld	[bytevesal],a
	ld	[proparado],a		; para colocar la animaci�n en parado
; Sonido al caer
	ld	a,2
	ld	[repe_fx],a
        ld	a,15		
        ld	[long_fx],a
	ld	hl,convalorson		; etiquetas en la rutina de interrupciones
	ld	[vervalorson+1],hl
	ret
;
; SUBIENDO.- Comprobar obstaculos arriba de la actual posici�n de salto.
;
CONT_SALTA_SUBIENDO:
	ld	a,[skipf_p]
	cp	1
	jr	c,aumento_skipf_salto
	xor	a
	ld	[skipf_p],a
	call	obst_arr
	and	a
	jr	z,B_ANTES_DE_SALTA_ACTU_ATRs       ; Anula salto si tropiezas obst�culo
TIRA_POR_TABLA:
	call	vuelcafondosprprota	; imprimo el fondo con los datos previos de fila y colu
; usamos bytevesal para recorrer la tabla de salto en vertical
	ld	a,[bytevesal]
	ld      hl,tab_sal_ve
	add     a,l
        ld      l,a                     ; hl apunta al valor dentro de la tabla
	ld	a,[fila]
	add	a,[hl]
	ld	[fila],a
	ld	hl,bytevesal
	inc	[hl]			; Actualizo bytevesal
	call	im_pix		; Antes saltabas a avolcamiento en SprProtaVolc.asm
; no comprobamos el suelo en el salto hacia arriba
	ld	a,[keypush]		; Para comprobar movim. horizontal salto.
	and	12		; anulo la pulsaci�n de las teclas arriba y espacio
	ld	[keypush],a
	jp	CUA_EVEN_CONT	

B_ANTES_DE_SALTA_ACTU_ATRs:
	ld	a,[flagpa]
	rra	
	ret	c
; Tienes que anular la subida el salto. Valores 0,2,4 a 16,12, 8
	ld	a,[bytevesal]
	add	a,a			; pongo el indice en su posic.inversa a la subida.
	PUSH	DE
	ld	d,a
	ld	a,16
	sub	d
	ld	[bytevesal],a
	ld	e,a
	ld	d,0
	ld	hl,tab_sal_ve
	add	hl,de
	POP	DE			; Si no es necesario no guardar.
	ret

;----------------------------------------------------------------------------------------
; PREPARACION DE EVENTO DE SALTO
;----------------------------------------------------------------------------------------
preparar_salto:
	xor	a			; skipframe a 0
	ld	[skipf_p],a
	ld	[proparado],a
	ld	a,2
	ld	[accion],a		; activo el salto
	ld	[anchosp],a		; ancho 2
	ld	hl,vuelcafondo
	ld	[vervuelcafondo+1],hl
;
; Usamos prota_parado para poner el frame1 como salto. Desplazandolo seg�n el frame
;
	ld	hl,BAN_IZ+1		; Etiq.direcci�n de sprite a izquierdas.
	ld	[hl],$f0		; coloco frame1 como el sprite de salto 
	ld	hl,BAN_DE+1		; Etiq.direcci�n de sprite a derechas.
	ld	[hl],$90		; coloco frame1 como el sprite de salto 
	call	PROTA_PARADO		; 
	ld	[spr],hl
	ld	hl,BAN_IZ+1		; Etiq.direcci�n de sprite a izquierdas.
	ld	[hl],$90		; Restauro el frame0 de parado en la rutina
	ld	hl,BAN_DE+1		; Etiq.direcci�n de sprite a derechas.
	ld	[hl],$30		; Restauro el sprite de parado de la rutina
	ret
;----------------------------------------------------------------------------------------
; Protagonista PARADO
;----------------------------------------------------------------------------------------
PROTA_PARADO:
	ld	a,[proparado]
	and	a
	jr	nz,fin_parado		; Si ya has pasado por aqui retorna.
; Discriminamos el sentido del prota.
	ld	a,[dirmovsp]
	cp	1
	jr	z,BAN_DE
; PARADO A IZQUIERDAS
BAN_IZ:					; WARNING- Usada en el Engine para cambiar el valor
	ld	hl,$e490		; spr0=arado izq ($90,$e4) spr1=salto izq ($f0,$e4)
	ld	[spr],hl
	call	pon_spr_buff		; vuelco el frame parado del sprite en el buffer
	call	busq_num_desp		; calculo el valor del desplazamiento Valor en A y B
	and	a			; si el valor es 0 no lo desplaces
	jr	z,BBAN_DE
LOOPDESPIZ:
	push	bc
	call	desp_spr_izq		; desplazo a la izquierda el frame
	pop	bc
	djnz	LOOPDESPIZ
	jr	BBAN_DE
; PARADO A DERECHAS
BAN_DE:					; WARNING- Usada en el Engine para cambiar el valor
	ld	hl,$e730		; spr0=arado der ($30,$e7) spr1=salto der ($90,$e7)
	ld	[spr],hl
	call	pon_spr_buff		; vuelco el frame parado del sprite en el buffer
	call	busq_num_desp		; calculo el valor del desplazamiento Valor en A y B
	and	a			; si el valor es 0 no lo desplaces
	jr	z,BBAN_DE
LOOPDESPDE:
	push	bc
	call	desp_spr_der		; desplazo a la derecha el frame
	pop	bc
	djnz	LOOPDESPDE
BBAN_DE:
	ld	a,1
	ld	[proparado],a
	jr	bfin_parado
fin_parado:
	ld	hl,sinvolcarfondo	; etiqueta para saltar en el engine
	ld	[vervuelcafondo+1],hl
bfin_parado:
	ld	hl,buffrotspr1		; Cargamos hl con el buffer que luego dejaremos en spr
	ret
busq_num_desp:
	ld	hl,tabla_des_spr	; movimientos.asm
	ld	a,[frame]
	add	a,a
	add	a,l
	ld	l,a
	inc	hl
	ld	a,[hl]
	ld	b,a
	ret
;----------------------------------------------------------------------------------------
; Impresion de espadazo. Se hace necesario al menos 3 variables: espada,
; flagespa y accion. No dispara sin tener espada, flagespa intercambiar�
; animaci�n entre frames y acci�n es el flag de mandoble activo o no.
;----------------------------------------------------------------------------------------
ESPADAZO:
; Para TIMER and a , skipf_p = 3
	ld	a,[skipf_p]
	CP	2		; EN PLATAFORMAS 4 SKIPF_P DEJA COLGADO AL PROTA
	jr	z,reset_skipf_espada
	inc	a
	ld	[skipf_p],a
	ret
reset_skipf_espada:
	xor	a
	ld	[skipf_p],a
	call	vuelcafondosprprotaEsp	; volcar el fondo de la espada 3x3
; Que sentido lleva el prota?
	ld      a,[dirmovsp]
        cp      1
        jr      z,disp_de		; izquierda o derecha
;--------------------------------------------------------------------------
; Espadazo a la izquierda. 
;--------------------------------------------------------------------------
	ld	a,[flagespa]
	inc	a			; incremento el valor para el siguiente.
	ld	[flagespa],a		; Que frame es ?
	cp	3			; Es el �ltimo ?
	jr	z,FIN_ESPADAZO_IZ
; Sacamos el valor que debe tener hl
	ld	hl,[DbEspFr]		; Frame Actual del Espadazo
	ld	bc,$90			; Valor del tama�o del frame Espadazo Actual.
	add	hl,bc
	ld	[DbEspFr],hl		; Guardo el nuevo valor.
; Vuelco el nuevo frame ESPADAZO en el buffer
	ld	[spr],hl
	call	pon_spr_esp_buff		
	call	busq_num_desp		; calculo el valor del desplazamiento Valor en A y B
; Desplazar esta animaci�n antes de volcarla a pantalla con frame
	and	a			
	jr	z,BLDESP_IZ_frame2	; si el valor es 0 no lo desplaces
LOOPDESPESPADAZO_IZ_frame2:
	push	bc
	call	desp_spr_esp_izq		; desplazo a la derecha el frame
	pop	bc
	djnz	LOOPDESPESPADAZO_IZ_frame2
BLDESP_IZ_frame2:
	ld	hl,buffespspr1		; Cargamos hl con el buffer que luego dejaremos en spr
	jp	BNO_ANIMA		; Para meter el valor en spr
FIN_ESPADAZO_IZ:
	call	vuelcafondosprprotaEsp	; volcar el fondo de la espada 3x3
	ld	hl,colu			; incremento la columna para dejar la pos.anterior
	inc	[hl]			; de la espada con 3 de ancho a los 2 del parado
	call	im_pix		; Antes saltabas a avolcamiento en SprProtaVolc.asm
;	call	avolcamiento
FIN_ESPADAZO_DE:
	ld	a,2
	ld	[anchosp],a
; Ojo aqui cambiar el valor de la etiqueta a 3 para el anchosp
	exx				; Guardo los registros, necesito hl
	ld	hl,chganchosp+1		; etiqueta en la rutina SprProtaImp.asm
	ld	[hl],a			; cambio el valor de B que es el anchosp 
	exx				; saco el valor de hl
	ld	a,[accion]
	res	0,a
	ld	[accion],a
	xor	a
	ld	[flagespa],a
	jp	SIN_MOV			;ANTES MOVER
;--------------------------------------------------------------------------
; Espadazo a la derecha. 
;--------------------------------------------------------------------------
disp_de:                                
	ld	a,[flagespa]		; Que frame es ?
	inc	a			; incremento el valor para el siguiente.
	ld	[flagespa],a
	cp	3			; Es el �ltimo ?
	jr	z,FIN_ESPADAZO_DE
; Sacamos el valor que debe tener hl
	ld	hl,[DbEspFr]		; Frame Actual del Espadazo
	ld	bc,$90			; Valor del tama�o del frame Espadazo Actual.
	add	hl,bc
	ld	[DbEspFr],hl		; Guardo el nuevo valor.
; Vuelco el nuevo frame ESPADAZO en el buffer
	ld	[spr],hl
	call	pon_spr_esp_buff		
	call	busq_num_desp		; calculo el valor del desplazamiento Valor en A y B
; Desplazar esta animaci�n antes de volcarla a pantalla con frame
	and	a			
	jr	z,BLDESP_DE_frame2	; si el valor es 0 no lo desplaces
LOOPDESPESPADAZO_DE_frame2:
	push	bc
	call	desp_spr_esp_der		; desplazo a la derecha el frame
	pop	bc
	djnz	LOOPDESPESPADAZO_DE_frame2
BLDESP_DE_frame2:
	ld	hl,buffespspr1		; Cargamos hl con el buffer que luego dejaremos en spr
	jp	BNO_ANIMA		; Para meter el valor en spr
