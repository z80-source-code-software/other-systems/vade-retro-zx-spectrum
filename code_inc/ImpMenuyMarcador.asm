;----------------------------------------------------------------------------------------
; impresion de menu  y marcador
;----------------------------------------------------------------------------------------
        org     24300
;-----------------------------------------------------------------------
; Rutina de pulsaci�n de tecla y soltado
Espera_tecla:
        xor     a
        in      a,(#fe)
        cpl
        and     #1f
        jr      z,Espera_tecla
	ret
;
; Soltado de teclas
;
Soltar_tecla:
	xor	a
        in      a,(#fe)
        cpl
        and     #1f
	jr	nz, Soltar_tecla
	ret
;----------------------------------------------------------------------------------------
; Rutina de borrado de pantalla completo
ruborco:
        ld      hl,#4000
        ld      de,#4001
        ld      [hl],l
        ld      bc,6144+767
        ldir
        ret
;----------------------------------------------------------------------------------------
imprimenu:
; El titulo se ha comprimido para tener espacio por lo que tenemos que llamar antes al descompresor
	call	descomprimetitulo	; en titulo.asm 
        ld      de,64000		; direccion donde se ha descomprimido
        ld      hl,txttitulo
	call    llamimprim
; espada
        ld      de,cachoespada
        ld      hl,txttitulo1
	call    llamimprim

        ld      de,cachoespada
        ld      hl,txttitulo2
	call    llamimprim
; sueloespada
        ld      de,resto_letrero
        ld      hl,txttitulo3
	call    llamimprim
; empu�adura
        ld      de,empunadura
        ld      hl,txttitulo4
	call    llamimprim
;Aqui pon las lineas 1 keyboard 2 joystick
        ld      de,letracontrol
        ld      hl,txtletrc
	call    llamimprim
;Imprimo logotipo retroworks
        ld      de,logo
        ld      hl,txtlogom
;----------------------------------------------------------------------------------------
; Impresion en caracteres
llamimprim:
;        call    impdatmenu
;       call    im_char			; para ahorrar t-states
;       call    imprbloques             ; " 
;        ret
impdatmenu:
        ld     a,[hl]
        ld     [al_tile],a
        inc    hl
        ld     a,[hl]
        ld     [an_tile],a
        inc    hl
        ld     a,[hl]
        ld     c,a                        ; COLUMNA
        ld     [col_dat],a
        inc    hl
        ld     a,[hl]
        ld     b,a                        ; FILA
        ld     [fil_dat],a
;        ret
;----------------------------------------------------------------------------------------
;RUTINA DE COORD.POR CARACT DE MH B=FILA y C=COLUMNA EN CARACTERES
;----------------------------------------------------------------------------------------
im_char:
        ld     h,#40
        ld     a,b
        and    24
        add    a,h
        ld     h,a
        ld     a,b
        and    7
        rrca
        rrca
        rrca
        add    a,c
        ld     l,a
;        ret	c�digo 201 ; valor que pongo desde gestacciones.asm gracias a la etiq.
;----------------------------------------------------------------------------------------
imprbloques:    ;  Para que el c�digo lo pueda modificar desde gestacciones.asm 201=ret 
        ld     a,[al_tile]	; 4/13 58= ld a,[nn] debo restaurar esta orden posteriormente.
        ld     c,a              ; 1/4  ALTURA
        ld     a,[an_tile]	; 4/13
        ld     b,a		; 1/4

i_arriba:
	ex     af,af'		; 1/4  respaldo de B
	push   hl		; 3/11
i_bu:	
	ld     a,[de]		; 2/7
        ld     [hl],a		; 2/7
        inc    de		; 1/6
        inc    l		; 1/4
        djnz   i_bu		; 3/13 , 2/8
        pop    hl		; 3/11
        inc    h		; 1/4
        ld     a,h		; 1/4	
        and    7		; 2/7
        jr     nz,i_sal2	; 3/12 , 7/7
        ld     a,l		; 1/4
        add    a,32		; 2/7
        ld     l,a		; 1/4
        jr     c,i_sal2		; 3/12 , 7/7
        ld     a,h		; 1/4
        sub    8		; 2/7
        ld     h,a		; 1/4
i_sal2:

	ex     af,af'		; 1/4  saco el valor de B=an_tile
	ld     b,a		; 1/4

	dec    c		; 1/4
        jr     nz,i_arriba	; 3/12 , 7/7
;----------------------------------------------------------------------------------------
impattr_gen:
;B=FILA y L=COLUMNA EN CARACTERES
        ld     a,[fil_dat]
        ld     b,a
        ld     a,[col_dat]
        ld     l,a
;llamada a RUTINA DE COORD. DE ATRIBUTOS  DATOS SPRPROTAIMP.ASM
        call   coordficu
i_at_nor:
        ld     a,[al_tile]
;        srl    a
;        srl    a
;        srl    a         ; DIVIDIMOS EL ALT_TILE ENTRE 8 PARA QUE EL VALOR SEA EN CHARS.
	rra
	rra
	rra		; Igual que lo anterior pero trasladamos el acarreo
	and	31	; que aqu� anulamos. Con esto ahorramos bytes y 5 estados al menos
i_atb:
	ex	af,af'			; Guardo el Alto en af'
        push   hl                      ; LA POSICION DE HL EN A.ATTR
        ld     a,[an_tile]
        ld     b,a
i_atb1:
        ld     a,[de]
        ld     [hl],a
        inc    de
        inc    l
        djnz   i_atb1
        pop    hl                      ; SACAMOS LA POSICION ATTR
        ld     c,$20
        add    hl,bc

	ex	af,af'			; Saco el Alto de af'
	dec	a			; 
	jr	nz,i_atb		; 3/12
	ret
;FIN IMPRESION DE MARCADORES

;----------------------------------------------------------------------------------------
;SUBRUTINA DE IMPRESION DE MARCADORES Y TILES
;----------------------------------------------------------------------------------------
empezamos:                      ; ojo cambia ya el nombre a la etiqueta EMPEZAMOS:
;impresion marcador de arriba
 	call   descomprimemarcadorarriba	; en marcadorarriba.asm 
        ld     de,64000				; direccion donde se ha descomprimido
        ld     hl,txtmarca
        call   llamimprim
;impresion marcador de abajo
 	call   descomprimemarcadorabajo		; en marcadorarriba.asm 
        ld     de,64000				; direccion donde se ha descomprimido
        ld     hl,txtmarca2
	jp	llamimprim
;	call   llamimprim
;        ret
;impresion final del juego
imprim_fin_juego:
; El final se ha comprimido para tener espacio por lo que tenemos que llamar antes al descompresor
	call	descomprimefinal	; en finalgfx.asm 
        ld      de,64000		; direccion donde se ha descomprimido
        ld     hl,txtfinal
        call   llamimprim
	ld     de,text_theend
        ld     hl,txttheend
	jp	llamimprim
;	call   llamimprim
;        ret
imprim_gameover:
        ld     de,gameover
        ld     hl,txtgameover
	jp	llamimprim
;	call   llamimprim
;        ret
txttitulo      defb 32,19,7,5
txtletrc       defb 32,7,11,11
txtlogom       defb 24,7, 2,21

txttitulo1     defb 32,2,21,12
txttitulo2     defb 32,2,21, 9

txttitulo3     defb 32,12,14,16
txttitulo4     defb 40,2,21,0

txtmarca       defb 32,32,0,0
txtmarca2      defb 32,32,0,20

txtfinal       defb 48,6,12,6

txttheend      defb 16,7,11,14
txtgameover    defb 16,7,12,10





; LOGO RETROWORKS
logo:
	DEFB	  0,  0,  0,  0,  0,  0,  0
	DEFB	  0,  0,  0,  0,  0,  0,  0
	DEFB	  0,  0,  0,  0, 64,  0,  0
	DEFB	  0,  0,  0,  2,  0,  0,  0
	DEFB	  0,  0,  0,  0,  6,  0,  0
	DEFB	  0,  0,  0,  8,203,  0,  0
	DEFB	  0,  0,  0,  0,207,  0,  0
	DEFB	  0,  0,  0,  0,  6,  0,  0

	DEFB	  7,  1,  0,  8,195,  8,  0
	DEFB	  8,135,192, 42,161,  8, 64
	DEFB	  8,153,  3, 42, 64, 10,160
	DEFB	  9, 37, 52,170,  0,106,128
	DEFB	 14, 61, 36,170,  1, 76, 64
	DEFB	  9, 33, 36,170,  2, 76, 32
	DEFB	  8,161, 36,182,133, 76, 32
	DEFB	  8,125, 35, 34,195, 74,192

	DEFB	  8,125, 35, 34, 16, 74,192
	DEFB	  0,  0,  0,  0, 16,  0,  0
	DEFB	  8,125, 35, 34, 40, 74,192
	DEFB	  0,  0,  0,  0, 56,  0,  0
	DEFB	  0,  0,  0,  0, 56,  0,  0
	DEFB	  0,  0,  0,  0, 16,  0,  0
	DEFB	  0,  0,  0,  0,  0,  0,  0
	DEFB	  0,  0,  0,  0, 16,  0,  0
; (Attributes here, insert label if needed)
	DEFB	  0,  0,  0,  2,  2,  0,  0
	DEFB	 71, 70, 70, 70, 80, 70, 70
	DEFB	 66, 66, 66, 66,  2, 66, 66

