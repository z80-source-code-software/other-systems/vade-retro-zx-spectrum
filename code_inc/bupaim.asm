;----------------------------------------------------------------------------------------
; BUSQUEDA DE PANTALLA E IMPRESION DE DATOS
;----------------------------------------------------------------------------------------
search_scr:
           ld   a,[panta]
           ld   hl,tabpant    ; DATOS DE LAS DIRECCIONES DONDE ESTAN LAS PANTALLAS.
           add  a,a
           add  a,l
           ld   l,a
           ld   a,[hl]
           ld   e,a
           inc  hl
           ld   a,[hl]
           ld   d,a
;
; Dejo DE en HL= DIR.PANTALLA A IMPRIMIR  dentro de dataspantCo.asm
;
	   ex   de,hl         
;VALORES SIGUIENT.SERAN LOS TILES QUE COMPONEN LA PANT.Y SU POSC.X en char E Y en scannes
           inc  hl
a_frm:
           ld   a,[hl]
           cp   $ff           ; 2/7 
           ret  z             ; fin rutina al encontrar 255
	   ld   [num_tile],a  ; GUARDAMOS EL TILE QUE QUEREMOS IMPRIMIR (antes ve_tile)
           inc  hl
           ld   a,[hl]
           ld   [fil_dat],a
           inc  hl
           ld   a,[hl]
           ld   [col_dat],a   ; Guardamos la fila en scann y la colu en char del tile
           inc  hl
           push hl            ; GUARDAMOS POSIC.HL QUE APUNTA AL SIGUIENTE TILE A BUSCAR
           call search_dat    ; BUSCAMOS EL TILE A INYECT.EN BUFF_PANT en TABLA DE DATOS.
           call c_dirbufftile ; CALC.DIRECCION EN BUFFER e inyeccion de tile y att.
           pop  hl            ; SACAMOS EL VALOR DEL SIGUIENTE TILE A IMPRIMIR Y SEGUIMOS
           jr   a_frm
;----------------------------------------------------------------------------------------
; SUBRUTINA DE BUSQUEDA DE LOS DATOS QUE COMPONEN LAS PANTALLAS
;----------------------------------------------------------------------------------------
search_dat:
           ld   de,tab_dir_tiles
	   ld	a,[num_tile]
	   add	a,a
	   ld	l,a
	   ld	h,0
	   add	hl,de
	   ld	e,[hl]
	   inc	hl
	   ld	d,[hl]
encontrado:
           inc  de
           ld   a,[de]         ; ALTO
           ld   [al_tile],a    ; ALTURA DEL TILE EN SCANNES PARA HACER LDIR
           inc  de
           ld   a,[de]         ; ancho
           ld   [an_tile],a    ; ANCHURA DEL TILE EN CARAS PARA HACER LDIR
           inc  de
           ld    [poti_im],de   ; tarda 6/20 mucho menos que lo anterior
           ret
; Tabla de las direcciones de los tiles
tab_dir_tiles:
	db	$4a,$88	;0
	db	$dd,$88	;1
	db	$04,$89	;2
	db	$4f,$89	;3
	db	$9a,$89	;4
	db	$c1,$89	;5
	db	$e8,$89	;6
	db	$7b,$8a	;7
	db	$0e,$8b	;8
	db	$a1,$8b	;9
	db	$c8,$8b	;10
	db	$ef,$8b	;11
	db	$ca,$8c	;12
	db	$f1,$8c ;13
	db	$06,$8d ;14
	db	$05,$8e ;15
	db	$1a,$8e ;16
	db	$53,$8e ;17
	db	$95,$8e	;18
	db	$3a,$8f ;19
	db	$c4,$8f	;20
	db	$f4,$8f	;21
	db	$00,$6c	;22	datastileobjeto
	db	$27,$6c	;23	"
	db	$4e,$6c	;24	"
	db	$75,$6c	;25	"
	db	$9c,$6c	;26	"
	db	$a8,$6c	;27	"
	db	$b4,$6c	;28	"
	db	$c9,$6c	;29	"
	db	$de,$6c	;30	"
	db	$ea,$6c	;31	"
	db	$24,$90 ;32
	db	$4b,$90 ;33
	db	$96,$90	;34
	db	$e1,$90	;35
	db	$2c,$91	;36
	db	$bf,$91	;37
	db	$52,$92	;38
	db	$a6,$92	;39
	db	$15,$93	;40
	db	$60,$93	;41
	db	$6c,$93	;42
	db	$93,$93	;43
	db	$ba,$93	;44
	db	$05,$94	;45
	db	$74,$94	;46
	db	$92,$94	;47
	db	$e6,$94	;48
	db	$3a,$95	;49
	db	$61,$95	;50
	db	$76,$95	;51
	db	$e5,$95	;52
	db	$1e,$96	;53
	db	$57,$96	;54
	db	$90,$96	;55
	db	$b7,$96	;56
	db	$de,$96	;57
	db	$05,$97	;58
	db	$2c,$97	;59
	db	$53,$97	;60
	db	$9e,$97 ;61
	db	$c5,$97 ;62
	db	$ec,$97	;63
	db	$40,$98	;64
	db	$24,$99	;65
	db	$a1,$9a	;66
	db	$ec,$9a	;67
	db	$37,$9b	;68
	db	$f6,$6c	;69	datastileobjeto

	db	$1d,$6d	;70	"
	db	$32,$6d	;71	"
	db	$3e,$6d	;72	"
	db	$53,$6d	;73	"
	db	$7a,$6d	;74	"
	db	$a1,$6d ;75	"
	db	$c8,$6d ;76     "
	db	$dd,$6d ;77	"
	db	$e9,$6d	;78	"
	db	$f5,$6d	;79	"
	db	$01,$6e ;80	"




; datastile OBJETOS

;	db	$00,$6c	;22 SELLO
;	db	$27,$6c	;23 "
;	db	$4e,$6c	;24 "
;	db	$75,$6c	;25 "
;	db	$9c,$6c	;26 CRUCETA DERE ESPADA
;	db	$a8,$6c	;27 CRUCETA IZQ. ESPADA
;	db	$b4,$6c	;28 MASTIL
;	db	$c9,$6c	;29 POCION
;	db	$de,$6c	;30 POMO ESPADA
;	db	$ea,$6c	;31 PUNTA ESPADA
