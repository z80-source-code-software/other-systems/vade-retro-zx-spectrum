;----------------------------------------------------------------------------------------
; Rutinas de Gestion de plataformas.
;----------------------------------------------------------------------------------------

; Gestion de Plataformas m�viles. Tama�o 1 char de alto por 2 char de ancho.
; M�ximo 2 por pantalla.Traslado de datos a la tabla de plataformas en pantalla (tabsprites.asm)
GESTPLATFORM:
	push	hl			; Guardo sin saber si es necesario
	push	de
	ld	ix,TABPLAPANT
SIG_PLATFORM:
	push	ix
	ld	a,[ix+0]		; ix+0=tipla ( 0, 1, 255 )
	cp	$ff			; Si valor 255 has acabado de moverlas.
	jp	z,FINGESTPLATFORM
	and	a			; tipla = 0 vertical, 1 horizontal
	jr	z,platVERT
;----------------------------------------------------------------------------------------
; Plataforma tiene movimiento horizontal.
	dec	[ix+6]			; contador de ticks
	jp	nz,AFINGESTPLATFORM	; No muevas la plataforma
con_movh_p:
	ld	a,[ix+7]		; Maximo contador de ticks
	ld	[ix+6],a		; Transfiero el contador de ticks
	ld	a,[ix+1]		; sentpla horiz.= 0 der, 1 izq.
	and	a
	jr	z,platfhorder
;
; plataforma horizontal en movimiento a izquierdas.
;
	call	verprota_ar
	ld	a,[ix+4]		; lim1 (izquierdas o arriba)
	ld	b,a
	ld	a,[ix+2]		; posX
	cp	b
	jr	z,finmovhiz
; borramos la plataforma volcando el fondo
	call	boplavofo
	dec	[ix+2]			; decrementamos X
; calculo la nueva posicion en el buffer
	call	im_pix_p
	jp	AFINGESTPLATFORM
finmovhiz:
	xor	a
	jr	bfinmovhder
;
; plataforma horizontal en movimiento a derechas.
;
platfhorder:
	call	verprota_ar
	ld	a,[ix+5]		; lim1 (derechas o abajo)
	ld	b,a
	ld	a,[ix+2]		; posX
	cp	b
	jr	z,finmovhder
; borramos la plataforma volcando el fondo
	call	boplavofo
	inc	[ix+2]			; incrementamos X
; calculo la nueva posicion en el buffer
	call	im_pix_p
	jr	AFINGESTPLATFORM
finmovhder:
	ld	a,1
bfinmovhder:
	ld	[ix+1],a
	jr	AFINGESTPLATFORM	
;
; Conjunto de subrutinas usables para borrar la plataforma volcando el fondo
;
boplavofo:				
	call	im_pix_p
	call	vuelcafondosprplat	; para volcar el fondo
	call	c_dirbuff_plat		; calculo la dir.buffer de atributos y lo inyecto.
	ret
; Esto podr�a haber sido un jp c_dirbuff_plat perfectamente
;----------------------------------------------------------------------------------------
; Plataforma tiene movimiento vertical.
platVERT:
	dec	[ix+6]			; contador de ticks
	jr	nz,AFINGESTPLATFORM	; No muevas la plataforma
con_movv_p:
	ld	a,[ix+7]		; Maximo contador de ticks
	ld	[ix+6],a		; Transfiero el contador de ticks
	ld	a,[ix+1]		; sentpla verti.= 0 arr, 1 aba.
	and	a
	jr	z,platfverarr
;
; plataforma vertical en movimiento vertical hacia abajo
;
	call	verprota_ar		; Miramos si el prota est� encima de la plataforma
	ld	a,[ix+5]		; lim2 [derechas o abajo]
	ld	b,a
	ld	a,[ix+3]		; posY
	cp	b
	jr	z,finmovvaba
	ex	af,af'
; borramos la plataforma volcando el fondo
	call	boplavofo
	ex	af,af'
	add	a,4			; incrementamos Y
	ld	[ix+3],a
; calculo la nueva posici�n el buffer
	call	im_pix_p
	jr	AFINGESTPLATFORM
finmovvaba:
	xor	a
	jr	bfinmovvarr
platfverarr:
;
; plataforma vertical en movimiento a arriba
;
	ld	a,[ix+4]		; lim2 [izquierdas o arriba]
	ld	b,a
	ld	a,[ix+3]		; posY
	cp	b
	jr	z,finmovvarr
	ex	af,af'
; borramos la plataforma volcando el fondo
	call	boplavofo
	ex	af,af'
	sub	4			; decrementamos Y
	ld	[ix+3],a
; calculo la nueva posici�n el buffer
	call	im_pix_p
	call	verprota_ar
	jr	AFINGESTPLATFORM
finmovvarr:
	ld	a,1
bfinmovvarr:
	ld	[ix+1],a
AFINGESTPLATFORM:
	ld	hl,$c0e0		; valor de la plataforma
	ld	[spr_p],hl
	call	impatrib_p		; imprimo el attr_plataforma
	call	imp_pla			; Imprimo la plataforma
	pop	ix			; sacamos el valor inicial
	ld	de,8
	add	ix,de			; cargo la siguiente plataforma
	jp	SIG_PLATFORM	
FINGESTPLATFORM:
	pop	ix
	pop	de
	pop	hl
	ret
;----------------------------------------------------------------------------------------
; Comprobaciones de posici�n del prota arriba de la plataforma
;----------------------------------------------------------------------------------------
verprota_ar:
	ld	a,[fila]
	ld	b,a
	ld	a,[ix+3]
	sub	24			; a= ix+3 - 24 fila del prota
	cp	b
	jr	z,verprota_ar_colu
	add	a,4			; a= ix+3 - 20 fila del prota cayendo a 4
	cp	b
	jr	z,verprota_ar_colu
	ret
verprota_ar_colu:
	ld	a,[ix+2]
	ld	b,a
	ld	a,[colu]
	cp	b
	jr	z,tarribaelprota
	dec	a
	cp	b
	jr	z,tarribaelprota
	add	a,2
	cp	b	;;;
	jr	nz,notarriba
tarribaelprota:
	ld	a,1
	ld	[proenpla],a
	ld	[posplat],ix
	ld	a,[keypush]
	and	12		; anulo la pulsaci�n de las teclas arriba y espacio
	ld	[keypush2],a
	ret
notarriba:
	xor	a
	ld	[proenpla],a
	ret
;
; Rutina que se llama al saber que est� el prota encima de la plataforma
;
Enlaplataforma:
	ld	ix,[posplat]
; Ajusto el prota para que saltes mejor encima de la plataforma

	ld	a,[keypush]
	and	12		; anulo la pulsaci�n de las teclas arriba y espacio
	ld	[keypush2],a
	ld	a,[ix+7]	; pongo el ticks de la plataforma
	ld	[skipf_p],a
; vuelcoelfondo del prota de tama�o 3x3 por si usas la espada.
	call	vuelcafondosprprotaEsp	
; Que tipo de plataforma erAs. Vertical u horizontal ?
	ld	a,[ix+0]		; ix+0=tipla ( 0, 1, 255 )
	and	a			; tipla = 0 vertical, 1 horizontal
	jr	z,arriba_platVERT
; comprobamos si la plataforma horiz.va a der o izq
	ld	a,[ix+2]
	ld	[colu],a
bbtarribaelprota:	
	call	im_pix		; Antes saltabas a avolcamiento en SprProtaVolc.asm
	xor	a
	ld	[proenpla],a
	jp	TER_EVEN_CONT		; De esta forma, No compruebo en el motor SEG_EVEN_CONT
arriba_platVERT:
; comprobamos si la plataforma baja o sube para bajar o subir al prota
	ld	a,[ix+3]
	sub	24
	ld	[fila],a
	jr	bbtarribaelprota