;-----------------------------------------------------------------------------------
; RUTINAS DE DETECCION DE CHOQUES
;-----------------------------------------------------------------------------------
parpapro	defs		1	;variable para saber si el prota parpadea despu�s del choque con enemigos. Inmunidad
;
; Entrada fila y colu  Salida calculo los limites del prota dejandolos en DE Y HL 
;

det_choques_enemigos:

;calc_pxy_pro:
        ld	a,[fila]	
	add	a,3
        ld	d,a		; d= fila +3px
        add	a,21
	ld	e,a		; e= fila +24px (ALTO)
        ld	a,[colu]
        ld	h,a		; h= colu
;
; discriminar si el anchosp es 2 o 3 para ajustar el golpeo de la espada si se produce o el choque
;
	ld	a,[anchosp]
	cp	3
	jr	z,adet_ch_enm
	ld	a,h
	inc	a	        ; l= colu +1    (ANCHO)
	jr	badet_ch_enm
adet_ch_enm:
	ld	a,h
	add	a,2
badet_ch_enm:
	ld	l,a	        ; l= colu +2   (ANCHO) para espadazo
;
; Tamiz de comprobacion con enemigos. Tabla ENEMIGOS IX+2 ES X e IX+3 ES Y
;
	ld	a,[ix+2]	
	ld	b,a		; b= col_enem
	ld	c,a             ; c= col_enem para enem.de un solo caracter
;
; de que ancho es el enemigo?
;
	ld	a,[ix+9]
	cp	1
	jr	z,enem_unodeancho
	inc	c		; c= col_enem para enem.de dos caracteres
enem_unodeancho
;
;comprobamos Columnas
;
        ld	a,c		
	cp	h
	ret	c
        ld	a,b
	cp	l
	jr	z,verfila_enem_prota
        ret	nc
verfila_enem_prota:
        ld	a,[ix+3]
	ld	c,a		; lo guardo para calcular su valor seg�n la altura del enemigo
	add	a,2
	ld	b,a		; b= fil_enem +2px
;
; como de alto es el enemigo ?
;
	ld	a,[ix+8]
	cp	16
	jr	z,enem_16_alto
	jr	c,enem_8_alto	; en caso contrario es el fantasma_BL
	ld	a,c		; recupero para sumarle el valor correspondiente
	add	a,22
	jr	bverfila_enem_prota
enem_16_alto:
	ld	a,c		; recupero para sumarle el valor correspondiente
	add	a,14
	jr	bverfila_enem_prota
enem_8_alto:
	ld	a,c		; recupero para sumarle el valor correspondiente
	add	a,6
bverfila_enem_prota:
	ld	c,a             ; c= fil_enem+alto Sp
;
;comprobamos Filas
;
	ld	a,c		
	cp	d
	ret	c
	ld	a,b
	cp	e
	ret	nc
;
; Comprobamos si la espada est� activa. 
;
	ld	a,[accion]
	bit	0,a			; Est� Activo el bit de espadazo ?
	jr	z,enemy_w
;
; matas al enemigo
;
	ld	a,6
	out	[254],a
	ld	a,1
	ld	[repe_fx],a
        ld	a,25
        ld	[long_fx],a
	ld	hl,convalorson	; etiquetas en la rutina de interrupciones
	ld	[vervalorson+1],hl
	ld	a,4
	out	[254],a
        ld	a,55
        ld	[long_fx],a
;
; activo la variable de choque de mandoble con espada al enemigo
;
	xor	a
	ld	[vchoqEnem],a
	inc	a
	ld	[vchoqEnemesp],a
	ret
enemy_w:

 	ld	a,1			; flag para el parpadeo del prota y que no lo puedan matar.
	ld	[inmunidad],a		; El siguiente enemigo que haya en pantalla.
;
; Sonido
;
	ld	[repe_fx],a
	ld	a,10		;13
        ld	[long_fx],a
	ld	hl,convalorson	; etiquetas en la rutina de interrupciones
	ld	[vervalorson+1],hl
;
; Quitar barra energia
;
	call	quita_energia
;
; Que enemigo es ?
;
	ld	a,[ix+18]
	and	a
	jr	nz,q_barr
;
; Animaciones, Nazgul y Delcram
;	
	ld	a,25		; 32 nazgul Aumento el contador de perdida de inmunidad para que no sea tan dr�matico
	ld	[parpapro],a
q_barr:
;
; activo la variable de choque y anulo la de matar al enemigo
;
	xor	a
	ld	[vchoqEnemesp],a
	inc	a
	ld	[vchoqEnem],a
	ret
;-----------------------------------------------------------------------------------
; RUTINAS DE COLOCACI�N DE DATOS DE LA MUERTE DE LOS ENEMIGOS POR CHOQUE
;-----------------------------------------------------------------------------------
ponmuerteEnemigo:
;
; desactivo la variable de choque
;
	xor	a
	ld	[vchoqEnemesp],a
;
; Ni la gota ni la flecha se desactivan con el choque
;
	ld	a,[ix+18]	; codanim (gota =1 flecha =128)
	rla
	ret	c		; si es la flecha no vuelques los valores de muerte
	rra
	rra
	ret	c		; si es la gota no vuelques los valores de muerte
	call	vuelcafondosprenem	; vuelco el fondo del enemigo
;
; Activamos la animacion de la muerte del enemigo y sus nuevos datos
;
	ld	[ix+6],0	; frame inicial
	ld	[ix+25],1	; bit choque activado
	ld	[ix+7],2	; frames 
	ld	[ix+12],$ae	; byte alto igual para 2 incrementar 1 si es ara�a o Blee
;
; Discriminar que enemigo es el que ha muerto
;
; bolaghost			44720	$aeb0
; ave,murcielago,culebra	44768	$aee0
; ara�a, BLee			44864	$af40
	ld	a,[ix+10]
	cp	32
	jr	c,enem_peq_1x1
	jr	z,enem_mediano_2x1
enem_grande_2x2:
	ld	[ix+11],$40	; dblow
	ld	[ix+12],$af	; dbhigh
	ld	[ix+10],64	; tama�o
	ret
enem_mediano_2x1:
	ld	[ix+11],$e0	; 2x1
	ret
enem_peq_1x1:
; es la bola
	ld	[ix+11],$b0	; 1x1
	ret
ponmuerteEnemigoNazgul:
;
; desactivo la variable de choque
;
	xor	a
	ld	[vchoqEnem],a
	call	vuelcafondosprenem	; vuelco el fondo del enemigo
;
; Activamos la animacion de la muerte del enemigo y sus nuevos datos
;
	ld	[ix+6],0	; frame inicial
	ld	[ix+25],1	; bit choque activado
;
; Discriminar que enemigo es el que ha muerto
;
	ld	[ix+8],16	; altosc =16 aunque sea el Bl que tiene 24
	ld	[ix+11],$40	; dblow
	ld	[ix+12],$af	; dbhigh
	ld	[ix+10],64	; tama�o
        ret
;-----------------------------------------------------------------------------------
;FIN DETECCION CHOQUES