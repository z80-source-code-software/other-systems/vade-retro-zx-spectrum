;----------------------------------------------------------------------------------------
; Calculo de la direcci�n del prota en el archivo de pantalla
;----------------------------------------------------------------------------------------
im_pix:
        ld      a,[fila]	;fila en scannes o pixels
        ld      l,a
        ld      a,[colu]	;colu en chars (para usar pixels pasar previam a char)
        call    tabla4metal
        ld      [dir_spr],HL    
; Dejar en caso de usar lasrutinas independientemente de la llamada desde avolcamiento.asm
;        ret

;----------------------------------------------------------------------------------------
;RUTINA DE CALCULO DE LA DIRECCION EN EL BUFFER INTERMEDIO DADAS LA FILA Y LA COLUMNA
;ENTRADA:FILA pixels Y COLUMNA en chars    SALIDA:Posicion del sprite en el buffer intermedio 
;----------------------------------------------------------------------------------------
c_dirbuff:
	xor	a		; acarreo a 0
	ld	e,a		; 1/4
        ld	a,[fila]	; 4/13
	rra			; 1/4
	rr	e		; 2/8
	rra			; 1/4
	rr	e		; 2/8
	rra			; 1/4
	rr	e		; 2/8
	ld	d,a		; 1/4
	ld	a,[colu]	; 4/13 
	add	a,e
	ld	e,a
	ld	hl,$ed90-$400	; 3/10 buffer $ed90 tama�o marcador de arriba 1024= $400
	add	hl,de           ; 3/11
	ld	[d_bu_spr],hl	; 
        ret			; 3/10. Total = 108 t-states.
; La modificacion de la anterior consiste en guardar el valor de para
; usarlo en el buffer de atributos ya que el c�lculo es igual. Trasp. pixels a char.

;	ld	[dir_buf_at],de
;       antes del add hl,de
;	luego cuando quieras calcular la direcc.buff. atributos
;	ld	de,[dir_buf_at]
;	ld	hl,$fd90-32
;	add	hl,de
;	ret

;----------------------------------------------------------------------------------------
; IMPRESION DE SPRITE
;----------------------------------------------------------------------------------------
mov:								; 40786
        ld      de,[dir_spr]	; 6/20 direccion del sprite en la videoram
	ld	hl,[spr]	; 5/16 direccion de sprite a imprimir
evaluamov:
        ld      c,24		; 2/7  altosc
looprota:
        push    de		; 3/11
;;
;	ld	a,[anchosp]	; 4/13	2 normal 3 espadazo
;	ld	b,a		; 1/4
chganchosp:			;       Sirve para cambiar el valor de B desde el motor (linea 423)
	ld	b,2		; 2/7   valor que cambiara durante el juego 2 o 3 si usas espada
; Ahorro 10 estados por cada iteraci�n , total 240 estados
;;
buloop:
        ld      a,[de]		; 2/7  Coge lo de la pantalla
        and     [hl]		; 2/7  Borro lo que no sean 1. Enmascaramiento
        inc     hl		; 1/6
        or      [hl]		; 2/7  Le sumo el sprite
        ld      [de],a		; 2/7  guarda en pantalla
        inc     hl		; 1/6
        inc     e		; 1/4  aumenta posici�n pantalla 1 a la derecha
        djnz    buloop		; 3/13 , 2/8   57estadossino y 52estasosisi
 
	pop     de		; 3/11 posicion de pantalla original
        inc     d               ; 1/4  aqu� viene la rutina de bajar scan de toda la vida
        ld      a,d		; 1/4
        and     7		; 2/7
        jr      nz,sal2		; 3/12 , 7/7
        ld      a,e		; 1/4
        add     a,32		; 2/7
        ld      e,a		; 1/4
        jr      c,sal2		; 3/12 , 7/7
        ld      a,d		; 1/4
        sub     8		; 2/7
        ld      d,a		; 1/4
sal2:
        dec     c		; 1/4
        jr      nz,looprota	; 3/12 , 7/7
        ret			; 3/10
;Fin rutina MOV.HL tiene el valor del siguiente frame a imprimir

;----------------------------------------------------------------------------------------
;Rutina de Impresion de atributos del prota.
;----------------------------------------------------------------------------------------
;impatrib:
;	ld	a,[fila]
;	and	248		; quito scannes intermedios
;	ld	c,a
;	ld	a,[colu]
;	ld	l,a
;	call    c_atr_prota_pant		
;	ld      b,3             ; alto del prota
;sifattrp:
;       push    hl
;        ld      c,2		; ancho del prota
;	ld      a,7             ; COLOR
;siattrpro:
;        ld      [hl],a
;        inc     hl
;        dec     c
;        jr      nz,siattrpro
;        pop     hl
;        ld      de,32
;        add     hl,de
;        djnz    sifattrp
;        ret
;----------------------------------------------------------------------------------------
; Rutina de c�lculo direc.Videoram Atrib..    Entrada C fila Scannes y L columna en char
;----------------------------------------------------------------------------------------
c_atr_prota_pant:	;imp_at:
		xor	a     ;2/8 anulo acarreo
; En IMP_at (carac.) rotaba 3 a der. multiplicando por 32.pero en scann. es 2 veces a izq
		sla	c     ;2/8
                rla           ;1/4
                sla	c     ;2/8
                rla           ;1/4
                ld	b,a   ;2/7
		ld	h,$58 ;2/7 
		add	hl,bc ;3/11 hl = direcci�n en pantalla 
		ret           ;3/10  ; Total 25 ciclos / 67 t-states
;----------------------------------------------------------------------------------------
;Rutina de calc.coord. de atributos B=FILA y L=COLUMNA EN CARACTERES
;----------------------------------------------------------------------------------------
; Se usa para imprimir marcadores, menus, imagenes
coordficu:
        ld      h,#58
        xor     a
        srl     b
        rra
        srl     b
        rra
        srl     b
        rra
        ld      c,a
        add     hl,bc
        ret             ;19ciclos / 72 t-states
