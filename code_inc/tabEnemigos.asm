;----------------------------------------------------------------------------------------
; Tabla de animaciones por pantalla. 
;----------------------------------------------------------------------------------------

ENEMIGOS:		; 54255
	db 255,0, 0,  0,0,0,0,0, 0, 0,  0,  0,  0,  0,  0, 0,  0,  0,  0,0, 0,  0,  0,  0, 0, 0
	db 255,0, 0,  0,0,0,0,0, 0, 0,  0,  0,  0,  0,  0, 0,  0,  0,  0,0, 0,  0,  0,  0, 0, 0
	db 255,0, 0,  0,0,0,0,0, 0, 0,  0,  0,  0,  0,  0, 0,  0,  0,  0,0, 0,  0,  0,  0, 0, 0
	db 255
;REST_ENEMIGOS:		; 54334
	db 255,0, 0,  0,0,0,0,0, 0, 0,  0,  0,  0,  0,  0, 0,  0,  0,  0,0, 0
	DB 255
			; 54356
; los db's son distintos vamos a calcular antes de ejecutar la rutina en pantalla, el valor
; de las posic. en pantalla y buffer de los enemigos. Lo haremos en llamada y lo meteremos
; en cada posici�n. POr eso los db's de la tabla ENEMIGOS ser� mayor que TABENEMPANT
; El calculo se har� solo cuando cambien de fila o colu para no perder tiempo. As� que los
; datos deben guardarse en db's y las variables skipframe y frame deben estar al entrar en
; pantalla en los valores superiores para que vaya a calcular la posici�n inicial y presente
; los enemigos en pantalla

; Gota:		1		
;	1 frames, 
;	vertical.	Un solo sentido
;	tama�o 1x1
; Bolaghost:	2
;	1 frames,
;	Vertical.	Un solo sentido
;	tama�o 1x1
; Ave:		4
;	2 frames,
;	Vertical.	Doble sentido con los mismos frames
;	tama�o 1x2
; Ara�a:	8
;	3 frames,
;	Vertical.	Doble sentido con los mismos frames
;	tama�o 2x2
; Flechas:	128
;	2 frames,
;	horizontal.	Un solo sentido. 
;	tama�o 1x2
; Murcielago:	64
;	3 frames,
;	horizontal.	Doble sentido con distintos frames
;	tama�o 1x2
; Chinchilla:	32
;	3 frames,
;	horizontal.	Doble sentido con distintos frames
;	tama�o 1x2
; Fantasmita:	16	Bruce Lee
;	3 frames,
;	horizontal.	Doble sentido con distintos frames
;	tama�o 2x3


; Explicaci�n de datos (no hay intervalo en los enemigos pero s� en el Bruce Lee):
; 
; activo (si o no), posx en char, posy en pixels, tamba�o1frame (para sumar a dblowAnimMem)
; codanim codigo para actuar sobre los enemigos vert 1,2,4,8 y horiz 16,32,64,128 cada uno tiene una funci�n 
; seg�n direcci�n. direccion ix+15 0 horizontal, 1 vertical
; sentido en la direcci�n: 0 arriba o derecha 1 abajo o izquierda. Al inicio estar� al reves (vert) excepto 
; los enemigos que solo tengan un sentido.
; desplaza, pixels que se mueve en vertical. en horizontal no se usa
;		dblowIniAnim	dbhighIniAnim	tama�oframe	n.fram	altosc	anchoch
; ara�a		$e0,		$bd,		64		3	16	2	
; ave		$a0,		$be,		32		2	8	2
; bola		$e0,		%be,		16		1	8	1
; gota		$f0,		$be,		16		1	8	1

; fantas_BL_der	$a0,		$b9,		96		3	24	2
; fantas_BL_izq $c0,		$ba,

; chinchillader $e0,		$bb,		32		3	8	2
; chinchillaizq $40,		$bc,
; flecha_der	$a0,		$bc,		32		2	8	2
; flecha_izq	$e0,		$bc,
; murcielagoder	$20,		$bd,		32		3	8	2
; murcielagoizq	$80,		$bd,

; Notas importantes:
; La tabla de ENEMIGOS es mayor que la TABENEMPANT porque debemos guardar los datos de la posic.
; en pantalla de los enemigos y en el buffer. 

; Defb's con los siguientes datos
; panta, activo, posx,  posy, ticks, frameskip, frameActual, framestotal, altosc, anchochars,
; ix0,	ix1,	ix2,	ix3,	ix4,	ix5,		ix6,	ix7,	  ix8,  	ix9
; tama�o1frame, dblowEnem, dbhighEnem, dblowEnemIni,dbhighEnemIni, direccion, limite1
;	ix10,	ix11,		ix12,  ix13,		ix14,	   ix15,	ix16
; limite2, codanim, sentido, desplaz	dblowPant, dbhighPant, dblowBuff, dbhighBuff, enemigoMuerto
; ix17,	    ix18,	ix19	ix20	ix21		ix22	ix23		ix24	ix25

; ix+	     0,1, 2,  3,4,5,6,7, 8, 9, 10, 11, 12, 13, 14,15, 16, 17,18,19,20, 21, 22, 23, 24
; enemigos en vertical
;	db  12,1,12, 88,1,5,1,1, 8, 2,-32,$a0,$be,$a0,$be, 1, 88,128,  4, 0, 4,   0, 0,  0, 0 ;ave REPETIR ENTRE 4 Y 8. BAJA 4
; 	db  12,1,22, 88,1,6,2,2,16, 2,-64,$e0,$bd,$e0,$bd, 1, 88,128,  8, 0, 8,   0, 0,  0, 0 ;ara�a REPETIR 4. BAJA 6
;	db  12,1,10, 88,1,4,0,0, 8, 1,-16,$e0,$be,$e0,$be, 1, 88,128,  2, 1, 1,   0, 0,  0, 0 ;bola
;	db  30,0,26, 56,1,3,0,0, 8, 1,-16,$f0,$be,$f0,$be, 1, 56, 88,  1, 1, 4,   0, 0,  0, 0 ;gota !ojo!
; enemigos en horizontal
;	db  30,1, 7, 88,1,4,2,2, 8, 2, 32,$e0,$bb,$e0,$bb, 0, 12,  3, 32, 0, 0,   0, 0,  0, 0 ;chinchillader
;	db  30,1, 7, 72,1,6,2,2, 8, 2, 32,$20,$bd,$20,$bd, 0, 12,  3, 64, 0, 0,   0, 0,  0, 0 ;murcielagoder (velocidad 3 a 6)
;	db  30,0, 3, 88,1,3,1,1, 8, 2, 32,$a0,$bc,$a0,$bc, 0, 12,  3,128, 0, 0,   0, 0,  0, 0 ;flechader
;	db  30,0,12, 88,1,3,1,1, 8, 2, 32,$e0,$bc,$e0,$bc, 0, 12,  3,128, 1, 0,   0, 0,  0, 0 ;flechaizq

;-------------------------------------------------------------------------------------------------------
; notas de posicion de enemigos. 
; 1. La bola siempre debe estar en el primer db de la pantalla. Por este motivo es causa el 2.
; 2. A raiz de esto la flecha JAMAS debe estar el primer db de la pantalla o interactuar�n juntas.
; O no podr�s ponerlas en la misma pantalla. Ya que la gota que aparece actua sobre ix+1 activandolas
; Notas importantes:
; poner ix+4 = 1, ix+6 = al �ltimo frame osea igual que ix+7. Esto es para siempre ya que de lo contrario
; no funcionar� correctamente la rutina de gestion. Hay que tenerlo en cuenta a la hora de salir de la 
; pantalla, la rutina de restauraci�n debe poner esos valores al grabar denuevo los enemigos de la pantalla
; en tabenempant. Habra que ver esto bien porque no se usan de igual forma los db's para enemigos vert u hor
; 
