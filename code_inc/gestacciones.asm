;--------------------------------------------------------------------------------------------------------
; Rutina de Gesti�n de Acciones 		Activada al recoger un objeto que provoca una acci�n.
;


comp_acciones:
	push	ix
	push	hl
	push	de
;
; Desactivo el objeto
;
	ld	ix,[churrAcc]	; Valor dado por la rutina rucoacciones.asm
	ld	a,[tipAccion]
	cp	2
	jp	z,Comp_Puerta_vertical
	cp	3
	jp	z,Actu_S_Enem
;--------------------------------------------------------------------------------------------------------
; tipAccion=1 Puerta Horizontal. Vamos a ver si has recogido los 4 sellos para ir al otro mapeado
	ld	a,[sello]
	and	a
	jp	nz,Desact_sinanular
;--------------------------------------------------------------------------------------------------------
; Si los has recogido. Si estas en la pantalla 28 hacer la animaci�n
	ld	a,[panta]
	cp	28
	jr	nz,VaciarPalanca28
;
; Borrar objeto en pantalla	
;
	ld	a,[ix+3]
; pasar a char para borrar aqu� directamente en pantalla.
	srl	a
	srl	a
	srl	a
	ld	[fil_dat],a		; fila_dat en char
	ld	b,a
	ld	a,[ix+2]
	add	a,2			; segunda parte del tile trampilla horiz. a borrar
	ld	[col_dat],a		; columna
	ld	c,a
	push	bc			;guardo los valores para la siguiente parte a borrar
	ld	a,72			; n.tile vacio de 1x2
	ld	[num_tile],a
	call	search_dat		; En bupaim.asm. Salida tile a imprimir en 'de'
	call	im_char
	pop	bc			; saco los valores de la siguiente parte de la trampilla a borrar
	dec	c
	dec	c			; col_dat en su posici�n a borrar
	ld	a,c
	ld	[col_dat],a
	push	bc			; guardo el valor para imprimir despues la trampilla nueva
	ld	de,[poti_im]
	call	im_char
; Pintar en pantalla la nueva trampilla
	ld	a,74			; n.tile con trampilla horizontal en Vertical
	ld	[num_tile],a
	call	search_dat		; alto, ancho y poti_im
; Entrada: de   contiene el tile a imprimir
	pop	bc			; saco los valores para usar en el pintado de la nueva trampilla
	call	im_char			; b y c
VaciarPalanca28:
;
; Borrar objeto en buffer	
;
	ld	a,[ix+2]		; col_dat
	ld	[col_dat],a
	push	af
	ld	a,[ix+3]		; fil_dat
	ld	[fil_dat],a		
	ld	a,72			; n.tile vacio de 1x2
	ld	[num_tile],a
	call	search_dat		; En bupaim.asm. Salida tile a imprimir en 'de'
	call	c_dirbufftile		; colocaci�n en buffer de los objetos
	pop	af			; saco col_dat y sumo 2
	add	a,2
	ld	[col_dat],a
	call	search_dat		; la trampilla es de 1x4
	call	c_dirbufftile		; hay que borrarla entera
; poner en el churro de la tabla de acciones el valor de la trampilla nueva
	ld	a,74			; n.tile con trampilla horizontal en Vertical
	ld	[ix+1],a
; Poner en buffer el nuevo tile 
	ld	[num_tile],a
	ld	a,[ix+2]		; col_dat
	ld	[col_dat],a
	ld	a,[ix+3]		; fil_dat
	ld	[fil_dat],a		
	call	search_dat		; En bupaim.asm. Salida tile a imprimir en 'de'
	call	c_dirbufftile		; colocaci�n en buffer de los objetos
;
; Desactivar la acci�n		
;
Desact_accion:
	ld	[ix+0],0	
Desact_sinanular:
	ld	hl,nomiraracciones
	ld	[vermiraracciones+1],hl		; Desactivo mirar objetos
	pop	de
	pop	hl
	pop	ix
	ret
;--------------------------------------------------------------------------------------------------------
; Accion de palanca normal. Activamos la palanca correspondiente.
Comp_Puerta_vertical:
; necesito calcular la posic. en el archivo de pantalla. Y solo necesito saber lo. La rutina
; im_char no tiene un ret para ahorrar bytes, ya que se usa habitualmente el conjunto y no quiero duplicarla
	ld	a,201			; valor comando ret lo pongo para retornar de la llamada 
	ld	[imprbloques],a		; a im_char que voy a hacer y no quiero duplicar la rutina.
	ld	a,[ix+2]
	ld	c,a			; columna del dato
	ld	a,[ix+3]
	srl	a
	srl	a
	srl	a
	ld	b,a			; fila del dato
	call	im_char
	ld	a,58			; valor comando ld a,[nn] lo pongo para dejar la rutina 
	ld	[imprbloques],a		; im_char como estaba antes de la llamada.
; a la vuelta de im_char 'hl' tiene el valor de la posic. donde est� la puerta a levantar.
	ld	e,l
	ld	d,h			; de=hl
	inc	h			; hl=de+1 o segundo scann
	ld	b,11			; n.veces a repetir el efecto
loopPal3:
	push	bc
	push	hl
	push	de
	ld	c,16			; altura de la puerta a 'elevar'
loopPal2:
	ld	b,1
loopPal:
	ld	a,[hl]
	ld	[de],a
; sonido de levantar
	push	bc
	ld	a,3
	ld	[repe_fx],a
        ld	a,12		; 2 y 12
        ld	[long_fx],a
	call	fx48k
	ld	a,4
	ld	[repe_fx],a
	ld	a,10		; 3 y 10
        ld	[long_fx],a
        call	fx48k
	pop	bc
; fin sonido
	inc	d
; voy a comprobar si has terminado con el primer char
	ld	a,[vardepuve]
	cp	1
	call	z,bvardepuve
	inc	h
	djnz	loopPal
	ld	a,h
	and	7
	jr	nz,quitarAc
	ld	a,1
	ld	[vardepuve],a
	ld	a,l
	add	a,32
	ld	l,a
	jr	c,quitarAc
	ld	a,h
	sub	8
	ld	h,a
quitarAc:
	dec	c
	jr	nz,loopPal2
	pop	de
	pop	hl
	pop	bc
	djnz	loopPal3
DesactivarPalanca:
; poner en el churro de la tabla de acciones el valor de la trampilla nueva
	ld	a,76			; n.tile con trampilla Vertical desactivada
	ld	[ix+1],a
; Poner en buffer el nuevo tile 
	ld	[num_tile],a
	ld	a,[ix+2]		; col_dat
	ld	[col_dat],a
	ld	a,[ix+3]		; fil_dat
	ld	[fil_dat],a		
	call	search_dat		; En bupaim.asm. Salida tile a imprimir en 'de'
	call	c_dirbufftile		; colocaci�n en buffer de los objetos
	jp	Desact_accion
vardepuve	defs	1
bvardepuve:
	ld	e,l
	ld	d,h	; copio el valor de hl en de
	ret
;--------------------------------------------------------------------------------------------------------
Actu_S_Enem:
;
; Ahora desactivamos las animaciones, excepto la gota. Osea solo fuegos y rayos
;
	push	ix
	ld	ix,ANIMAC
BActu_S_Enem:
	ld	a,[ix+0]
	cp	255
	jr	z,fin_des_animaC
	ld	a,[ix+18]
	and	a
	jr	z,A_fin_des_animaC	; llamaradas o rayos
BBActu_S_Enem:
	ld	de,20
	add	ix,de			; no actua sobre la gota
	jr	BActu_S_Enem
A_fin_des_animaC:
	ld	[ix+1],0		; desactivar los fuegos y rayos
	call	im_pix_a	;Antes	call	avolcamiento_anim en SprAnimVolc.asm
	call	vuelcafondospranim
	jr	BBActu_S_Enem
fin_des_animaC:	
	pop	ix
	jp	Desact_accion		; Probar este y probar a desactivar la acci�n tambi�n