;----------------------------------------------------------------------------------------
; Calculo de la direcci�n del prota en el archivo de pantalla
;----------------------------------------------------------------------------------------
; ix+2 colu en chars, ix+3 fila en pixels SALIDAS: HL y dir_spr_e
im_pix_e:							; 45869
        ld      l,[ix+3]	;fila en scannes o pixels
        ld      a,[ix+2]	;colu en chars (para usar pixels pasar previam a char)
        call    tabla4metal
	ld	[ix+22],h
	ld	[ix+21],l
	ret
;----------------------------------------------------------------------------------------
;RUTINA DE CALCULO DE LA DIRECCION EN EL BUFFER INTERMEDIO DADAS LA FILA Y LA COLUMNA
;ENTRADA:FILA pixels Y COLU en chars   SALIDA:Pos. del sprite en el buffer interm. en HL 
;----------------------------------------------------------------------------------------
c_dirbuff_e:							; 45883
	xor	a		; acarreo a 0
	ld	e,a		; 1/4
        ld	a,[ix+3]	; 4/13
	rra			; 1/4
	rr	e		; 2/8
	rra			; 1/4
	rr	e		; 2/8
	rra			; 1/4
	rr	e		; 2/8
	ld	d,a		; 1/4
	ld	a,[ix+2]	; 4/13 
	add	a,e
	ld	e,a
	ld	hl,$ed90-$400	; 3/10 buffer $ed90 tama�o marcador de arriba 1024= $400
	add	hl,de           ; 3/11
	ld	[ix+24],h
	ld	[ix+23],l
	ret			; 3/10. Total = 108 t-states.
;----------------------------------------------------------------------------------------
; IMPRESION DE SPRITE.
;----------------------------------------------------------------------------------------
imp_ene:							; 45911
;        ld      de,[dir_spr_e]	; direccion del sprite en la videoram
	ld	d,[ix+22]
	ld	e,[ix+21]
;	ld	hl,[spr_e]	; direccion de sprite a imprimir
	ld	h,[ix+12]
	ld	l,[ix+11]
evaluamov_e:
;	ld	a,[altosc_e]	; dato que vendr� de los db's de la tabla
	ld	c,[ix+8]
;	ld      c,a             ; paso el valor a c
loopenem:
        push    de
;	ld	b,[ix+9]	; ancho
chganchospen:			; Etiqueta para indicar la direccion de hl
	ld	b,2		; usaremos el registro para cambiar el valor 1,2
buloopenem:			; por ejemplo desde TabyGestNazgul o GestionenemigosFinal.asm
        ld      a,[de]          ; Coge lo de la pantalla
        and     [hl]            ; Borro lo que no sean 1. Enmascaramiento
        inc     hl
        or      [hl]            ; Le sumo el sprite
        ld      [de],a		; guarda en pantalla
        inc     hl
        inc     e		; aumenta posici�n pantalla 1 a la derecha
        djnz    buloopenem
        pop     de		; posicion de pantalla original
        inc     d		; aqu� viene la rutina de bajar scan de toda la vida
        ld      a,d
        and     7
        jr      nz,sal2ene
        ld      a,e
        add     a,32
        ld      e,a
        jr      c,sal2ene
        ld      a,d
        sub     8
        ld      d,a
sal2ene:
        dec     c
        jr      nz,loopenem
        ret
;----------------------------------------------------------------------------------------
;Rutina de Impresion de atributos del ENEMIGO.
;----------------------------------------------------------------------------------------

; No la usamos       LOS ENEMIGOS SE PINTAN COMO EL PROTA, CON EL COLOR QUE HAY DE FONDO 

;impatrib_e:							; 45955
; Impresion de atributos en la Videoram Atrib.
;	call	c_dirAtr_enem
;	ld      b,1                ; alto del enemigo
;sifattrp_e:
;	push	hl
;	ld      c,2	           ; ancho del enemigo
;siattrpro_e:
;       ld      [hl],70		   ; COLOR amarillo con brillo
;       inc     hl
;       dec     c
;       jr      nz,siattrpro_e
;	pop	hl
;       ld      de,32
;       add     hl,de
;       djnz    sifattrp_e
;       ret

;----------------------------------------------------------------------------------------
; C�lculo direc.Videoram Atrib.. fila pixels columna en char
;----------------------------------------------------------------------------------------
; No la usamos

;c_dirAtr_enem:							; 45989
;	ld	a,[ix+3]	; 5/19 fila
;	and	248		; 2/7 anulando valores intermedios para el calculo
;	ld	c,a		; 1/4
;	xor	a		; 1/4
;	sla	c		; 2/8 desplazo la fila 2 veces a
;	rla			; 1/4 izquierda rotando b
;	sla	c		; 2/8 para que el resultado de dividir /8
;	rla			; 1/4 y luego multiplicar x32 sea lo mismo.
;	ld	b,a		; 1/4 fila en bc
;	ld	a,[ix+2]	; 5/19 colu
;	ld	l,a		; 1/4
;	ld	h,$58		; 2/7 
;	add	hl,bc		; 3/11 hl = direcci�n Atributos 103t-states
;	ret
;----------------------------------------------------------------------------------------
; Calculo del attr.del fondo plataforma en el Buffer Attr.    fila pixels columna en char
;----------------------------------------------------------------------------------------
; No la usamos

;c_dirbuff_enem:							; 46011
; C�lculo direc.en el buffer Atrib.. 
;	ld	hl,$fd90	; buffer de atributos.
;	ld	a,[ix+3]	; 5/19 fila
;	and	248		; 2/7 anulando valores intermedios para el calculo
;	sub	32		; tama�o del marcador superior
;	jr	z,sumacoluatt_enem
;	ld	c,a		; 1/4
;	xor	a		; 1/4
;	sla	c		; 2/8 desplazo la fila 2 veces a
;	rla			; 1/4 izquierda rotando b
;	sla	c		; 2/8 para que el resultado de dividir /8
;	rla			; 1/4 y luego multiplicar x32 sea lo mismo.
;	ld	b,a		; 1/4 fila en bc
;	add	hl,bc
;sumacoluatt_enem:
;	ld	a,[ix+2]	; 5/19 colu
;	ld	c,a		; 1/4
;	ld	b,0		; 2/7 
;	add	hl,bc		; 3/11 hl = direcc.en Buffer Atrib. 103t-states
;	ex	de,hl		; lo guardo en de
; imprimo el atributo en la pantalla
;	call	c_dirAtr_enem	; hl = direc.Atributos en pantalla.
;	ld	a,[de]		; de = direc.Atrib. en el buffer.Atrib.
;	ld	[hl],a
;	inc	l
;	inc	e
;	ld	a,[de]
;	ld	[hl],a
;	ret


