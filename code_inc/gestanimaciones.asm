;----------------------------------------------------------------------------------------
; Rutinas de Gestion de animaciones.
;----------------------------------------------------------------------------------------
; valor [skipf_e] = 1 cuando hayan animaciones. Si no empezara en 0.
GESTANIMACIONES:
	push	ix
	push	hl			; Guardo sin saber si es necesario
	push	de
	ld	ix,ANIMAC
; Si hay animaciones siempre ser�n 2
BGESTANIMACIONES:
;
; animaci�n activa S/N
;
	ld	a,[ix+1]
	and	a
	jp	z,verotraanimacion
;
; intervalo activo S/N
;
	ld	a,[ix+13]
	and	a
	jp	z,incrementarintervalo
;
; contador de ticks
;
	ld	a,[ix+4]	
	dec	a
	ld	[ix+4],a
	jp	nz,imprimemismAnim
;
; pongo el skipframe del db's en ticks
;
	ld	a,[ix+5]		
	ld	[ix+4],a
;
; Comprobamos si es el �ltimo frame
;
	ld	a,[ix+7]	; framesTotal
	ld	b,a
	ld	a,[ix+6]	; frameActual
	cp	b
	jr	z,ponframeAnimac_0
;
; Incremento el valor del frame
;
	inc	a
	ld	[ix+6],a
	call	vuelcafondospranim	; vuelco el fondo de la animaci�n
;
; Ahora con el tama�o del frame calculamos el frame a imprimir. Sumandolo a dblowAnim
;
	ld	c,[ix+10]		; tama�o del frame
	ld	b,0
	ld	l,[ix+11]		; dblowAnim
	ld	h,[ix+12]		; dbhighAnim
	add	hl,bc
	ld	[ix+11],l
	ld	[ix+12],h		; guardo el valor para el pr�ximo frame
	jr	BimprimemismAnim
;
; Pongo el frame a 0. Antes discrimino si es la gota la que hay que animar
; (gota terminada de aparecer= GOTA o gota terminada de desaparecer = gota que aparece)
; Estas animaciones est�n supeditadas una a que termina la otra y la otra a que termine
; la gota de caer en su trayectoria. Por eso es importante discriminar aqu� quien es quien
;
ponframeAnimac_0:	
	call	im_pix_a	;Antes	call	avolcamiento_anim en SprAnimVolc.asm
	call	vuelcafondospranim	; vuelco el fondo de la animaci�n
;
; frame e intervalo a 0
;
	xor	a		
	ld	[ix+6],a	
	ld	[ix+13],a	
;
; Comprobamos codanim (1 gota que aparece, 2 gota que desaparece)
;
	ld	a,[ix+18]
	rra			; Gota que termina de salir
	jr	c,gotabeg
	rra			; Gota que desaparece
	jr	c,gotaend
;
; Si no es la gota comenzando o terminando continua
;
	ld	a,[ix+16]
Vueltagotabeg_end:
	ld	[ix+11],a
	ld	a,[ix+17]
	ld	[ix+12],a
	jp	verotraanimacion
;
; Finaliza la animaci�n de la gota que aparece. Colocar en los db's la de la gota que salpica
;
gotabeg:
	ld	[ix+1],0	; desactivo el db's (se activar� desde gestenemigos (gota) )
	ld	[ix+13],1	; dejo activado el intervalo en el churro para no esperar el salpicado.	
;
; nuevo valor de codanim, convierto gota que comienza en gota que termina para despues
;
	ld	[ix+18],2
;
; calculamos la nueva posicion de Y con la distancia al suelo.
;
	ld	a,[ix+3]	; posY
	ld	b,[ix+19]	; distancia al suelo
	add	a,b
	ld	[ix+3],a	; nueva posicion, para la animaci�n en el suelo
;
; lowbyte de la gotasuelo
;
	ld	a,$90
	ld	[ix+16],a	; es necesario que tambi�n est� en el registro A
;
; activo la gota en la tabla de enemigos
;
	push	ix
	ld	ix,ENEMIGOS
	ld	[ix+1],1
	pop	ix
	jr	Vueltagotabeg_end
;
; Desaparece la animaci�n en el suelo. Colocar en los db's la de la gota que comienza
;
gotaend:
;
; nuevo valor de codanim
;
	ld	[ix+18],1
;
; calculamos la nueva posicion de Y con la distancia al suelo.
;
	ld	a,[ix+19]	; distancia al suelo
	ld	b,a
	ld	a,[ix+3]	; posY
	sub	b
	ld	[ix+3],a	; nueva posicion, para la animaci�n en el suelo
;
; highbyte de la gota empieza a brotar y lowbyte 
;
	ld	[ix+17],$c0	; highbyte gotaempiezaabrotar
	ld	a,$60		; es necesario que tambi�n est� en el registro A
	ld	[ix+16],a	; lowbyte gotaempiezaabrotar
	jr	Vueltagotabeg_end
imprimemismAnim:
	ld	l,[ix+11]
	ld	h,[ix+12]
BimprimemismAnim:
;
; guardo hl en la variable para imprimir
;
	ld	[spr_a],hl		
	call	im_pix_a	;Antes	call	avolcamiento_anim en SprAnimVolc.asm
	call	imp_ani			; imprimo el enemigo
	jr	verotraanimacion
;
; incrementar el ticks_intervalo
;
incrementarintervalo:
	ld	a,[ix+14]
	add	a,1
	jr	c,pona1intervalo
	ld	[ix+14],a
	jr	verotraanimacion
;
; Activo el intervalo para empezar con la animaci�n.
;
pona1intervalo:
	inc	a			; a de entrada aqu� es 0 
	ld	[ix+13],a
	ld	a,[ix+15]		; cojo frameskip_intervalo y lo copio en ticks_intervalo
	ld	[ix+14],a		; restauro el valor para la pr�xima vez.
;
; inmunidad Activada ?
;
	ld	a,[inmunidad]
	and	a
	jr	nz,verotraanimacion
;
; Detectar choques de los enemigos con el prota
;
	call	det_choques_enemigos
; Si has tocado a la animacion con la espada, no compruebes si te tocan a t� para no crear confusion con las variables de choque de enemigos
;
	ld	a,[vchoqEnemesp]
	and	a
	jr	nz,pormandoble		; si valor 1 entonces no compruebes si te toca el enemigo (en este caso la animacion)
;
; comprobacion de si ha chocado con enemigo. 
;
	ld	a,[vchoqEnem]
	and	a
	jr	z,averotraanimacion
;
; Activo la inmunidad
;
	ld	a,1			; flag para el parpadeo del prota y que no lo puedan matar.
	ld	[inmunidad],a		; El siguiente enemigo que haya en pantalla.
;
; Quito el flag de vchoqEnem que LO USO PARA BORRAR AL ENEMIGO QUE HAS TOCADO.Pero no debe ser activado
; cuando lo que te toca es una animaci�n.
;
averotraanimacion:
	xor	a
baverotraanimacion:
	ld	[vchoqEnem],a
;
; Actuar sobre la siguiente animacion
;
verotraanimacion:
	ld	de,20
	add	ix,de
;
; Comprobamos si ya has mirado ambas animaciones
;
	ld	a,[ix+0]
	cp	255
	jp	nz,BGESTANIMACIONES
FINGESTANIMACIONES:
	pop	de
	pop	hl
	pop	ix
	ret
pormandoble:
	xor	a
	ld	[vchoqEnemesp],a	; pongo el flag de matar el enemigo por espada a 0 para no matar los enemigos al pegarle a una animacion
	jr	baverotraanimacion
