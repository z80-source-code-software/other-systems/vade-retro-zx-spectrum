;----------------------------------------------------------------------------------------
; Rutina de volcado de datos de las plataformas en pantalla
;----------------------------------------------------------------------------------------
vuelcaplat_pant:
        ld	hl,TABPLATFORM -09
        ld	a,[panta]
        ld	b,a
	ld	de,9
bvu_plf_pant:
        add	hl,de    ;siguiente db's
	ld	a,[hl]
        cp	#ff   ;255 para salir
	ret	z
	cp	b
        jr	nz,bvu_plf_pant
bbvu_plf_pant:
	ld	[tplfpan],hl		; Direcci�n para el retorno
	ld	hl,hayplataformas
        ld	[verplataformas+1],hl
	ld	hl,[tplfpan]

; hl ya tiene el valor de inicio de los enemig. de esa pantalla en TABSPRAN

	ld	de,TABPLAPANT
bvuelca_a_tabplapant:
	ld	a,[panta]
        ld	b,a
        ld	a,[hl]
        cp	b
	ret	nz
	inc	hl	; salto el valor de panta
	ld	bc,8
        ldir
        jr	bvuelca_a_tabplapant
;----------------------------------------------------------------------------------------
; Rutina de volcado de las plataformas en pantalla a los datos correspondientes.
;----------------------------------------------------------------------------------------
vuel_plat_act_a_pant:					; 41222
	ld	hl,nohayplataformas
        ld	[verplataformas+1],hl

	ld	hl,TABPLAPANT
	ld	de,[tplfpan]
bvuelcaplatpant_apant:
	ld	a,[hl]
	cp	255
	RET	Z
	push	hl
	inc	de
	ld	bc,4	; traspaso de tipla,sentpla, posx,posy
	ldir
	pop	hl
	inc	de
	inc	de
	inc	de
	inc	de	; salto los limites que son los datoa que no se han volcado.
	ld	bc,8	; siguiente plataforma, si la ah�.
	add	hl,bc
	jr	bvuelcaplatpant_apant
;----------------------------------------------------------------------------------------
; Rutina para vaciar la tabla TABPLAPANT
cleartabplapant:
	ld	hl,RESTTABPLAPANT
	ld	de,TABPLAPANT
	ld	bc,16
	ldir
        ret