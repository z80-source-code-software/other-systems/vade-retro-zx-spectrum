;-----------------------------------------------------------------------------------
; Entrada fila y colu  Salida calculo los limites del prota dejandolos en DE Y HL 
;
comp_objetos:
;calc_pxy_pro:
        ld	a,[fila]	
	add	a,3
        ld	d,a		; d= fila +3px
        add	a,21
	ld	e,a		; e= fila +24px (ALTO)
        ld	a,[colu]
        ld	h,a		; h= colu
	inc	a
        ld	l,a	        ; l= colu +1    (ANCHO)
	ld	a,[col_dat]	
	ld	b,a		; b= col_dat
	inc	a
	ld	c,a             ; c= col_dat+1
;
;comprobamos Columnas
;
        ld	a,c		
	cp	h
	ret	c
        ld	a,b
	cp	l
	jr	z,verfilaobje
        ret	nc
verfilaobje:
        ld	a,[fil_dat]	
	ld	b,a		; b= fil_dat
	add	a,24
        ld	c,a             ; c= fil_dat+alto Sp
;
;comprobamos Filas
;
	ld	a,c		
	cp	d
	ret	c
	ld	a,b
	cp	e
	ret	nc
;
; Sonido
;
	ld	a,1
	ld	[repe_fx],a
        ld	a,99
        ld	[long_fx],a
	ld	hl,convalorson	; etiquetas en la rutina de interrupciones
	ld	[vervalorson+1],hl
	ld	hl,nomirarobjetos
	ld	[vermirarobjetos+1],hl		; Desactivar recoger. Por haberlo hecho.
;
; antes comprobar si hay que hacer alguna acci�n o se va a imprimir en el marcador
;
	ld	a,[tipObjeto]
	and	a
	jr	z,AumentarEnergia
	cp	1
	jr	z,RecogerSello
	cp	2
	jr	z,ActivaPocima
	cp	3			; 3 = ESPADA
	jr	nz,Act_Puertas
ActivaEspada:
	ld	hl,espada
	dec	[hl]
	call	z,activAccal_poc_esp
	jr	ColocaObjeto
ActivaPocima:
	ld	a,1
	ld	[PocimaAct],a
; Si recoges la p�cima despu�s de las calveras tengo que tener activo la acci�n. Por eso esta llamada.
	ld	a,[calavera]
	and	a
	call	z,activAccal_poc_esp		
	jr	ColocaObjeto
; Son 4 los sellos que te dan opci�n a pasar a abajo
RecogerSello:
	call	ColocaObjeto
	ld	hl,sello
	dec	[hl]
	jr	z,Salida_ru_66
	ret
;
; Desact_enem_foc:	; 
; Act_Puertas:		; Ambas utilizan el mismo c�digo
;
Act_Puertas:
	call	borrObjBuf
Salida_ru_66:
	ld	hl,miraracciones	; Activo el salto a compacciones.asm
	ld	[vermiraracciones+1],hl
	ret
AumentarEnergia:
	call	Aumentador_Energia	; En barraenergia.asm
	ld	hl,calavera
	dec	[hl]
	jr	nz, ColocaObjeto
	ld	a,[PocimaAct]
	cp	1			; si hay 1 es que llevas la p�cima
	call	z,activAccal_poc_esp
ColocaObjeto:
	call	borrObjBuf
	jp	pinObjMar
;	call	pinObjMar
;	ret

; Una vez recogidas las partes de la espada o todas las calaveras se activa la accion.
; El motivo es porque no sabes en que momento recoger�s la �ltima parte de la espada o
; la �ltima de las calaveras. Entonces no puedo encorsetarme a rucoacciones.asm
activAccal_poc_esp:
	ld	hl,miraracciones2
	ld	[vermiraracciones2+1],hl
	ld	a,4
	ld	[finanimac],a		; Valor para la animaci�n de la flecha en el marcador
	ld	a,7
	ld	[skipf_f],a
	ld	a,1
	ld	[hechizo],a		; Ahora puedes matar a Delcram
	ret

;-----------------------------------------------------------------------------------
borrObjBuf:
;
; Desactivo el objeto, lo borro de la pantalla y lo coloco en el marcador
;
	ld	ix,[churrObj]	; 
	ld	[ix+0],0	; Desactivo el objeto. 
;
; Borrar objeto en buffer	
;
	ld	a,[ix+7]		; n.tile vacio
	ld	[num_tile],a
	call	search_dat		; En bupaim.asm. Salida tile a imprimir en 'de'
	call	c_dirbufftile		; colocaci�n en buffer de los objetos
borrObjPantalla:
;
; Borrar objeto en pantalla	
;
	ld	a,[ix+3]
; pasar a char para borrar aqu� directamente en pantalla.
;	srl	a
;	srl	a
;	srl	a
	rra
	rra
	rra		; Igual que lo anterior pero trasladamos el acarreo
	and	31	; que aqu� anulamos. Con esto ahorramos bytes y 5 estados al menos
	ld	[fil_dat],a		; fila_dat en char
	ld	b,a
	ld	a,[ix+2]
	ld	[col_dat],a		; columna
	ld	c,a
	ld	de,[poti_im]
	jp	im_char			; devolvemos el control con s� RET
;	call	im_char
;	ret

;
; Colocaci�n en el marcador de los tiles
;
pinObjMar:
	ld	a,[ix+5]
	ld	[col_dat],a		; columna en el marcador
	ld	c,a
	ld	a,[ix+6]
	ld	[fil_dat],a		; fila en el marcador en char
	ld	b,a
	ld	a,[ix+1]
	ld	[num_tile],a
	call	search_dat		; alto, ancho y poti_im
	jp	im_char			; devolvemos el control con s� RET
; Entrada: de   contiene el tile a imprimir
;	call	im_char			; Entrada b = fila y c = colu (menuymarcador.asm)
;	ret