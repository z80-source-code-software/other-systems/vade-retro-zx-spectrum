;--------------------------------------------------------------------------
; Rutina de Controles
CONTROLES:
	ld	a,247
	in	a,[#fe]
        rra
        jr	nc,teclas
        rra
        jr	nc,mando_de_juegos
	jr      CONTROLES
teclas:
	ld	hl,KEYBOARD
	ld	[control],hl
	jr	SON_MENU
mando_de_juegos:
;JOYSTICK:
        ld	hl,JOYSTICK
	ld	[control],hl
; sonido de pulsacion de tecla
SON_MENU:
	ld	a,1		;1
	ld	[repe_fx],a
        ld	a,169		;169
        ld	[long_fx],a
        call	fx48k
;        call	Espera_tecla
;        ret
	jp	Espera_tecla
;-------------------------------------------------------------------------------
; FX Sonidos	Gestionenemigosfinal2.asm rutinainterr.asm program.asm compobjetos2.asm
; dtechoquesF3C.asm detchoquesdelcram.asm gestacciones.asm gestaccionesB.asm
fx48k:
	push	bc
        ld	a,[repe_fx]
        ld	b,a
armu:
	push    bc
        ld	a,[long_fx]
	ld	c,a
	xor	a
sonmu:
	out	[254],a
        ld	b,c
espera:
	djnz	espera
        xor	16
	dec	c
        jr	nz,sonmu
        pop	bc
        djnz	armu
        pop	bc
        ret
;-------------------------------------------------------------------------------
; Gancho rutina de interrupciones para la musica
ponmusicaint:
        ld hl,intmusic
        ld [intaddr+1],hl
        ret
