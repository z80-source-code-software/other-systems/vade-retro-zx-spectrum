;----------------------------------------------------------------------------------------
; Movimientos direccionales
;----------------------------------------------------------------------------------------
obst_arr:
		ld	a,[fila]
        	cp	34              ; MAX.POSIC.PANTALLA A arriba
        	jr	nc,YSIAR
		xor	a
		ld	[senpro],a	; 0
		inc	a
        	ld	[flagpa],a      ; Banderin activo de paso de pantallas
        	ld	a,128
		ld	[fila],a
        	ld	[last_p+1],a    ; Inicializan VALORES CUANDO MUERES
		ld	a,[colu]
        	ld	[last_p],a
		jp	hayobs
YSIAR:
		ld	a,[fila]        ; primer CHR debajo del prota
        	dec	a
		and	248
		ld	c,a	        ; Guardo la fila a comprobar en c
		ld	e,c	        ; Preservo el valor para los 3 calculos.
		ld	a,[colu]
		ld	l,a	        ; Guardo La columna en l
		call	c_atr_prota_pant
; primer CHR arriba del prota 
		ld	a,[hl]
		bit     6,a             ; comprob.si el flag de brillo atrib.est� activo
		jp	nz,hayobs
; segundo CHR arriba del prota 
		inc	hl
		ld	a,[hl]
		bit     6,a             ; comprob.si el flag de brillo atrib.est� activo
		jp	nz,hayobs
		jp	Nohayobs
;---------------------------------------------------------------------------
obst_aba:
		ld	a,[fila]
        	cp	134             ; MAX.POSIC.PANTALLA A abajo
        	jr	c,YSIAB
		ld	a,1
        	ld	[flagpa],a      ; Banderin activo de paso de pantallas
		inc	a
		ld	[senpro],a	; 2
		ld	a,32
        	ld	[fila],a
        	ld	[last_p+1],a    ; Inicializan VALORES CUANDO MUERES
		ld	a,[colu]
        	ld	[last_p],a
		jp	hayobs
YSIAB:                                  ; Comprobar el suelo bajo tus pies
		ld	a,[fila]        ; primer CHR debajo del prota
        	add	a,24		; ANTES 25
		and	248
		ld	c,a	        ; Guardo la fila a comprobar en c
		ld	e,c	        ; Preservo el valor para los 3 calculos.
		ld	a,[colu]
		ld	l,a	        ; Guardo La columna en l
		call	c_atr_prota_pant
; primer CHR debajo del prota 
		ld	a,[hl]
		bit     6,a             ; comprob.si el flag de brillo atrib.est� activo
		jp	nz,hayobs	; est� entre 0 y 79.
; primer CHR debajo del prota 8pixels a la derecha
		inc	hl
		ld	a,[hl]
		bit     6,a             ; comprob.si el flag de brillo atrib.est� activo
		jp	nz,hayobs	; est� entre 0 y 79.
		jp	Nohayobs
;------------------------------------------------------------------------------------
obst_de:                                ; A la derecha   40873
		ld	a,[colu]
		cp	29              ; MAX.POSIC.PANTALLA A derecha
        	jr	NZ,YSIDE
		ld	a,1
		ld	[colu],a
        	ld	[last_p],a
        	ld	[flagpa],a      ; Banderin activo de paso de pantallas
		ld	[senpro],a	;1
		ld	a,[fila]
        	ld	[last_p+1],a    ; Inicializan VALORES CUANDO MUERES
		jp	hayobs
YSIDE:					; 40903
		ld	a,[fila]        ; primer CHR a derecha del prota
		add	a,4		; antes 2
		and	248	        ; Le quito los 3 bits inferiores para quedarmecon
		ld	c,a             ; la posic. a convertir a char exacta
		ld	a,[colu]
		add	a,2
		ld	l,a
		call	c_atr_prota_pant
; Tengo que guardar el resultado para que estss rutinas vayan m�s r�pido
; Calculada la primer posici�n en altura las restantes solo hay que sumar $20 a fila
		ld	a,[hl]
		bit     6,a             ; comprob.si el flag de brillo atrib.est� activo
		jp	nz,hayobs
; CHR a derecha del prota 8 pixels abajo
		ld	bc,$20
		add	hl,bc
		ld	a,[hl]
		bit     6,a             ; comprob.si el flag de brillo atrib.est� activo
		jp	nz,hayobs
; CHR a derecha del prota 15 pixels abajo
		add	hl,bc
		ld	a,[hl]
		bit     6,a             ; comprob.si el flag de brillo atrib.est� activo
		jp	nz,hayobs
		jp	Nohayobs
;------------------------------------------------------------------------------------
obst_iz:                                ; la izquierda
		ld	a,[colu]
		cp	1
		jr	NZ,YSIIZ
		ld	a,1
        	ld	[flagpa],a      ; Banderin activo de paso de pantallas
		ld	a,3		; 3
		ld	[senpro],a	
		ld	a,[fila]
        	ld	[last_p+1],a    ; Inicializan VALORES CUANDO MUERES
		ld	a,29
		ld	[colu],a
        	ld	[last_p],a
		jp	hayobs
YSIIZ:
		ld	a,[fila]        ; primer CHR
		add	a,4		; antes 2
		and	248	        ; Le quito los 3 bits inferiores para quedarmecon
		ld	c,a             ; la posic. a convertir a char exacta
		ld	a,[colu]
		dec	a
		ld	l,a
		call	c_atr_prota_pant
; Tengo que guardar el resultado para que estss rutinas vayan m�s r�pido
; Calculada la primer posici�n en altura las restantes solo hay que sumar $20 a fila
		ld	a,[hl]
		bit     6,a             ; comprob.si el flag de brillo atrib.est� activo
		jr	nz,hayobs
;CHR a izq. 8pixels abajo
		ld	bc,$20
		add	hl,bc
		ld	a,[hl]
		bit     6,a             ; comprob.si el flag de brillo atrib.est� activo
		jr	nz,hayobs
;CHR a 15 pixels abajo
		add	hl,bc
		ld	a,[hl]
		bit     6,a             ; comprob.si el flag de brillo atrib.est� activo
		jr	nz,hayobs
;--------------------------------------------------------------------------
; actvamos o desactivamos obstaculos
Nohayobs:
		ld	a,1
		ret
hayobs:
		xor	a
		ld	[proenpla],a	; Pongo la variable de estar en la plataforma a 0
		ret
