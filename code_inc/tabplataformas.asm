;----------------------------------------------------------------------------------------
; Tabla de plataformas por pantalla. 
; Db's: panta, tipla,sentpla, posx,posy,lim1,lim2

; Explicaci�n: tipla=(0=vertical u 1=horizontal), sentpla (Horiz=0der,1izq Vertic=0arr,1aba)

; lim1,lim2  limites predeterminados, no comprobar finales de pantalla ni obstaculos.
; *** ojo *** Contador de ticks = 1 siempre, Maximo ticks
; M�ximo 2 plataf.X pantalla. No se traspasa panta a tabplapant

RESTTABPLAPANT:				; 
;tipla,sentpla, posx,posy,lim1,lim2,skipf,skipftot
	db 255,0,0,0,0,0,0,0
	db 255,0,0,0,0,0,0,0
	db 255

TABPLATFORM:				; 
	db 13,0,0, 6,112, 88,136,1,8	;6	
	db 16,0,0,23,112, 96,136,1,8	;6
	db 24,0,1,21, 96, 80,120,1,8	;4
	db 29,0,1,15, 96, 88,136,1,8	;6

	db 33,0,0,20,136, 96,144,1,8	;6
	db 33,0,1, 6, 88, 64,136,1,8	;8
	
	db 36,1,1,22,144, 14, 23,1,6
	db 36,1,0, 5,144,  4,  8,1,8
	db 37,1,1,11,112, 10, 17,1,6
	db 37,0,1,25, 96, 88,136,1,8	;4

	db 38,0,0,13,120,104,136,1,8	;6

	db 39,1,0,14,120, 13, 22,1,8

	db 39,0,1,24, 80, 72,120,1,6
	db 40,0,0, 7, 80, 64, 88,1,6	
	db 40,0,1,18, 72, 64,128,1,4
	db 41,1,1, 8,136,  7, 13,1,8
	db 41,1,0,23,136, 18, 23,1,8

	db 44,0,0,20,136, 88,144,1,6	;5
	db 46,0,0,15,120, 96,128,1,6
	db 46,0,1,26, 80, 72,120,1,6
	db 49,0,1,12, 80, 72,120,1,5
	db 51,0,1,23,112,104,152,1,6
	db 52,0,1, 7, 72, 64,144,1,6
	db 52,0,0,15,128, 72,136,1,8
	db 53,1,0, 9,144,  8, 15,1,8
	db 54,1,0,11, 96, 10, 24,1,8
	db 56,0,1,15, 80, 72,136,1,6
	db 57,1,1,13, 96,  9, 20,1,8
	db 255
