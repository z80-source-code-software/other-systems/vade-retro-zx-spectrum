
; Final del Juego
final_game:
	incbin "gfx_inc\finalgfx.zx7"	; Final comprimido
; Lo descomprimo en un trozo del buffer de pantalla. Voy a ponerlo en 64000=FA00
descomprimefinal:
	ld	hl,final_game	; vaderetro
	ld	de,64000	; $fa00	Direcci�n en el buffer de pantalla donde descomprimirse.
	ld	bc,215		; bytes del titulo comprimido.
; llamamos al descompresor
	jp	dzx7