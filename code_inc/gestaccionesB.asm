;--------------------------------------------------------------------------------------------------------
; Rutina de Gesti�n de Acciones 		Activada al recoger un objeto que provoca una acci�n.
;

; Se utiliza solo para animar la activaci�n de la espada o calaveras+pocima

comp_acciones2:
	push	ix
	push	hl
	push	de
; Comprobaci�n de objeto que ha activado ACCIONES 2	
	ld	a,[tipObjeto]
	cp	3	
	jr	z,Arrow_Espada		; La espada es la que se ha activado
; cualquier otro caso es la Pocima o las calaveras las que han activado la acci�n
Arrow_poison:

; Comprobaci�n de fin de animacion 
	ld	a,[finanimac]
	and	a
	jr	z,Desact_y_anular2
;
; Colocaci�n en el marcador de la flecha abajo
;
	ld	a,16
	ld	[col_dat],a		; columna en el marcador
	ld	c,a
	ld	a,21
	ld	[fil_dat],a		; fila en el marcador en char
	ld	b,a
	ld	a,79			; Coloc_arrow_down
	ld	[num_tile],a		; tile de borrado es 71
	call	pinflecmar
; skipf_p para la flecha 
	ld	a,[skipf_f]
	and	a
	jr	z,reset_skipf_flechapoison
	dec	a
	ld	[skipf_f],a
	ld	hl,#5ab0
	ld	[hl],a
	jr	Desact_sinanular2
reset_skipf_flechapoison:
	call	son_flecha		; sonido de flecha activa
	ld	a,7
	ld	[skipf_f],a
	ld	hl,#5ab0
	ld	[hl],0
	ld	hl,finanimac
	dec	[hl]
	jr	nz,Desact_sinanular2
	jr	Desact_y_anular2
;
; Flecha Espada
;
Arrow_Espada:

; Comprobaci�n de fin de animacion 
	ld	a,[finanimac]
	and	a
	jr	z,Desact_y_anular1
;
; Colocaci�n en el marcador de la flecha abajo
;
	ld	a,18
	ld	[col_dat],a		; columna en el marcador
	ld	c,a
	ld	a,21
	ld	[fil_dat],a		; fila en el marcador en char
	ld	b,a
	ld	a,78			; Coloc_arrow_down
	ld	[num_tile],a		; tile de borrado es 71
	call	pinflecmar
; skipf_p para la flecha 
	ld	a,[skipf_f]
	and	a
	jr	z,reset_skipf_flechaespada
	dec	a
	ld	[skipf_f],a
; colorear la flecha
	ld	hl,#5ab2
	ld	[hl],a
	jr	Desact_sinanular2
reset_skipf_flechaespada:
	call	son_flecha		; sonido de flecha activa
	ld	a,7
	ld	[skipf_f],a
; borramos la flecha
borrar_flechaesp:
	ld	hl,#5ab2
	ld	[hl],0
	ld	hl,finanimac
	dec	[hl]
	jr	nz,Desact_sinanular2
Desact_y_anular1:
; Imprimimos el texto en pantalla
	call	Imprime_Marco
	call	Imprime_TextoEspada
	jr	bDesact_y_anular2
Desact_y_anular2:
; Imprimimos el texto en pantalla
	call	Imprime_Marco
	call	Imprime_TextoPocima
bDesact_y_anular2:
	ld	hl,nomiraracciones2
	ld	[vermiraracciones2+1],hl		; Desactivo mirar objetos
Desact_sinanular2:
	pop	de
	pop	hl
	pop	ix
	ret
pinflecmar:
	call	search_dat		; alto, ancho y poti_im
; Entrada: de   contiene el tile a imprimir
	jp	im_char
;	call	im_char			; Entrada b = fila y c = colu (menuymarcador.asm)
;	ret

;
; Sonido
;
son_flecha:	
	ld	a,1
	ld	[repe_fx],a
        ld	a,125
        ld	[long_fx],a
	jp	fx48k
;        call	fx48k
;	ret