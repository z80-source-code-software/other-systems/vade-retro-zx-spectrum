 titulo:
	incbin "gfx_inc\titulo.zx7"	; Nombre comprimido VADE RETRO
; Descomprimo el titulo en un trozo del buffer de pantalla. Voy a ponerlo en 64000=FA00
descomprimetitulo:
	ld	hl,titulo	; vaderetro
	ld	de,64000	; $fa00	Direcci�n en el buffer de pantalla donde descomprimirse.
	ld	bc,392		; bytes del titulo comprimido.
; llamamos al descompresor
	jp	dzx7