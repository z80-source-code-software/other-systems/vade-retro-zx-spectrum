;-------------------------------------------------------------------------------------------------------
; Rutina de volcado de enemigos de la pantalla en tabEnemigos.asm    42474

ruvodatsprenpant:
	ld	a,[panta]
	add	a,a
	ld	b,0
	ld	c,a
	ld	hl,tabindenep
	add	hl,bc
	ld	e,[hl]
	inc	hl
	ld	d,[hl]
	ex	de,hl
	ld	[pdbtabEn],hl	; guardo la posic. en TABENEMPANT donde dejar los nuevos datos de los enem
	ld	de,ENEMIGOS
;
; hl contiene la direcci�n donde est�n los datos a volcar
;
contVolEne:
	push	de
	pop	ix
	ld	bc,21
	ldir
;
; calculo los valores para ix21 a ix24 de los enemigos en la tabla ENEMIGOS
;
	exx
	call	im_pix_e
	call	c_dirbuff_e
	exx

;
; la tabla enemigos consta de 25 posiciones + 1 posiciones por la �ltima metida de enemigo muerto
;
	ld	a,e	;1 / 4
	add	a,5	;2 / 7
	ld	e,a	;1 / 4
;
; uso el valor al que apunta hl ahora como comprobaci�n de que puedo seguir volcando datos
;
	ld	a,[panta]
	ld	c,a
	ld	a,[hl]
	cp	c
	jr	z,contVolEne
	ret
;-------------------------------------------------------------------------------------------------------
; Rutina de volcado de TABLA ENEMIGOS en tabEnemigosPant.asm		42511

ruvodatpantspren:
;
; comprobamos si es una pantalla que NO tiene enemigos
;
	ld	a,[pdbtabEn]
	cp	$3e	; db bajo de la direccion que indica que NO hay enemigos
	jr	nz,Bruvodatpantspren
	ld	a,[pdbtabEn+1]
	cp	$d4	; db alto de la direccion que indica que NO hay enemigos
	ret	z
Bruvodatpantspren:
	ld	de,[pdbtabEn]
	ld	hl,ENEMIGOS
	ld	ix,ENEMIGOS
BBruvo:
	ld	a,[hl]
	rla		; asi es mucho m�s r�pido pero no puedo haber ning�n dato superior a 128
	ret	c	; IX+0 que es panta no puede ser igual o superior a 128
;
; Comprobamos que no sea el enemigo flecha,bola,gota. Estos no se guardan osea que siempre salen de su pos.inicial
;
	ld	a,[ix+18]
	cp	128		; flecha
	jr	z,BajoBBruvo
	cp	2		; bola
	jr	z,BajoBBruvo
	cp	1
	jr	z,BajoBBruvo	; gota
;
; Ahora ver si alg�n enemigo ha sido tocado. Si es as� no salvar los datos en su direcc.de pantalla
;
	ld	a,[ix+25]
	cp	1
	jr	z,BajoBBruvo
;
; Vamos a copiar los siguientes datos en el fichero de enemigos en pantallas
;
	inc	hl
	inc	hl
	inc	de
	inc	de
	ld	bc,19
	ldir
	inc	hl
	inc	hl
	inc	hl
	inc	hl
	inc	hl
	ld	bc,26
	add	ix,bc
	jr	BBruvo
;
; Siguiente churro de db's en caso de que el anterior sea una flecha o una bola
;
BajoBBruvo:
	ld	bc,26		; 25 sin el bit de enemigo muerto
	add	hl,bc
	add	ix,bc		; siguiente churro db's en ENEMIGOS para HL y IX
	ex	de,hl
	ld	bc,21
	add	hl,bc
	ex	de,hl		; siguiente churro db's en TABENEMPANT para DE
	jr	BBruvo
;-------------------------------------------------------------------------------------------------------
; Rutina de limpieza de Tabla ENEMIGOS

limpia_ENEMIGOS:
	ld	hl,ENEMIGOS
	ld	de,ENEMIGOS+1
	ld	bc,77
	xor	a
	ld	[hl],a
	ldir
	ld	a,255
	ld	[54255],a
	ld	[54281],a
	ld	[54307],a
	ret
;
; Tabla indice de los db's de los enemigos en cada pantalla
;

; $b948		;$48,$b9	;Churro que restaura valores a 0 y que vale como indice de pantalla sin enemigos

tabindenep:	;(ojo que los indices cambian si no est�n en un lugar fijo o si introduces algun dato nuevo)
	db	$54,$d4	;0	54353
	db	$69,$d4	;1	54374
	db	$7e,$d4	;2	54395 
	db	$93,$d4 ;3	54416
	db	$bd,$d4 ;4	54458
	db	$d2,$d4 ;5	54479
	db	$e7,$d4 ;6	54500
	db	$fc,$d4 ;7	54521
	
	db	$3e,$d4 ;8	54331

	db	$11,$d5 ;9	54542
	db	$26,$d5 ;10	54563
	db	$3b,$d5 ;11	54584
	db	$50,$d5 ;12	54605
	db	$65,$d5 ;13	54626
	db	$7a,$d5 ;14	54647
	db	$8f,$d5 ;15	54668
	
	db	$3e,$d4 ;16	54331+3

	db	$b9,$d5 ;17	54710
	db	$ce,$d5 ;18	54731

	db	$3e,$d4 ;19	54331

	db	$f8,$d5 ;20	54773
	db	$0d,$d6 ;21	54794
	db	$37,$d6 ;22	54836
	db	$61,$d6 ;23	54878
	db	$76,$d6 ;24	54899
	db	$8b,$d6 ;25	54920

	db	$3e,$d4 ;26	54331

	db	$b5,$d6 ;27	54962
	db	$df,$d6 ;28	55004
	db	$f4,$d6	;29	55025
	db	$1e,$d7	;30	55067

	db	$5d,$d7	;31	55130
	db	$87,$d7 ;32	55172
	db	$9c,$d7 ;33	55193
	db	$c6,$d7 ;34	55235
	db	$f0,$d7 ;35	55277
	db	$1a,$d8 ;36	55319
	db	$2f,$d8 ;37	55340
	db	$59,$d8 ;38	55382
	db	$83,$d8 ;39	55424
	db	$98,$d8 ;40	55445
	db	$ad,$d8 ;41	55466
	db	$d7,$d8 ;42	55508
	db	$16,$d9 ;43	55571
	db	$55,$d9 ;44	55634
	db	$7f,$d9 ;45	55676
	db	$94,$d9 ;46	55697
	db	$a9,$d9	;47	55718
	db	$e8,$d9	;48	55781
	db	$12,$da	;49	55823
	db	$27,$da ;50	55844
	db	$3c,$da	;51	55865
	db	$66,$da ;52	55907
	db	$7b,$da ;53	55928
	db	$ba,$da ;54	55991
	db	$e4,$da ;55	56033
	db	$f9,$da ;56	56054

	db	$3e,$d4 ;58	54331
	