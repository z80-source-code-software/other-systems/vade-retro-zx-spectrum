;-------------------------------------------------------------------------
;Rutina de interrupciones para el player. Activado en RUCONTROLES.ASM
        org    #9b9b       ;39835
intaddr:			; Etiqueta para cambiar los valores del salto siguiente
        jp     intmusic		; a intmusic o intgame
intmusic:
        push	af
        push	bc
        push	de
        push	hl
        call	INICIO		; en el player de wyz
        pop	hl
        pop	de
        pop	bc
        pop	af
        ei
        reti


intgame:
	push	af
        push	hl
;
; Emitir sonido o no
;
vervalorson:			; Etiqueta para cambiar el salto y que se oiga sonido o no
	jp	sinvalorson
convalorson:
        call	fx48k
;
; elimino el salto de emitir sonido
;
	ld	hl,sinvalorson	
	ld	[vervalorson+1],hl
sinvalorson:

	ld	a,[ticks]
        inc	a
        ld	[ticks],a
        cp	25	
	jr	c,finisr
	xor	a
        ld	[ticks],a
finisr:
	pop	hl
        pop	af
        ei
        reti
