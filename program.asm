;----------------------------------------------------------------------------------------
; Nombre del juego:  vaderetro                        01/05/1985 retomado el 31/10/2012
;----------------------------------------------------------------------------------------
	include "code_inc\ImpMenuyMarcador.asm"
	include "code_inc\marcadorarriba.asm"
	include "code_inc\marcadorabajo.asm"
	include "code_inc\titulo.asm"		; incluye binario comprimido.
	include "code_inc\rucontroles.asm"	; termina de momento en 27353
	include "code_inc\finalgfx.asm"		; incluye binario comprimido
;*WARNING* *WARING* *WARNING*			BYTES libres 381  -----	*WARNING* *WARING* *WARNING*
	include "gfx_inc\datastileObjeto.asm"	; $6c00  27648 DIRECCION FIJA
	include "code_inc\tabplataformas.asm"	; 
	include "code_inc\rucobjpant.asm"	; 28503... 28558 tabla objetos,   fin en 28756
	include "code_inc\textos.asm"		; 28757
	include "code_inc\tabanimaciones.asm"	; 29115 tabla de animaciones
	include "code_inc\MenuyMarcadorRestos.asm"
	include "code_inc\descompmusica5.asm"
	org	30529		
	incbin "music\musicaVR5.zx7"		; 30529 fin en 31855 (comprimido) 1327
	incbin "music\vaderetro_menu.zx7"	; 31856 ocupa 220bytes
	incbin "music\vaderetro_gameover.zx7"	; 32164 ocupa 286bytes
	include "music\musicaVR5labels.sym"	; direcciones de las etiquetas del player
	include "code_inc\colorespant.asm"	; 32362
	include "gfx_inc\gameovertxtgfx.asm"	; 32407
	include "code_inc\DelcramTabbackup.asm"	; 32553 26 bytes valores a restaurar de aganzio 
	include "code_inc\rucoacciones.asm"	; 32559
	include "code_inc\tabpantas.asm"	; org 32652		32512 +116
	org     32768
;----------------------------------------------------------------------------------------
; INICIO DEL PROGRAMA
;----------------------------------------------------------------------------------------
Inicio_vr:
	xor     a
        out     [254],a
	call	Soltar_tecla
	call	Espera_tecla	; Espera para ver la pant.presentacion MenuyMarcador.asm
;----------------------------------------------------------------------------------------
; Reseteo de valores, limpieza de buffers, impr.menu, borrado, etc
;----------------------------------------------------------------------------------------
Reset_var:
	call	Rest_delcram	; si cabe la subrutina aqu� mejor meterla aqu�.

	call	backup_objetos	; backupobjpant.asm
	call	limpiobuffer	; limpiar solo buffers de pantalla y atributos.
	xor	a
; al volver de limpiobuffer tengo hl,de en el inicio de las variables de ram. poner a 0
	ld	[hl],a
	ld	bc,111
	ldir
;----------------------------------------------------------------------------------------
; Inicializacion de variables
;----------------------------------------------------------------------------------------
	ld	[delcramcm],a	; variable de toques para matar a Delcram (est� en otro sitio y no lo puedo vaciar con el c�digo anterior)
	ld	[hechizo],a	; igual que la anterior
	inc	a
	ld	[dirmovsp],a	; 1
	ld	[vidas],a       ; 
	ld	[senpro],a      ;
	ld	[final],a	;		
	inc	a
					; Importante, Si mueres o finalizas el juego dando espadazos, deja anchosp = 3
	ld	hl,chganchosp+1		; por eso uso la etiqueta en la rutina SprProtaImp.asm
	ld	[hl],a			; Durante el juego cambio el valor de B que es el anchosp 
					; Y el juego debe empezar con anchosp=2
	ld	[anchosp],a	; 2
	ld	a,6		; 6	
	ld	[calavera],a
	dec	a
	ld	[espada],a	; 5 para probar comentar, para jugar descomentar
	dec	a		; 
	ld	[sello],a	; 4
	ld	hl,$4074
	ld	[barra],hl	; energia al m�ximo
	ld	a,11		;55boss		;33	37	; 11 para jugar 
	ld	[panta],A
	ld	a,28		; 28	 columna en char pruebas en 20
	ld	[colu],A        ; COLU
        ld	[last_p],A
	ld	a,120		;120	para jugar			
        ld	[fila],A	; FILA
        ld	[last_p+1],A
	ld	a,8
	ld	[offbarra],a	; offset de barra energia
	ld	[pausacolor],a	; valor color para el fadeout de colores. colorespant.asm
	ld	hl,$e730	; monigote mirando a la derecha
	ld	[spr],hl
	ld	[mira_a],hl
	ld	a,8		; 16 originalmente
	ld	[parpapro],a	; ticks de parpadeo del prota al activar la inmunidad
	ld	hl,$d43e	; Direccion de pantalla sin enemigos
	ld	[pdbtabEn],hl	

	ld	hl,vuelcafondo
	ld	[vervuelcafondo+1],hl		; variable para ir a vuelcafondo o sinvolcarfondo

	ld	hl,nomirarobjetos
	ld	[vermirarobjetos+1],hl		; variable para mirar si hay objetos en pantalla activos
	
	ld	hl,nomiraracciones
	ld	[vermiraracciones+1],hl		; variable para mirar si hay acciones a realizar

	ld	hl,nomiraracciones2
	ld	[vermiraracciones2+1],hl	; variable para mirar si hay acciones2 a realizar

	ld	hl,NO_LLAMA_DELCRAM
	ld	[Pre_Delcram],hl

	ld	hl,sinvalorson			; etiquetas en la rutina de interrupciones
	ld	[vervalorson+1],hl

	call	des_musica	; descomprimo la m�sica y el player en el buffer de pantalla
	call	des_song0
	call	ponmusicaint	; cargo puntero rutina de interrupciones para la musica
;----------------------------------------------------------------------------------------
; Interrupciones y paseos con el player de WYZ
;----------------------------------------------------------------------------------------
	di			; deshabilitar
	call	PLAYER_OFF
	call	def_buffer_mu	; llamo a descompmusica.asm
	ld	a,#9c           ; Comienzo tabla interrup.
        ld	i,a
        im	2
	xor	a		; Cargo la cancion 0
	call	CARGA_CANCION	; Hacemos sonar la canci�n con el player desde la ISR
	ei			; al habilitarlas
;----------------------------------------------------------------------------------------
; Men� Principal del juego. Espera pulsaci�n de tecla
;----------------------------------------------------------------------------------------
Menu:
	call    ruborco         ; Borro la pantalla de presentacion
        call    imprimenu       ; En menuymarcador.asm
	call	CONTROLES	; Elegir control de juego
	call	Espera_tecla	; Espera para empezar a jugar           MenuyMarcador.asm
        call	PLAYER_OFF
	call    ruborco         ; Borro el menu
;----------------------------------------------------------------------------------------
; Bucle principal- Motor del juego              COMIENZA EL JUEGO
Inicio_juego:      
;----------------------------------------------------------------------------------------
	call    empezamos		; imprimarcador en menuymarcador.asm   24246
	call	pon_spr_buff
	ld	hl,intgame		; puntero de rutina de interrupciones
        ld	[intaddr+1],hl
Volcado_de_pantalla:
        call	LLAMADA			; llamada a rutinas en el cambido de pantalla.(m�s abajo)
	call	calc_pon_spr_prota	; avolcamiento + mov
GAME:					
;
; comprobamos la variable final para saber en que punto andamos
;
	ld	a,[final]
        and	a
	jp	z,FINALJUEGO		; Si hay acarreo ve a final del juego
;
; Comprobaci�n de vidas o cambio de pantalla
;
	ld	a,[vidas]
        and	a
        jp	z,MUERTO		; si hay 0 est�s muerto
;
; Activado el objeto?
;
vermirarobjetos:
	jp	nomirarobjetos
mirarobjetos:
	call	comp_objetos
nomirarobjetos:
;
; Activada acci�n a realizar				
;
vermiraracciones:
	jp	nomiraracciones
miraracciones:
	call	comp_acciones
nomiraracciones:
;
; Activada acci�n de acci�n
;
vermiraracciones2:
	jp	nomiraracciones2
miraracciones2:
	call	comp_acciones2
nomiraracciones2:
;
; Activada animaci�n
;
vermiraranimaciones:
	jp	nomiraranimac
miraranimac:
	call	GESTANIMACIONES
nomiraranimac:
;
; sincronizaci�n de la pantalla lectura del teclado, impresion prota y enemigos
;
	halt
	call	LEER			
	ld	a,[flagpa]		; 1 cambio de pantalla 0 no hay cambio
	rra	
	jr	c,Volcado_de_pantalla
;
; Activado volcar el fondo
;
vervuelcafondo:
	jp	sinvolcarfondo
vuelcafondo:
	call	vuelcafondosprprota	; SprProtaVolc.asm
sinvolcarfondo:
	call	mov			; Antes era call pon_spr_prota		
	call	GESTENEM
;
; Comprobaci�n de plataformas
;
verplataformas:
	jp	nohayplataformas
hayplataformas:
	call	GESTPLATFORM		; Gestion de plataformas moviles.
nohayplataformas:
;
; Inmunidad del protagonista.
;
	ld	a,[inmunidad]
	and	a
	jp	z,GAME
	ld      hl,parpapro		; Espera de tiempo entre toques con enemigos
        dec     [hl]
	jp	nz,GAME
	ld	a,8			; 8
	ld	[parpapro],a
	xor	a
	ld	[inmunidad],a
	ld	[vchoqEnem],a
	jp	GAME
;----------------------------------------------------------------------------------------
; RUTINA DE JOYSTICK y CURSORES Adaptada
; entrada: valores 1,3,5,7 = arriba,derecha,abajo,izquierda ; salida: valor en D
;----------------------------------------------------------------------------------------
LEER:
;----------------------------------------------------------------------------------------
; miramos si hemos elegido teclas o joystick
	ld	hl,[control]    ;Valor que se nutre en RUCONTROL.ASM
        jp	[hl]
;----------------------------------------------------------------------------------------
; RUTINA DE JOYSTICK			rutina ok pero sin comprobar si pulsabas ARRIBA
;----------------------------------------------------------------------------------------
JOYSTICK:
	ld	e,0
	ld	a,231
	in	a,[#fe]
	push	af
	xor	31	; enmascaramos del 6 al 0
	rra	
	rl	e	; pasamos fuego que quedar� en el bit 1
	rra	
	rl	e	; pasamos arriba que quedar� en el bit 0
	ld	d,e
	pop	af
	xor	31	; enmascaramos denuevo la semifila
	rra		; Otra forma de hacerlo
	rra
	rra
	rra
	rl	e	; derecha
	rra
	rl	e	; izquierda
	sla	e
	sla	e	; los dejo en su sitio		52 states
	ld	a,d
	or	e
	ld	e,a
	jr	A_EVENTOS_CONT
;----------------------------------------------------------------------------------------
; RUTINA DE TECLADO
;----------------------------------------------------------------------------------------
KEYBOARD:			;(ANTES MT)
	ld	e,0
	ld	a,#df
	in	a,[#fe]
	xor	3	        ; ENMASCARAMOS LAS TECLAS O y P
	rra
	rl	e		; Bit 3 valor derecha
	rra
	rl	e		; Bit 2 valor izquierda
	ld	a,#7f
	in	a,[#fe]
	xor	1		; Enmascaro la tecla Space
	rra
	jr	nc,mirarqa
; Estas pulsando ahora, pues comprueba si antes lo hiciste
	ld	a,[keypush2]	; Que valor ten�a antes?
	and	2		; Miro el valor de Q anteriormente.
	cp	2
	jR	z,estabaspulsandoSpace	; por lo que entiendo que NO PUEDES VOLVER A PULSAR.
	scf			; pongo el acarreo a 1 
	rl	e
	jr	nopulsabasQ	; activo espadazo en ambas variables y no compruebo salto.
;Antes si hab�as pulsado, anula esta nueva pulsacion para keypush
estabaspulsandoSpace:
	xor	a		; pongo el acarreo a 0
	rl	e
	jr	bnopulsabasQ	; Con lo cual no miro el salto.ni cambies keypush2
mirarqa:
	rl	e
	ld	a,#fb
	in	a,[#fe]
	xor	1		; Enmascaramos la tecla saltar
; Ver si hab�as pulsado ya la tecla anteriormente y no la has soltado.
	rra	
	jr	nc,nopulsabasQ	; No pulsas la Q sino hay acarreo
	ld	a,[keypush2]	; Que valor ten�a antes?
	and	1		; Miro el valor de Q anteriormente.
	cp	1
	jr	z,estabaspulsandoQ	;,por lo que entiendo que NO PUEDES VOLVER A PULSAR.
	scf			; No pulsabas anteriormente Q,pongo a 1 el acarreo
nopulsabasQ:
	rl	e
	ld	a,e
	ld	[keypush2],a
	jr	bA_EVENTOS_CONT
estabaspulsandoQ:
	xor	a		; acarreo a 0 y valor para Q = 0
bnopulsabasQ:
	rl	e		; Bit 0 valor Salto
; en E tenemos el valor de las pulsaciones de (p,o,q,space)
A_EVENTOS_CONT:
	ld	a,e		; 1/4 son 1byte
bA_EVENTOS_CONT:
	ld	[keypush],a	; 4/13 3bytes
;----------------------------------------------------------------------------------------
; RUTINA DE EVENTOS DE CONTROL
; Valores de Senpro = 0-salta, 1-derecha, 2-abajo, 3-izq, 4-sal/der, 5-sal/izq, 6-parado
;----------------------------------------------------------------------------------------
EVENTOS_DE_CONT:
;
; Usamos ACCION con un doble proposito. Comprobar si vienes aqui en un disparo o salto previo
;
	ld      a,[accion]		; 4/13
        rra	                        ; 1/4 Vemos si has disparado
        jp      c,ESPADAZO		; 3/10
        rra				; 1/4 Vemos si estaba el sprite saltando
	jp	c,CONT_SALTA		; 3/10 total=39tstates
;
; Comprobamos si estamos arriba de una plataforma
;
	ld	a,[proenpla]
	cp	1
	jp	z,Enlaplataforma	; Rutina gestplataformasB.asm Y retorno a TER_EVEN_CONT
;
; Lo Segundo es comprobar si CAE al no encontrar suelo	(p,o,space,q)
;
SEG_EVEN_CONT:
	call	obst_aba		; comprobamos el suelo
	and	a
	jr	z,TER_EVEN_CONT		; Si hay obstaculo bajo tus pies, continua 
	ld	a,2			; en caso contrio debes caer
	ld	[senpro],a
	jp	MIRA_ABA
;
; Lo Tercero es comprobar si SALTA AHORA Y COMO LO HACE
;
TER_EVEN_CONT:
	ld	a,[flagpa]		; 1 cambio de pantalla 0 no hay cambio
	rra
	ret	c
	ld	a,[keypush]
	bit	0,a
	jp	z,CUA_EVEN_CONT		; si hay 0 no hay salto
	ld	a,2
	ld	[accion],a		; activo el salto
	call	preparar_salto
	ret
;
; Lo Cuarto es comprobar si ha pulsado DISPARO,DERECHA,IZQUIERDA
;
CUA_EVEN_CONT:
	ld	a,[keypush]		;comprobaciones de pulsacion de tecla DISPARO
	bit	1,a
	jp	z,DERECHA
	ld      a,[espada]
	and     a
	jr      nz,DERECHA               ; Si no llevas la espada no golpeas
;
; Si ACTIVADA la espada, imprime el spr en la direccion correcta 
;
	call	vuelcafondosprprota	; imprimo el fondo con los datos previos de fila y colu
	ld      a,7
	ld      [senpro],a              ; Activamos la espada para golpear
	xor	a
	ld	[skipf_p],a		; Pongo a 0 el skipframe
;
; sonido mandoble por interrupciones
;
	inc	a
	ld	[repe_fx],a		; 1
        ld	a,86
        ld	[long_fx],a
	ld	hl,convalorson	; etiquetas en la rutina de interrupciones
	ld	[vervalorson+1],hl
	ld	a,3
	ld	[anchosp],a		; Ancho del prota a 3
;
; cambio el valor de la direcci�n en SprProtaImp.asm donde B=2 
;
	ld	hl,chganchosp+1		; etiqueta en la rutina SprProtaImp.asm
	ld	[hl],a			; cambio el valor de B que es el anchosp 
	ld	a,[accion]
	set	0,a			; Activo el bit de espadazo.
        ld      [accion],a
	ld      a,[dirmovsp]            ; hacia donde miraba ?
        cp      1
        jr      z,MIRADER_ESPADA
; ESPADAZO a la izquierda	
	ld	hl,colu			; decremento la columna para ajustar el sprite
	dec	[hl]			; de la espada con 3 de ancho a los 2 del parado
	call	im_pix		; Antes saltabas a avolcamiento en SprProtaVolc.asm
; Vuelco el frame ESPADAZO en el buffer
	ld	hl,$e9d0		; sprite 1 espadazo izquierda
	ld	[spr],hl
	ld	[DbEspFr],hl		; Guardo el VALOR para usarlo despu�s con los otros frames.
	call	pon_spr_esp_buff		
	call	busq_num_desp		; calculo el valor del desplazamiento Valor en A y B
; Desplazar esta animaci�n antes de volcarla a pantalla con frame
	and	a			
	jr	z,BLDESP_IZ		; si el valor es 0 no lo desplaces
LOOPDESPESPADAZO_IZ:
	push	bc
	call	desp_spr_esp_izq		; desplazo a la derecha el frame
	pop	bc
	djnz	LOOPDESPESPADAZO_IZ
BLDESP_IZ:
	ld	hl,buffespspr1		; Cargamos hl con el buffer que luego dejaremos en spr
	jp	BNO_ANIMA		; Para meter el valor en spr
MIRADER_ESPADA:
; Desplazar esta animaci�n antes de volcarla a pantalla con frame
	ld	hl,$eb80		; Sprite 1 espadazo derecha
	ld	[spr],hl
	ld	[DbEspFr],hl		; Guardo el VALOR para usarlo despu�s con los otros frames.
	call	pon_spr_esp_buff		; vuelco el frame ESPADAZO del sprite en el buffer
	call	busq_num_desp		; calculo el valor del desplazamiento Valor en A y B
; Desplazar esta animaci�n antes de volcarla a pantalla con frame
	and	a			
	jr	z,BLDESP_DE		; si el valor es 0 no lo desplaces
LOOPDESPESPADAZO_DE:
	push	bc
	call	desp_spr_esp_der		; desplazo a la derecha el frame
	pop	bc
	djnz	LOOPDESPESPADAZO_DE
BLDESP_DE:
	ld	hl,buffespspr1		; Cargamos hl con el buffer que luego dejaremos en spr
	jp	BNO_ANIMA		; Para meter el valor en spr
;
;	Movimientos derecha
;
DERECHA:				;33383
	ld	a,[keypush]
	cp	8			; se pulso derecha?
	jr	nz,IZQUIERDA		; si es NO, vamos a IZQUIERDA
; Compruebo que no estes saltando para obviar el skipf_p durante el mismo
	ld	a,[accion]
	cp	2
	jr	z,CAMB_DIRMOV_DER
; Para TIMER and a , skipf_p = 3
	ld	a,[skipf_p]
	cp	2
	jr	c,aumento_skipf_de	
	xor	a
	ld	[skipf_p],a
	jr	CAMB_DIRMOV_DER
aumento_skipf_de:
	inc	a
	ld	[skipf_p],a
	jp	SALTA_ACTU_ATRs
CAMB_DIRMOV_DER:
	ld	a,[dirmovsp]
	cp	1
	jr	z,sipaderecha
; En caso contrario, que estuvieras pulsando a izquierdas, cambia el sentido del prota.
	ld	hl,tabla_chg_spr
	ld	a,[frame]
	add	a,a
	add	a,l
	ld	l,a
	inc	hl
	ld	a,[hl]
	ld	[frame],a	; nuevo valor de frame
; estabas pulsando a la derecha por lo tanto continua
sipaderecha:
	ld	hl,$e790
	ld	[mira_a],hl		; spr1 de der.
	jr	MIRA_DER
;
;	Movimientos izquierda
;
IZQUIERDA:
	cp	4			; se pulso izquierda?
	jr	nz,SIN_MOV		; si es NO, vamos a mirar DERECHA
; Compruebo que no estes saltando para obviar el skipf_p durante el mismo
	ld	a,[accion]
	cp	2
	jr	z,CAMB_DIRMOV_IZQ
; Para TIMER and a , skipf_p = 3
	ld	a,[skipf_p]
	cp	2
	jr	c,aumento_skipf_iz
	xor	a
	ld	[skipf_p],a
	jr	CAMB_DIRMOV_IZQ
aumento_skipf_iz:
	inc	a
	ld	[skipf_p],a
	jp	SALTA_ACTU_ATRs
CAMB_DIRMOV_IZQ:
	ld	a,[dirmovsp]
	cp	3
	jr	z,sipaizquierda
	ld	hl,tabla_chg_spr
	ld	a,[frame]
	add	a,a
	add	a,l
	ld	l,a
	inc	hl
	ld	a,[hl]
	ld	[frame],a
; Llegas pulsando a la izquierda por lo tanto continua
sipaizquierda:
	ld	hl,$e4f0
	ld	[mira_a],hl		; spr1 de izq.
	jr	MIRA_IZQ
;
; Aqui lleganos si no ha habido ninguna pulsacion
;
SIN_MOV:
	ld	a,6			; aqui quitamos el estado
	ld	[senpro],a		; senpro = 6 no anda
	sub	4
	ld	[anchosp],a		; Restauramos el valor anchosp=2 por si ha sido modificado (espada)
;ANTES_DE_NO_ANIMA:			; Activar parado si no pulsas teclas.
	ld	hl,vuelcafondo		; etiqueta para saltar en el engine
	ld	[vervuelcafondo+1],hl
	call	PROTA_PARADO		; Imprimimos al prota en parado movimientos.asm
	jp	BNO_ANIMA		; Para meter el valor en spr
;----------------------------------------------------------------------------------------
; Actuamos sobre los valores de SENPRO distintos de 6 que es el valor de estar parado
;----------------------------------------------------------------------------------------
;
; Comprobar si hay movimiento a la derecha
;
MIRA_DER:				;33528
	ld      a,1
	ld      [dirmovsp],a		; variable de direccion de movimiento
	ld	a,[frame]
	cp	5
	jr	z,COMP_OB_D
	cp	2
	jr	z,COMP_OB_D
	jp	ACTU_ATRs
;en caso de que frame = 2 o 5 comprobar obstaculos y salida de pantalla
COMP_OB_D:
	call	obst_de
	and	a
	jp	z,SALTA_ACTU_ATRs	;parado	; Solo en el caso de que tropieces con obst�culo
	ld	hl,colu
	inc	[hl]
	jr	A_COMP_OB_I
;
; Comprobar si hay movimiento a la izquierda
;
MIRA_IZQ:
	ld      a,3
	ld      [dirmovsp],a		; variable de direccion de movimiento
	ld	a,[frame]
	cp	5
	jr	z,COMP_OB_I
	cp	2
	jr	z,COMP_OB_I
	jr	ACTU_ATRs
;en caso de que frame = 2 o 5 comprobar obstaculos y salida de pantalla
COMP_OB_I:
	call	obst_iz
	and	a
	jr	z,SALTA_ACTU_ATRs	; parado.Solo en el caso de que tropieces con obst�culo
	ld	hl,colu
	dec	[hl]
A_COMP_OB_I:
	call	vuelcafondosprprota	; imprimo el fondo con los datos previos de fila y colu
	call	im_pix		; Antes saltabas a avolcamiento en SprProtaVolc.asm
; Compruebo de donde vienes
	ld	a,[frame]
	cp	5
	jr	nz,ACTU_ATRs
; En caso contrario era el frame 2
RESET_FRAM:
	xor	a
	ld	[frame],a
	jr	PARACHSPs		
;
; Comprobar si hay movimiento hacia abajo, si estamos cayendo.
;
MIRA_ABA:
; En caso de senpro = 2 es que el prota cae.
	call	vuelcafondosprprota	; 1.volcar fondo
	ld	a,[fila]		; 
	add     a,4			; 2. aumentar fila
	ld      [fila],a
	call	im_pix		; Antes saltabas a avolcamiento en SprProtaVolc.asm
	jr	ANO_ANIMA		
;----------------------------------------------------------------------------------------
; Esta es la parte encargada de cambiar el prota 
; las animaciones, cambios de coordenadas y otras acciones del prota
;----------------------------------------------------------------------------------------
SALTA_ACTU_ATRs:
; En caso de que llegues a final de pantalla,ven X aqui para no repetir la impresion prota.
	ld	hl,vuelcafondo		; etiqueta para volcar fondo al inicio del engine
	ld	[vervuelcafondo+1],hl
	ret
ACTU_ATRs:
	ld      hl,frame
        inc     [hl]
PARACHSPs:
	ld	hl,vuelcafondo		; etiqueta para volcar fondo al inicio del engine
	ld	[vervuelcafondo+1],hl
;----------------------------------------------------------------------------------------
; Valor para BC,sumar a HL(mira_a) para que resulte el spr. a volcar
ANO_ANIMA:
	xor	a
	ld	[proparado],a		; Desactivo q.est� parado para PROTA_PARADO movimientos.asm
	ld	a,[frame]		
	bit	1,a			; primera criba valores 0 a 3 
	jr	nz,mayoroiguala2
	bit	2,a
	jr	nz,mayoroiguala4	; O de 4 a 5
;menoroigualatres:
	and	a			; cojemos los bits 2,1,0 de frame
	jr	z,valores0
	ld	bc,$60			; en caso contrario bc = animacion 0+90
	jr	NO_ANIMA
valores0:
	ld	bc,$0			; bc = 0 animacion
	jr	NO_ANIMA
mayoroiguala2:
	cp	2			; si hay cero el valor de frame es 2
	jr	z,valores2
	ld	bc,$120			; en caso contrario bc = animacion 0+$1b0
	jr	NO_ANIMA
valores2:
	ld	bc,$c0			; bc = animacion 0+$120
	jr	NO_ANIMA
mayoroiguala4:
	cp	4
	jr	z,valores4
	ld	bc,$1e0
	jr	NO_ANIMA
valores4:
	ld	bc,$180
NO_ANIMA:
	ld	hl,[mira_a]		; le sumamos a MIRA_A --> 0,$90,$120 o $1b0
	add     hl,bc			; apuntando a la misma o siguiente animacion
BNO_ANIMA:
	ld      [spr],hl		; metemos el valor de hl en spr para imprimir
;
; Salida de la rutina LEER
;
	ret
;----------------------------------------------------------------------------------------
; Actualizacion de situacion y pantallas. Para el cambio entre pantallas.
;----------------------------------------------------------------------------------------
LLAMADA:
	ld	a,[senpro]
	ld	[last_se],a		; guardo el sentido al entrar en pantalla. (vale para nargul y...)
	ld	hl,nomirarobjetos
	ld	[vermirarobjetos+1],hl		; Desactivado el objetoA
	call	quitanimaciones		; rucolanim.asm
	ld	hl,00
	ld	[d_bu_spr_p],hl
	ld	[dir_spr_p],hl
	call	vuel_plat_act_a_pant	; 33583
	call	cleartabplapant		; limpio la tabla de plataformas en pantalla
	xor	a
	ld	[flagpa],a		; a= 255 en vuel_plat_act_a_pant
	ld	hl,NO_LLAMA_NAZGUL
	ld	[Pre_Nazgul],hl
	ld	hl,NO_LLAMA_DELCRAM
	ld	[Pre_Delcram],hl
	ld	a,2			; Para que no se quede enganchado al cambiar de pantalla 
	ld	[skipf_p],a		; y pinte otros frames
	call	limpiobuffer	
	call	ruvodatpantspren	; Vuelco los datos de la tabla ENEMIGOS en tabenempant
	call	limpia_ENEMIGOS		; Vacio la tabla de ENEMIGOS (ruvodatspr.asm)
	call	mapping			; busco pantalla a imprimir
	call	search_scr		; busco y coloco los tiles de la pantalla en el buffer
	call	vuelcaplat_pant
	call	poneracciones		; siempre antes que ponerobjetos
	call	ponerobjetos	
	call	ponanimaciones		; pongo las animaciones
	call    volcar_pant
	call	ruvodatsprenpant	; volcado TABENEMPANT  -> ENEMIGOS (ruvodatspr.asm)
	ld	a,[panta]
	cp	9
	jr	nc,D_llamada
	ld	hl,LLAMA_NAZGUL
	ld	[Pre_Nazgul],hl
	call	chg_pant_nazgul		; valores nuevos para nazgul.
	ret
D_llamada:
	cp	57
	ret	nz
E_llamada:
	ld	hl,LLAMA_DELCRAM
	ld	[Pre_Delcram],hl
	ret
;----------------------------------------------------------------------------------------
Parafinalygameover:
	ld	hl,sinvalorson	; etiquetas en la rutina de interrupciones
	ld	[vervalorson+1],hl
	call	mov
	call	Soltar_tecla	
	call	fadeout_color	; Fade Out de colores
	jp	ruborco
;----------------------------------------------------------------------------------------
FINALJUEGO:
	call	Parafinalygameover
;
; Mostrar el final	; Inicializar m�sica The End
;
	call	imprim_fin_juego
	call	coloca_mu_final	; colocamusica.asm
	jr	PAGAMEOVERYFINAL
;----------------------------------------------------------------------------------------
MUERTO:
	call	Parafinalygameover
;
; Inicializar m�sica gameover  ; Mostrar el GAME OVER
;
	call	coloca_mu_gameover	; colocamusica.asm
	call	imprim_gameover
;----------------------------------------------------------------------------------------
PAGAMEOVERYFINAL:
	call	Soltar_tecla	
	call	Espera_tecla	; Espera teclas
	di			; deshabilitar isr y apagar el player
	call	PLAYER_OFF
	jp	Inicio_vr	
;----------------------------------------------------------------------------------------
calc_pon_spr_prota:
	call	im_pix		
	call	mov		
	call	vuelcafondosprprota	; imprimo el fondo con los datos previos de fila y colu
	ret
pon_spr_buff:
;----------------------------------------------------------------------------------------
; Mover el SPR del prota al buffer de desplazamiento para su uso. Sprite PARAADO
	ld	hl,[spr]		; monigote actual
	ld	de,buffrotspr1		; $
	ld	bc,96			; para 2 x 3
	ldir
	ret
pon_spr_esp_buff:
;----------------------------------------------------------------------------------------
; Mover el SPR del prota al buffer de desplazamiento para su uso. Sprite ESPADAZO
	ld	hl,[spr]		; monigote actual
	ld	de,buffespspr1		; $e400
	ld	bc,144			; para 3 x 3
	ldir
	ret
;
;----------------------------------------------------------------------------------------
; Rutinas de mapeo y tablas de direccion entre pantallas y tiles ;$6228
	include "code_inc\mapeadorNuevo.asm"	
	include "code_inc\bupaim.asm"
	include "code_inc\CalcTileDirBuffeInyec.asm"
	include "code_inc\PantVolcTilesyAttr.asm"
	include "code_inc\compobjetos.asm"	; fin 34887
;*WARNING* *WARING* *WARNING*		 	BYTES libres  24 -----  *WARNING* *WARING* *WARNING* 
	include "gfx_inc\datastile.asm"		; ocupa 4941bytes Empieza en 34890 fin en  39773
	include "code_inc\rucolanim.asm"	; 39774 coloca animaciones en pantalla
;*WARNING* *WARING* *WARNING*			BYTES libres  7 -----	*WARNING* *WARING* *WARNING*
;----------------------------------------------------------------------------------------
; Tabla y rutina de interrupciones		; 39835
	include "code_inc\rutinainterr.asm"	; fin en 39890
	include "code_inc\backupobjyaccpant.asm"
;*WARNING* *WARING* *WARNING*			BYTES libres  24 ----- 	*WARNING* *WARING* *WARNING*	fin 39913
	include "code_inc\tabinterr.asm"	; 39936 fin en 40193
;----------------------------------------------------------------------------------------
; Rutinas de movimiento del sprite
	include "code_inc\movimientosX2.asm"	; 40193               
	include "code_inc\movimientosNormalesX2.asm"	                     
	include "code_inc\SprprotaImp.asm"	; 41016
	include "code_inc\SprProtaVolc.asm"
	include "code_inc\SprProtaDesp.asm"	;			fin en 41164 
	include "code_inc\gestionenemigos.asm"	; 41339		fin en 41933
	include "code_inc\TabyGestNazgul.asm"	; 41934			fin en 42374 
	include "code_inc\ruvodatspr.asm"	; 42375
	include "code_inc\detchoques.asm"	; 42681
	include "code_inc\barraenergia.asm"	; 42916			fin en 43033
	include "code_inc\SprEnemVolc.asm"	;			fin en 43133
	include "code_inc\iaboss.asm"		; 43179
	include "code_inc\Delcramdetchoques.asm"	;43299		fin en 43660  43654
	include "code_inc\Delcrambackup.asm"	; 12 bytes valores a restaurar de aganzio 
	include "gfx_inc\finaltxtgfx.asm"	; 43669 fin en 43798
;*WARNING* *WARING* *WARNING*			BYTES libres  3 ----- 	*WARNING* *WARING* *WARNING*	fin 39913
	org	43799
	incbin "music\vaderetro_5.zx7"		; 43799 ocupa 809
	include "code_inc\backupacciones.asm"
	include "code_inc\rulimpiabuffers.asm"
;*WARNING* *WARING* *WARNING*			BYTES libres  47 ----- 	*WARNING* *WARING* *WARNING*	fin 39913
;----------------------------------------------------------------------------------------
	org	44720
	include "gfx_inc\sprMuerteEnemigos.asm"	;
;----------------------------------------------------------------------------------------
; Calculo de direcciones en la videoram
	include "code_inc\cdirmemMetalFpixCchar.asm"	; 45056 fin en 45567
;----------------------------------------------------------------------------------------
; Rutinas y tablas de Enemigos
	include "code_inc\tablassprymov.asm"	; 45568 $b200
	include "code_inc\gestplataformas3.asm"	; 45585
	include "code_inc\gestacciones.asm"	; 45974
	include "code_inc\gestaccionesB.asm"	; 46373
	include "code_inc\SprPlatImp.asm"	; 46496
	include "code_inc\gestanimaciones.asm"	; 46659 gestor de animaciones
	include "code_inc\SprAnimImp.asm"	; 
	include "code_inc\SprAnimVolc.asm"	; 
	include "code_inc\SprEnemImp.asm"	; 
	include "code_inc\DelcramTabyGest.asm"	; 47218
	include "code_inc\colocamusica.asm"	
	include "code_inc\dzx7mini.asm"
;*WARNING* *WARING* *WARNING*			BYTES libres -- 51 ---	*WARNING* *WARING* *WARNING*
;----------------------------------------------------------------------------------------
; Sprites Enemigos
;SPRITES enemigos:
	include "gfx_inc\sprenemigos4.asm"	; org $b9a0 47520
;----------------------------------------------------------------------------------------
; Plataformas moviles				; org $c0e0 49376
	include "gfx_inc\plataforma.asm"	; ocupa 32bytes
;----------------------------------------------------------------------------------------
; definici�n de las pantallas	
;	org	$c100	 49408
	include "code_inc\dataspantCo.asm"	;			fin en 54254
;----------------------------------------------------------------------------------------
; definici�n de los enemigos en las pantallas	
	org	$d3ef				;	54255
	include "code_inc\tabEnemigos.asm"	;	restauracion 54331
	include "code_inc\tabEnemigosPant.asm"	;	fin 56099
	include "gfx_inc\spraganzio3.asm"	; ocupa 2016 bytes
;----------------------------------------------------------------------------------------
; las rutinas siguientes se han movido para ocupar espacio y dejar otro m�s arriba
	include "code_inc\SprPlatVolc.asm"	; 58115
	include "code_inc\tabAnimac.asm"	; 58164 churro donde se vuelca tabanimaciones.asm
	include "code_inc\tablasvarias.asm"	; 58205
;*WARNING* *WARING* *WARNING*			BYTES libres 7----   *WARNING* *WARING* *WARNING*
;----------------------------------------------------------------------------------------
; Buffers para desplazamiento del prota.
	org	$e3a0		; 58272
buffrotspr1:              ; 2x3 x8                          
	ds	96        ; 96 bytes para volcar el spr parado y rotarlo frame 0
buffespspr1:              ; 3x3 x8 $e400 = 58368
	ds	144       ; 144 bytes para volcar el espadazo y rotarlo
;----------------------------------------------------------------------------------------
; Frames Protagonista
;----------------------------------------------------------------------------------------
SPR_prota:                     ; 2x3 x8 x2mask+spr x 7 frames x2izq y der total= 2016 
	org	$e490	;58512
; Tama�o frame 3x3x8x2=96 bytes o $60h:
; spr_izq_parado = #e490	; spr_der_parado = #e730
; spr_izq_0 = #e4f0	; spr_der_0 = #e790
; spr_izq_1 = $e550	; spr_der_1 = $e7f0
; spr_izq_2 = $e5b0	; spr_der_2 = $e8b0
; spr_izq_3 = #e610	; spr_der_3 = #e910
; spr_izq_4 = #e670	; spr_der_4 = #e970
; spr_izq_5 = #e6d0	; spr_der_5 = #e9d0
	include "gfx_inc\sprprota2x3_6fr2.asm"            ; Total = 1344bytes
;----------------------------------------------------------------------------------------
; Frames Protagonista  de la espada  3x3x8 x2 = 144bytes $90h
; spr1_disparo_izq = #e9d0                   spr1_disparo_der  = #eb80
; spr2_disparo_izq = #ea60                   spr2_disparo_der  = #ec10	
; spr3_disparo_izq = #eaf0                   spr2_disparo_der  = #eca0	
	include "gfx_inc\sprprotaespada3x3b.asm"            ; 
	include "code_inc\ruvodatplf.asm"	; Volcado plataformas 
;----------------------------------------------------------------------------------------
; Buffers de pantalla y atributos de la misma
	org	$ed90
buffpant:                                       ; $ed90 = 60816
        ds      4096      ; Buffer de pantalla
buffattpant:                                    ; $fd90
        ds      512       ; Buffer de atribut.de pantalla   fin $ff90
;----------------------------------------------------------------------------------------
; *  V A R I A B L E S  *			; $ff91
;----------------------------------------------------------------------------------------
; Variable MIRA_A Se usa para saber el numero de sprite que esta usando el prota
ticks       equ 65425    
spr         equ 65426    
vidas       equ 65428    
obje        equ 65429    
fila        equ 65430
colu        equ 65431
num_tile    equ 65432	 
flagpa	    equ 65433
frame       equ 65434
senpro      equ 65435
dirmovsp    equ 65436
dead        equ 65437
skipf_e	    equ 65438	; 1byte skip-frame del enemigo/ animaci�n enemiga
last_p      equ 65439
al_tile     equ 65440
an_tile     equ 65441
fil_dat     equ 65442
col_dat     equ 65443
dir_spr     equ 65444    ; posicion del spr.en pantalla
poti_im     equ 65446
poti_ia     equ 65448
final       equ 65450
timer	    equ 65451
repe_fx     equ 65453
long_fx     equ 65454
inmunidad   equ 65455
control     equ 65456
cont_inmu   equ 65458
accion      equ 65459
espada      equ 65460
flagespa    equ 65461
d_bu_spr    equ 65462	; direccion en el buffer del prota
bytevesal   equ 65464
anchosp     equ 65465	
DbEspFr	    equ	65466	; 2 bytes guarda el valor actual del frame Espadazo a imprimir.
cronos      equ 65468
vchoqe      equ 65469
pdbtabEn    equ 65470	; 2 bytes posic. en la tabla de enemigos por pantalla donde restaurar valores
;valorson    equ 65472	; salto la rutina de sonido en la gestion de interrupciones.
barra	    equ 65474	; 2 bytes barra energia. Char en la barra
offbarra    equ 65476	; offset de la barra de energia para saber donde est�s.
;miraAnim    equ 65477	; 2 bytes para saber si debe mirar las animaciones
tsprpan     equ 65479	; "
ene_ep      equ 65480
flagfin     equ 65482
cancion     equ 65483
tab_col_d   equ 65484
mira_a      equ 65486	; 2bytes
keypush     equ 65488
keypush2    equ 65489
last_se     equ 65490	; guarda el valor de senpro al entrar en la pantalla.
proparado   equ 65491
tplfpan	    equ 65492	; 2bytes para la rutina ruvodatplf.asm 
;volfonsp    equ 65494	; 2bytes direccion de salto en el engine
skipf_p	    equ 65496	; skip-frame prota
;platform    equ 65497	; 2bytes salta a etiq. si hay o no plataformas en pantalla
; byte anterior NO usar lo necesito en la rutina de mapping para el registro H
panta       equ 65500	; el registro L valdr� el valor de aqu� en la rutina mapping.
d_bu_spr_p  equ 65501	; 2bytes
dir_spr_p   equ 65503   ; 2bytes posicion de la plataforma en pantalla
spr_p       equ 65505   ; 2bytes plataforma a imprimir
proenpla    equ 65507	; �Esta el prota subido en la plataforma?
posplat	    equ 65508	; 2bytes posic. IX de la plataf. en pantalla sobre la que est�s subido
;objetoA     equ 65510	; 2bytes Objeto en pantalla Activo si o no
tipAccion   equ 65512	; tipo accion a resolver
churrObj    equ 65513   ; 2bytes Guarda el churro a comprobar en el engine del objeto en pantalla
tipObjeto   equ 65515	; Que clase de objeto vas a recoger?
PocimaAct   equ 65516	; Pocima activa o no, para usar cono la espada
sello	    equ 65517
;Mir_Acc	    equ 65518	; 2bytes	
churrAcc    equ 65520	; 2bytes Guarda el otro churro este el de las acciones
;Mir_Acc2    equ 65523	; 2bytes	
finanimac   equ 65525	; dictamina si la animaci�n de las flehas del marcador han acabado.
calavera    equ 65526
skipf_f	    equ 65527	; skip-frame flechas marcador indicativas de accion
ancho_e	    equ 65528	; ancho del sprite enemigo / animacion enemiga
altosc_e    equ 65529	; alto " "
d_bu_spr_a  equ 65530	; 2bytes direccion en el buffer de la animacion
dir_spr_a   equ 65532   ; 2bytes posicion de la animacion en pantalla
spr_a       equ 65534   ; 2bytes enemigo a imprimir
